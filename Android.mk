LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := SDL2
LOCAL_SRC_FILES := $(LOCAL_PATH)/SDL/libs/${TARGET_ARCH_ABI}/libSDL2.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE := Divine

LOCAL_C_INCLUDES := $(LOCAL_PATH)/include $(LOCAL_PATH)/SDL

LOCAL_SRC_FILES := \
	$(subst $(LOCAL_PATH)/,, \
	$(wildcard $(LOCAL_PATH)/src/entities/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/engine/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/input/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/utils/zlib-1.2.11/*.c) \
	$(wildcard $(LOCAL_PATH)/src/image/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/resource/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/scenemanager/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/text/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/timer/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/window/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/driver/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/filesystem/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/xml/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/utils/*.cpp) \
	$(wildcard $(LOCAL_PATH)/src/driver/OpenGLES/*.cpp))

LOCAL_LDLIBS := -lGLESv2 -llog 

LOCAL_SHARED_LIBRARIES := SDL2

include $(BUILD_SHARED_LIBRARY)
