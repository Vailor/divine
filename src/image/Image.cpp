/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <image/Image.hpp>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <math/Matrix.hpp>
#include <filesystem/IFile.hpp>
#include <SDL2/SDL.h>
#include <utils/Log.hpp>
#include <stdexcept>
#include <algorithm>
#include <ctype.h>
#include "UniversalImageLoader.hpp"

namespace DivineAPI
{
	std::map<std::string, Create> CImage::m_ImageFormats;

	CImage::CImage() : m_Pixels(nullptr), m_Format(Pixelformat::UNKNOWN) 
	{
		if(m_ImageFormats.empty())
		{
			RegisterImageFormat("png", reinterpret_cast<Create>(&UniversalImageLoader::Create));
			RegisterImageFormat("jpg", reinterpret_cast<Create>(&UniversalImageLoader::Create));
			RegisterImageFormat("bmp", reinterpret_cast<Create>(&UniversalImageLoader::Create));
			RegisterImageFormat("tga", reinterpret_cast<Create>(&UniversalImageLoader::Create));
		}
	}

	/*!
	 * @brief Creates a new empty image.
	 * 
	 * @param Size: The resolution of the new image.
	 * @param Format: The pixelformat of the image.
	 * 
	 * @throw std::invalid_argument Occurs if the pixel format is unknown.
	 */
	void CImage::CreateEmptyImage(Vec2Di Size, Pixelformat Format)
	{
		FreeImage();
		m_Size = Size;

		int BytesPerPixel = GetBytesPerPixel(Format);
		m_Format = Format;

		unsigned char *Pixels = new unsigned char[m_Size.x * m_Size.y * BytesPerPixel];

		memset(Pixels, 0, m_Size.x * m_Size.y * BytesPerPixel);
		m_Pixels = Pixels;
	}

	CImage::CImage(const CImage &Img) : CImage()
	{
		CopyImage(Img);
	}

	void CImage::CopyImage(const CImage &Img)
	{
		CreateEmptyImage(Img.GetSize(), Img.GetPixelformat());
		memcpy(m_Pixels, Img.GetPixels(), m_Size.x * m_Size.y * GetBytesPerPixel(Img.GetPixelformat()));
		m_Path = Img.m_Path;
	}

	bool CImage::IsEqual(CImage::Pixelformat Format1, CImage::Pixelformat Format2)
	{
		if(Format1 == Pixelformat::RGBA_8_8_8_8 || Format1 == Pixelformat::RGB_8_8_8)
		{
			if(Format2 == Pixelformat::RGB_8_8_8 || Format2 == Pixelformat::RGBA_8_8_8_8)
				return true;
		}
		else if(Format1 == Pixelformat::BGRA_8_8_8_8 || Format1 == Pixelformat::BGR_8_8_8)
		{
			if(Format2 == Pixelformat::BGR_8_8_8 || Format2 == Pixelformat::BGRA_8_8_8_8)
				return true;
		}
		else if(Format1 == Pixelformat::ALPHA_8)
		{
			if(Format2 == Pixelformat::RGBA_8_8_8_8 || Format2 == Pixelformat::BGRA_8_8_8_8)
				return true;
		}

		return false;
	}

	void CImage::ConvertPixelFormat(CImage::Pixelformat Format)
	{
		int BytesPerNewData = GetBytesPerPixel(Format);
		size_t NewDataLength = m_Size.x * m_Size.y * BytesPerNewData;
		unsigned char *NewData = new unsigned char[NewDataLength];

		unsigned char *Data = reinterpret_cast<unsigned char*>(m_Pixels);
		int BytesPerData = GetBytesPerPixel(m_Format);
		size_t Length = m_Size.x * m_Size.y * BytesPerData;	

		for(size_t i = 0; i < Length; i += BytesPerData)
		{
			for(size_t j = 0; j < BytesPerNewData; j++)
			{
				int Begin = BytesPerNewData * (i / BytesPerData);

				if(m_Format != Pixelformat::ALPHA_8)
				{
					if(j < BytesPerData)
						NewData[Begin + j] = Data[i + j];
					else	
						NewData[Begin + j] = 255;
				}
				else
				{
					if(j + 1 == BytesPerNewData)
						NewData[Begin + j] = Data[i];
					else
					{
						NewData[Begin + j] = 255;
					}
				}
			}	
		}

		delete[] Data;
		m_Pixels = NewData;
	}

	void CImage::SwapRB(CImage::Pixelformat Format)
	{
		size_t Size = m_Size.x * m_Size.y * GetBytesPerPixel(Format);

		uint8_t *Data = (uint8_t*)m_Pixels;

		for(size_t i = 0; i < Size; i += GetBytesPerPixel(Format))
		{
			uint8_t Tmp = Data[i];
			Data[i] = Data[i + 2];
			Data[i + 2] = Tmp;
		}
	}

	/*!
	 * @brief Loads a file.
	 * 
	 * @param Path: Path to the file to load.
	 * 
	 * @throws std::invalid_argument Occurs if the path is empty.
	 * @throw std::runtime_error Occurs if the format is unknown or the file couldn't loaded.
	 */
	void CImage::LoadImage(const std::string &Path)
	{
		if(Path.empty())
			throw std::invalid_argument("The path is empty!");

		FreeImage();
		m_Path = Path;
		IImageHandler *Loader = nullptr;

		std::string Ext = IFile::GetFileExtension(Path);
		std::transform(Ext.begin(), Ext.end(), Ext.begin(), ::tolower);

		std::map<std::string, Create>::iterator IT = m_ImageFormats.find(Ext);
		if(IT != m_ImageFormats.end())
		{
			Loader = IT->second();

			try
			{
				m_Pixels = Loader->LoadImage(Path, m_Size, m_Format);
			}
			catch(...)
			{
				delete Loader;
				throw;
			}
			
			delete Loader;

			if(!m_Pixels)
				throw std::runtime_error("Couldn't load image! File: " + Path);
		}
		else
			throw std::runtime_error("Unsupported format!");
	}

	/*!
	 * @brief Loads an image from memory.
	 * 
	 * @param Data: CImage data.
	 * @param Length: Length of the data.;
	 * @param Ext: The image type. E.g. png, jpg, bmp, tga.
	 * 
	 * @throw std::runtime_error Occurs if the format is unknown or the file couldn't loaded.
	 */
	void CImage::LoadFromMemory(const void *Data, size_t Length, const std::string &Ext)
	{
		FreeImage();
		IImageHandler *Loader = nullptr;

		std::string Tmp = Ext;

		std::transform(Tmp.begin(), Tmp.end(), Tmp.begin(), ::tolower);
		std::map<std::string, Create>::iterator IT = m_ImageFormats.find(Tmp);
		if(IT != m_ImageFormats.end())
		{
			Loader = IT->second();

			try
			{
				m_Pixels = Loader->LoadImage(Data, Length, m_Size, m_Format);
			}
			catch(...)
			{
				delete Loader;
				throw;
			}
			
			delete Loader;

			if(!m_Pixels)
				throw std::runtime_error("Couldn't load image!");
		}
		else
			throw std::runtime_error("Unsupported format!");
	}

	/*!
	 * @brief Saves a file.
	 * 
	 * @param Path: Path to the file to save.
	 * 
	 * @throws std::invalid_argument Occurs if the path is empty.
	 * @throw std::runtime_error Occurs if the format is unknown or the file couldn't saved.
	 */
	void CImage::SaveImage(const std::string &Path)
	{
		if(Path.empty())
			throw std::invalid_argument("The path is empty!");

		IImageHandler *Saver = nullptr;

		std::string Ext = IFile::GetFileExtension(Path);
		std::transform(Ext.begin(), Ext.end(), Ext.begin(), ::tolower);

		std::map<std::string, Create>::iterator IT = m_ImageFormats.find(Ext);
		if(IT != m_ImageFormats.end())
		{
			Saver = IT->second();
			Saver->SaveImage(Path, *this);
		}
		else
			throw std::runtime_error("Unsupported format!");
	}

	/*!
	 * @brief Converts the pixelformat of the image to another one
	 * 
	 * @param Format: The new pixel format.
	 * 
	 * @throws std::invalid_argument Occurs if the format is unknown.
	 * @throw std::runtime_error Occurs if no image is assigned.
	 * @annotation Function does nothing, if Format is equal the format of this image.
	 */
	void CImage::ConvertToFormat(CImage::Pixelformat Format)
	{
		if(Format == m_Format)
			return;

		if(Format == Pixelformat::UNKNOWN)
			throw std::invalid_argument("Unknown pixel format.");

		if(!m_Pixels)
			throw std::runtime_error("No image assigned.");

		if(IsEqual(m_Format, Format))
			ConvertPixelFormat(Format);
		else if(GetBytesPerPixel(m_Format) == GetBytesPerPixel(Format))
			SwapRB(Format);
		else
		{
			ConvertPixelFormat(Format);
			SwapRB(Format);
		}

		m_Format = Format;
	}

	/*!
	 * @brief Sets the color of one pixel at the given position.
	 * 
	 * @param Pos: Position of the pixel.
	 * @param Color: New color of the pixel.
	 * 
	 * @throw std::runtime_error Occurs if no image is assigned.
	 */
	void CImage::SetPixel(Vec2Di Pos, Color color)
	{
		if(!m_Pixels)
			throw std::runtime_error("No image assigned!");

 		uint8_t *Pixels = (uint8_t*)m_Pixels;
 		Pos = Vec2Di(fabs(round(Pos.x)), fabs(round(Pos.y)));

 		if(Pos.x < 0 || Pos.y < 0 || Pos.x >= m_Size.x || Pos.y >= m_Size.y)
 			return;

		int BytesPerPixel = GetBytesPerPixel(m_Format);

		for(int i = 0; i < BytesPerPixel; i++)
		{
			if(IsEqual(m_Format, Pixelformat::RGB_8_8_8))
				Pixels[(Pos.x + m_Size.x * Pos.y) * BytesPerPixel + i] = color.c[i];
			else
			{
				if(i == 0)
					Pixels[(Pos.x + m_Size.x * Pos.y) * BytesPerPixel + i] = color.c[i + 2];
				else if(i == 2)
					Pixels[(Pos.x + m_Size.x * Pos.y) * BytesPerPixel + i] = color.c[i - 2];
				else
					Pixels[(Pos.x + m_Size.x * Pos.y) * BytesPerPixel + i] = color.c[i];
			}
		}
	}

	/*!
	 * @brief Sets the color of one pixel at the given position.
	 * 
	 * @param Pos: Position of the pixel.
	 * @param Color: New color of the pixel.
	 * 
	 * @throw std::runtime_error Occurs if no image is assigned.
	 */
	void CImage::SetPixel(Vec2Di Pos, unsigned int Pixel)
	{
		SetPixel(Pos, Color(Pixel));
	}

	/*!
	 * @brief Draws an image on the given position.
	 * 
	 * @throw std::runtime_error Occurs if no image is assigned.
	 */
	void CImage::DrawImage(Vec2Di Pos, const CImage &img)
	{
		if(!m_Pixels)
			throw std::runtime_error("No image assigned!");

		unsigned int *Pixels = (unsigned int*)m_Pixels;
		unsigned int *ImgPixels = (unsigned int *)img.m_Pixels;

		for(int x = Pos.x; x < Pos.x + img.GetSize().x; x++)
		{
			for(int y = Pos.y; y < Pos.y + img.GetSize().y; y++)
			{
				if(x < m_Size.x && y < m_Size.y)
					Pixels[x + m_Size.x * y] = ImgPixels[(x - Pos.x) + img.GetSize().x * (y - Pos.y)];
			}
		}
	}

	/*!
	 * @brief Returns the count of bytes of the given pixel format.
	 * 
	 * @throw std::invalid_argument Occurs if the format is unknown.
	 */
	int CImage::GetBytesPerPixel(Pixelformat Format)
	{
		int BytesPerPixel = 0;

		switch(Format)
		{
			case Pixelformat::ALPHA_8:
			{
				BytesPerPixel = 1;
			}break;

			case Pixelformat::RGB_8_8_8:
			case Pixelformat::BGR_8_8_8:
			{
				BytesPerPixel = 3;
			}break;

			case Pixelformat::RGBA_8_8_8_8:
			case Pixelformat::BGRA_8_8_8_8:
			{
				BytesPerPixel = 4;
			}break;

			default:
			{
				throw std::invalid_argument("Unknown pixel format.");
			}break;
		}

		return BytesPerPixel;
	}

	/*!
	 * @brief Frees all the image data.
	 */
	void CImage::FreeImage()
	{
		if(m_Pixels)
		{
			unsigned char *Data = (unsigned char*)m_Pixels;

			delete Data;
			m_Pixels = nullptr;
		}

		m_Format = Pixelformat::UNKNOWN;
		m_Size = Vec2Di();
		m_Path.clear();
	}


	/*!
	 * @brief Registers a new image format.
	 * 
	 * @param Ext: The extension of the format.
	 * @param Creator: A creator handler which allows to construct the loader class. See also ::Create.
	 * 
	 * @throw std::invalid_argument Occurs if Ext is empty or Creator is null.
	 */
	void CImage::RegisterImageFormat(std::string Ext, Create Creator)
	{
		if(Ext.empty())
			throw std::invalid_argument("An empty file format extension is not allowed.");

		if(!Creator)
			throw std::invalid_argument("The creator function is null.");

		if(Ext[0] != '.')
			Ext.insert(0, ".");

		std::transform(Ext.begin(), Ext.end(), Ext.begin(), ::tolower);

		m_ImageFormats[Ext] = Creator;
	}
}  // namespace DivineAPI
