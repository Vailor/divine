/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef JPEGLOADER_HPP_
#define JPEGLOADER_HPP_

#include <image/IImageHandler.hpp>
#include <utils/zstream.hpp>

namespace DivineAPI
{
	/*! See the documentation of IImageHandler */
	class UniversalImageLoader : public IImageHandler
	{
		public:
			UniversalImageLoader() {}

			static IImageHandler *Create()
			{
				return new UniversalImageLoader;
			}

			void *LoadImage(const std::string &Path, Vec2Di &Size, CImage::Pixelformat &Format);
			void *LoadImage(const void *Data, size_t Length, Vec2Di &Size, CImage::Pixelformat &Format);
			void SaveImage(const std::string &Path, const CImage &img);

			/*!
			 * @brief Compression routine for png files.
			 */
			static uint8_t *PNGCompression(uint8_t *Data, int Size, int *OutSize, int Quality);

			virtual ~UniversalImageLoader() {}

		private:
			static ZCompressionStream zstreamc;
			static CImage::Pixelformat GetFormat(int Comp);

			//STB image wrapper.
			static int Read(void *User, char *Data, int Size);
			static void Seek(void *User, int n);
			static int Eof(void *User);

			static void WriteImage(void *User, void *Data, int Size);
	};
}

#endif /* JPEGLOADER_HPP_ */
