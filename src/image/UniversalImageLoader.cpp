/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <filesystem/IFile.hpp>
#include <algorithm>
#include <ctype.h>
#include <utils/zstream.hpp>

#include "UniversalImageLoader.hpp"

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

#define STBI_NO_STDIO
#define STBI_NO_PSD
#define STBI_NO_GIF
#define STBI_NO_HDR
#define STBI_NO_PIC
#define STBI_NO_PNM 

#define STBI_WRITE_NO_STDIO
//#define STBIW_ZLIB_COMPRESS(Data, Size, OutSize, Quality) DivineAPI::UniversalImageLoader::PNGCompression(Data, Size, OutSize, Quality)

#include "stb_image.h"
#include "stb_image_write.h"

#include <utils/Log.hpp>

namespace DivineAPI
{
	ZCompressionStream UniversalImageLoader::zstreamc = ZCompressionStream();

	void *UniversalImageLoader::LoadImage(const std::string &Path, Vec2Di &Size, CImage::Pixelformat &Format)
	{
		unsigned int *Pixels = nullptr;
		int Comp = 0;

		stbi_io_callbacks IO;
		IO.read = &Read;
		IO.skip = &Seek;
		IO.eof = &Eof;

		IFile *File = nullptr;

		try
		{
			File = IFile::Create(Path, IFile::Openmode::READ);
		}
		catch(...)
		{
			if(File)
				delete File;
			
			throw;
		}

		Pixels = reinterpret_cast<unsigned int *>(stbi_load_from_callbacks(&IO, File, &Size.x, &Size.y, &Comp, 0));
		delete File;

		if(!Pixels)
			throw std::runtime_error(stbi_failure_reason());


		Format = GetFormat(Comp);

		return Pixels;
	}

	void *UniversalImageLoader::LoadImage(const void *Data, size_t Length, Vec2Di &Size, CImage::Pixelformat &Format)
	{
		unsigned int *Pixels = nullptr;
		int Comp = 0;

		Pixels = reinterpret_cast<unsigned int *>(stbi_load_from_memory(reinterpret_cast<const stbi_uc*>(Data), Length, &Size.x, &Size.y, &Comp, 0));

		if(!Pixels)
			throw std::runtime_error(stbi_failure_reason());

		Format = GetFormat(Comp);

		return Pixels;
	}

	void UniversalImageLoader::SaveImage(const std::string &Path, const CImage &img)
	{
		IFile *File = nullptr;

		try
		{
			File = IFile::Create(Path, IFile::Openmode::WRITE);
		}
		catch(...)
		{
			if(File)
				delete File;
			
			throw;
		}

		CImage Tmp = img;

		if(Tmp.GetPixelformat() != CImage::Pixelformat::RGBA_8_8_8_8)
			Tmp.ConvertToFormat(CImage::Pixelformat::RGBA_8_8_8_8);

		std::string Ext = IFile::GetFileExtension(Path);
		std::transform(Ext.begin(), Ext.end(), Ext.begin(), ::tolower);

		int Ret = 0;

		if(Ext == ".jpg")
			Ret = stbi_write_jpg_to_func(WriteImage, File, Tmp.GetSize().x, Tmp.GetSize().y, 4, Tmp.GetPixels(), 90);
		else if(Ext == ".bmp")
			Ret = stbi_write_bmp_to_func(WriteImage, File, Tmp.GetSize().x, Tmp.GetSize().y, 4, Tmp.GetPixels());
		else if(Ext == ".tga")
			Ret = stbi_write_tga_to_func(WriteImage, File, Tmp.GetSize().x, Tmp.GetSize().y, 4, Tmp.GetPixels());
		else if(Ext == ".png")
		{
			zstreamc.Flush();
			Ret = stbi_write_png_to_func(WriteImage, File, Tmp.GetSize().x, Tmp.GetSize().y, 4, Tmp.GetPixels(), Tmp.GetSize().x * 4);
			zstreamc.Flush();
		}

		delete File;

		if(!Ret)
			throw std::runtime_error("Couldn't save file. File: " + Path);
	}

	CImage::Pixelformat UniversalImageLoader::GetFormat(int Comp)
	{
		switch(Comp)
		{
			case 3:
			{
				return DivineAPI::CImage::Pixelformat::RGB_8_8_8;
			}break;

			case 4:
			{
				return DivineAPI::CImage::Pixelformat::RGBA_8_8_8_8;
			}break;

			default:
			{
				return DivineAPI::CImage::Pixelformat::UNKNOWN;
			}break;
		};
	}

	int UniversalImageLoader::Read(void *User, char *Data, int Size)
	{
		IFile *File = reinterpret_cast<IFile*>(User);
		return File->Read(Data, Size);
	}

	void UniversalImageLoader::Seek(void *User, int n)
	{
		IFile *File = reinterpret_cast<IFile*>(User);
		File->Seek(n, IFile::Seekpos::CUR);
	}

	int UniversalImageLoader::Eof(void *User)
	{
		IFile *File = reinterpret_cast<IFile*>(User);
		return static_cast<int>(File->IsEOF());
	}

	void UniversalImageLoader::WriteImage(void *User, void *Data, int Size)
	{
		IFile *File = reinterpret_cast<IFile*>(User);
		File->Write(reinterpret_cast<char *>(Data), Size);
	}

	/*!
	 * @brief Compression routine for png files.
	 */
	uint8_t *UniversalImageLoader::PNGCompression(uint8_t *Data, int Size, int *OutSize, int Quality)
	{
		uint8_t *Buffer = nullptr;
		zstreamc.SetCompressionLevel(Quality);
		zstreamc.Write(Data, Size);

		Buffer = reinterpret_cast<uint8_t*>(STBI_MALLOC(zstreamc.Size()));
		zstreamc.Read(Buffer, zstreamc.Size());

		*OutSize = zstreamc.Size();

		return Buffer;
	}
}  // namespace DivineAPI



