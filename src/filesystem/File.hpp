/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef FILE_HPP_
#define FILE_HPP_

#include <filesystem/IFile.hpp>
#include <SDL2/SDL.h>

namespace DivineAPI
{    
	class File : public IFile
	{
		public:
			File();

			/*!
			 * @brief Opens a file.
			 * 
			 * @param Path: Path to the file.
			 * @param mode: See Openmode.
			 * 
			 * @throw std::runtime_error Throws if the combination of Path and mode lead to an error.
			 */
			File(const std::string &Path, IFile::Openmode mode) : File()
			{
				Open(Path, mode);
			}

			/*!
			 * @brief Opens a file.
			 * 
			 * @param Path: Path to the file.
			 * @param mode: See Openmode.
			 * 
			 * @throw std::runtime_error Throws if the combination of Path and mode lead to an error.
			 */
			void Open(const std::string &Path, IFile::Openmode mode);

			/*!
			 * @brief Reads a line of a file.
			 * 
			 * @return Returns a line or an empty string, if the file is empty or the end of the file is reached.
			 * 
			 * @throw std::runtime_error Throws if the file isn't opened in read mode.
			 */
			std::string ReadLn();

			/*!
			 * @brief Reads data from the file.
			 * 
			 * @param Buf: The buffer to store the data in.
			 * @param Size: The size to read.
			 * 
			 * @return Returns the size of readed data.
			 * 
			 * @throw std::runtime_error Throws if the file isn't opened in read mode.
			 */
			size_t Read(char *Buf, size_t Size);

			/*!
			 * @brief Writes data to the file.
			 * 
			 * @param Buf: The buffer to be written.
			 * @param Size: The size to be written.
			 * 
			 * @return Returns the size of the written data.
			 * 
			 * @throw std::runtime_error Throws if the file isn't opened in write mode or if the data couldn't be written.
			 */
			size_t Write(const char *Buf, size_t Size);

			/*!
			 * @brief Moves the cursor to a specified position.
			 * 
			 * @param Pos: The size in bytes to move the cursor.
			 * @param From: See Seekpos.
			 * 
			 * @return Returns the current position in the datastream.
			 * 
			 * @throw std::runtime_error Throws if seek fails.
			 */
			int64_t Seek(int64_t Pos, IFile::Seekpos From);

			/*!
			 * @return Gets the current position in the datastream.
			 * 
			 * @throws std::runtime_error Throws if tell fails.
			 */
			int64_t Tell();

			/*!
			 * @brief Gets the next character of the file.
			 * @return Returns -1 if the end of file is reached, otherwise the next byte.
			 * 
			 * @throw std::runtime_error Throws if the file isn't opened in read mode.
			 */
			int Get();

			/*!
			 * @return Returns true, if the end of file is reached.
			 */
			bool IsEOF();

			/*!
			 * @brief Closes the file.
			 */
			void Close();

			/*!
			 * @return Gets the total file size.
			 */
			size_t Size()
			{
				return m_Filesize;
			}

			/*!
			 * @return Returns true if the file is opened.
			 */
			bool IsOpen()
			{
				return m_IsOpen;
			}

			virtual ~File()
			{
				Close();
			}

		private:
			bool m_IsOpen, m_EOF;
			size_t m_Filesize;
			size_t m_Filepos;

			SDL_RWops *IO;
	};
}

#endif /* FILE_HPP_ */
