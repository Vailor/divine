/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <filesystem/IFile.hpp>
#include "File.hpp"

namespace DivineAPI
{
	/*!
	 * @brief Creates an instance for file handling.
	 * 
	 * @param Path: Path to the file.
	 * @param mode: See Openmode.
	 * 
	 * @throw std::runtime_error Throws if the combination of Path and mode lead to an error.
	 */
    IFile *IFile::Create(std::string Path, IFile::Openmode mode)
    {
        return new File(Path, mode);
    }

    /*!
     * @return Gets the file extension of a given file. An empty string is returned if no extension is found.
     */
    std::string IFile::GetFileExtension(const std::string &Path)
    {
        size_t DelimiterPos = Path.find_last_of("/\\");
        if(DelimiterPos == std::string::npos)
            DelimiterPos = 0;

        size_t Pos = Path.find_first_of('.', DelimiterPos);

        return Path.substr(Pos);
    }

    /*!
     * @return Gets the path of a file. An empty string is returned if no extension is found.
     * 
     * @attention The path includes the the last path delimiter. 
     */
    std::string IFile::GetFilePath(const std::string &Path)
    {
        size_t DelimiterPos = Path.find_last_of("/\\");
        if(DelimiterPos == std::string::npos)
            return "";

        return Path.substr(0, DelimiterPos + 1);
    }

    /*!
     * @return Gets the file name.
     */
    std::string IFile::GetFileName(const std::string &Path)
    {
        size_t DelimiterPos = Path.find_last_of("/\\");
        if(DelimiterPos == std::string::npos)
            DelimiterPos = -1;

        return Path.substr(DelimiterPos + 1);
    }
} // DivineAPI
