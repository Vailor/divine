/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "File.hpp"
#include <stdexcept>
#include <utils/Log.hpp>

namespace DivineAPI
{
	File::File()
	{
		 m_IsOpen = false;
		 m_EOF = false;
		 m_Filesize = -1;
		 m_Filepos = 0;
		 IO = nullptr;
	}

	/*!
	 * @brief Opens a file.
	 * 
	 * @param Path: Path to the file.
	 * @param mode: See Openmode.
	 * 
	 * @throw std::runtime_error Throws if the combination of Path and mode lead to an error.
	 */
	void File::Open(const std::string &Path, IFile::Openmode mode)
	{
		SDL_ClearError();

		if(IO)
			Close();

		std::string SDLMode;
		m_Filepos = 0;
		m_EOF = false;

		//Decodes the mode in the sdl mode types.
		switch(mode)
		{
			case IFile::Openmode::READ:
			{
				SDLMode = "rb";
			}break;

			case IFile::Openmode::WRITE:
			{
				SDLMode = "wb";
			}break;

			case IFile::Openmode::RW:
			{
				SDLMode = "w+b";
			}break;

			case IFile::Openmode::APPEND:
			{
				SDLMode = "ab";
			}break;

			default:
			{
				throw std::runtime_error("Unknown mode.");
			}
		}

		IO = SDL_RWFromFile(Path.c_str(), SDLMode.c_str());
		if(!IO)
			throw std::runtime_error("Couldn't open file: " + Path + " " + std::string(SDL_GetError()));

		m_IsOpen = true;

		//Gets the file size.
		m_Filesize = SDL_RWsize(IO);
	}

	/*!
	 * @brief Reads a line of a file.
	 * 
	 * @return Returns a line or an empty string, if the file is empty or the end of the file is reached.
	 * 
	 * @throw std::runtime_error Throws if the file isn't opened in read mode.
	 */
	std::string File::ReadLn()
	{
		std::string Ret;
		int c = 0;

		while(c != '\n')
		{
			c = Get();
			if(c == -1)
				break;

			Ret += (char)c;
		}

		return Ret;
	}

	/*!
	 * @brief Reads data from the file.
	 * 
	 * @param Buf: The buffer to store the data in.
	 * @param Size: The size to read.
	 * 
	 * @return Returns the size of readed data.
	 * 
	 * @throw std::runtime_error Throws if the file isn't opened in read mode.
	 */
	size_t File::Read(char *Buf, size_t Size)
	{
		if(IO)
		{
			size_t Retval = SDL_RWread(IO, Buf, 1, Size);
			m_Filepos += Retval;

			if(Retval != Size)
				m_EOF = true;

			if(Retval == 0 && SDL_GetError())
				throw std::runtime_error(SDL_GetError());

			return Retval;
		}
		else
			throw std::runtime_error("File is not opened in read mode!");	
	}

	/*!
	 * @brief Writes data to the file.
	 * 
	 * @param Buf: The buffer to be written.
	 * @param Size: The size to be written.
	 * 
	 * @return Returns the size of the written data.
	 * 
	 * @throw std::runtime_error Throws if the file isn't opened in write mode or if the data couldn't be written.
	 */
	size_t File::Write(const char *Buf, size_t Size)
	{
		if(IO)
		{
			size_t Written = SDL_RWwrite(IO, Buf, 1, Size);

			if(Written < Size && SDL_GetError())
				throw std::runtime_error(SDL_GetError());

			return Written;
		}
		else
			throw std::runtime_error("File is not opened in write mode!");	
	}

	/*!
	 * @brief Moves the cursor to a specified position.
	 * 
	 * @param Pos: The size in bytes to move the cursor.
	 * @param From: See Seekpos.
	 * 
	 * @return Returns the current position in the datastream.
	 * 
	 * @throw std::runtime_error Throws if seek fails.
	 */
	int64_t File::Seek(int64_t Pos, IFile::Seekpos From)
	{
		if(IO)
		{
			int SDLFrom = -1;

			//Decodes the seekpos to sdls seekpos.
			switch(From)
			{
				case IFile::Seekpos::BEG:
				{
					SDLFrom = RW_SEEK_SET;
				}break;

				case IFile::Seekpos::END:
				{
					SDLFrom = RW_SEEK_END;
				}break;

				case IFile::Seekpos::CUR:
				{
					SDLFrom = RW_SEEK_CUR;
				}break;

				default:
				{
					return -1;
				}
			}

			int64_t Ret = SDL_RWseek(IO, Pos, SDLFrom);
			if(Ret == -1)
				throw std::runtime_error(SDL_GetError());

			return Ret;
		}
		else
			throw std::runtime_error("No file is opened.");
	}

	/*!
	 * @return Gets the current position in the datastream.
	 * 
	 * @throws std::runtime_error Throws if tell fails.
	 */
	int64_t File::Tell()
	{
		if(IO)
		{
			int64_t Ret = SDL_RWtell(IO);
			if(Ret == -1)
				throw std::runtime_error(SDL_GetError());

			return Ret;
		}
		else
			throw std::runtime_error("No file is opened.");
	}
	
	/*!
	 * @brief Gets the next character of the file.
	 * @return Returns -1 if the end of file is reached, otherwise the next byte.
	 * 
	 * @throw std::runtime_error Throws if the file isn't opened in read mode.
	 */
	int File::Get()
	{
		int Ret = static_cast<int>(SDL_ReadU8(IO));

		if(Ret == 0)
		{
			m_EOF = true;
			Ret = -1;
		}

		return Ret;
	}

	/*!
	 * @return Returns true, if the end of file is reached.
	 */
	bool File::IsEOF()
	{
		if(!m_EOF && m_Filesize != 0)
			m_EOF = (m_Filepos == m_Filesize);

		return m_EOF;
	}

	/*!
	 * @brief Closes the file.
	 */
	void File::Close()
	{
		m_IsOpen = false;
		m_Filesize = 0;

		if(IO)
		{
			SDL_RWclose(IO);
			IO = nullptr;
		}
	}
}


