/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "OpenGLESObjectBuffer.hpp"

namespace DivineAPI
{
    COpenGLESObjectBuffer::COpenGLESObjectBuffer(COpenGLESDriver *Driver)
    {
        m_Driver = Driver;
        m_Mode = IObjectBuffer::RenderModes::TRIANGLE_STRIP;

        glGenBuffers(1, &m_VBO);

        m_Size = 0;
        m_Indices = 0;
        m_TrisCount = 0;
        m_VertexCount = 0;

        m_IndicesBuffer = 0;
        m_Attribute = static_cast<VertexAttributes>(0);
    }

    /*!
	 * @brief Uploads or updates vertices of this buffer.
	 * 
	 * @param Vertices: Vertices to upload.
	 * @param Attribute: The vertex attribute to update or upload. See also VertexAttributes.
	 * 
	 * @remarks If an attribute is to be update, care must be taken that the update buffer has the same size as the already existing buffer.
	 * @remark An exception to this is the update of all attributes that exists in this buffer.
	 * 
	 * @throw std::runtime_error Throws only if the first remark occurs.
	 */
	void COpenGLESObjectBuffer::UploadVertices(const std::vector<Vertex> &Vertices, VertexAttributes Attribute)
    {
        if(!(m_Attribute & Attribute) && m_VertexCount != 0 && Vertices.size() != m_VertexCount)
            throw std::runtime_error("You can't resize only one vertex attribute.");

        std::vector<Vec3Df> Positions;
        std::vector<Vec2Df> UVs;
        std::vector<Color> Colors;

        Positions.resize(Vertices.size());
        UVs.resize(Vertices.size());
        Colors.resize(Vertices.size());

        Positions.reserve(Vertices.size());
        UVs.reserve(Vertices.size());
        Colors.reserve(Vertices.size());

        for(size_t i = 0; i < Vertices.size(); i++)
        {
            Positions[i] = Vertices[i].Pos;
            UVs[i] = Vertices[i].UV;
            Colors[i] = Vertices[i].Col;
        }

        glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

        if(Vertices.size() != m_VertexCount)
        {
            m_VertexCount = Vertices.size();
            m_Size = m_VertexCount * sizeof(Vertex);
            m_TrisCount = m_VertexCount / 3;

            glBufferData(GL_ARRAY_BUFFER, m_Size, nullptr, GL_DYNAMIC_DRAW);

            GLint AttribLocation;

            AttribLocation = m_Driver->GetAttribLocation(ATTRIBUTE_VERTEX_COORDS);
            glEnableVertexAttribArray(AttribLocation);
            glVertexAttribPointer(AttribLocation, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);

            AttribLocation = m_Driver->GetAttribLocation(ATTRIBUTE_UV_COORDS);
            glEnableVertexAttribArray(AttribLocation);
            glVertexAttribPointer(AttribLocation, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)(Positions.size() * sizeof(Vec3Df)));

            AttribLocation = m_Driver->GetAttribLocation(ATTRIBUTE_COLOR);
            if(AttribLocation != -1)
            {
                glEnableVertexAttribArray(AttribLocation);
                glVertexAttribPointer(AttribLocation, 4, GL_UNSIGNED_BYTE, GL_TRUE, 0, (const GLvoid*)(UVs.size() * sizeof(Vec2Df)));
            }
        }

        if(Attribute & VERTEX_POSITION)
            glBufferSubData(GL_ARRAY_BUFFER, 0, Positions.size() * sizeof(Vec3Df), Positions.data());

        int TT = glGetError();

        if(Attribute & VERTEX_UV)
            glBufferSubData(GL_ARRAY_BUFFER, (Positions.size() * sizeof(Vec3Df)), UVs.size() * sizeof(Vec2Df), UVs.data());

        TT = glGetError();
        
        if(Attribute & VERTEX_COLOR)
            glBufferSubData(GL_ARRAY_BUFFER, UVs.size() * sizeof(Vec2Df), Colors.size() * sizeof(Color), Colors.data());

        TT = glGetError();
    }

	/*!
	 * @brief Uploads or updates the indices.
	 * 
	 * @param Indices: The new indices for this buffer.
	 * @param ObjIndices: The count of vertices per object.
	 */
	void COpenGLESObjectBuffer::UploadIndices(const std::vector<unsigned short> &Indices, const std::vector<int> &ObjIndices)
    {
        if(!ObjIndices.empty())
            m_IndicesPerObj = ObjIndices;
        else
            m_IndicesPerObj.push_back(Indices.size());

        if(m_IndicesBuffer == 0)
            glGenBuffers(1, &m_IndicesBuffer);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IndicesBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, Indices.size() * sizeof(unsigned short), Indices.data(), GL_DYNAMIC_DRAW);

        int TT = glGetError();

        m_TrisCount = Indices.size() / 3;
        m_Indices = Indices.size();
    }

	/*!
	 * @brief Renders the object buffer.
	 */
	void COpenGLESObjectBuffer::Render()
    {
        GLenum Mode;

        switch(m_Mode)
        {
            case IObjectBuffer::RenderModes::TRIANGLE_FAN:
            {
                Mode = GL_TRIANGLE_FAN;
            }break;

            case IObjectBuffer::RenderModes::TRIANGLE_STRIP:
            {
                Mode = GL_TRIANGLE_STRIP;
            }break;

            case IObjectBuffer::RenderModes::TRIANGLES:
            {
                Mode = GL_TRIANGLES;
            }break;
        };

        GLint AttribLocation = m_Driver->GetAttribLocation(ATTRIBUTE_VERTEX_COORDS);

        AttribLocation = m_Driver->GetAttribLocation(ATTRIBUTE_VERTEX_COORDS);
        glEnableVertexAttribArray(AttribLocation);

        AttribLocation = m_Driver->GetAttribLocation(ATTRIBUTE_UV_COORDS);
        glEnableVertexAttribArray(AttribLocation);

		glBindBuffer(GL_ARRAY_BUFFER, m_VBO);

        AttribLocation = m_Driver->GetAttribLocation(ATTRIBUTE_VERTEX_COORDS);
        glVertexAttribPointer(AttribLocation, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);

        AttribLocation = m_Driver->GetAttribLocation(ATTRIBUTE_UV_COORDS);
        glVertexAttribPointer(AttribLocation, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)(m_VertexCount * sizeof(Vec3Df)));

        if(m_IndicesBuffer)
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IndicesBuffer);

       /* glVertexAttribPointer(PostAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(PostAttrib);*/

        int TT = glGetError();

        if(m_IndicesBuffer)
        {
            //glDrawElements(Mode, m_Indices, GL_UNSIGNED_SHORT, 0);
            uintptr_t Offset = 0;
            for(size_t i = 0; i < m_IndicesPerObj.size(); i++)
            {
                glDrawElements(Mode, m_IndicesPerObj[i], GL_UNSIGNED_SHORT, reinterpret_cast<void*>(Offset));
                Offset += m_IndicesPerObj[i] * sizeof(unsigned short);
            }
        }
        else
        {
            for(int i = 0; i < m_TrisCount; i++)
			    glDrawArrays(Mode, i * 3, 3);
        }

        AttribLocation = m_Driver->GetAttribLocation(ATTRIBUTE_VERTEX_COORDS);
        glDisableVertexAttribArray(AttribLocation);

        AttribLocation = m_Driver->GetAttribLocation(ATTRIBUTE_UV_COORDS);
        glDisableVertexAttribArray(AttribLocation);
    }

	/*!
	 * @brief Deletes this buffer on the gpu side.
	 */
	void COpenGLESObjectBuffer::ClearBuffer()
    {
        glDeleteBuffers(1, &m_VBO);
        glGenBuffers(1, &m_VBO);
        
        if(m_IndicesBuffer != 0)
        {
            glDeleteBuffers(1, &m_IndicesBuffer);
            m_IndicesBuffer = 0;
        }

        m_Size = 0;
        m_Indices = 0;
        m_TrisCount = 0;
        m_VertexCount = 0;
        m_Attribute = static_cast<VertexAttributes>(0);
    }

    /*!
	 * @brief Deletes this object. You must use this to free the object instead of the delete operator.
	 */
	void COpenGLESObjectBuffer::Release()
    {
        m_Driver->RemoveObjectBuffer(this);
        delete this;
    }

	COpenGLESObjectBuffer::~COpenGLESObjectBuffer() 
    {
		glDeleteBuffers(1, &m_VBO);

        if(m_IndicesBuffer)
            glDeleteBuffers(1, &m_IndicesBuffer);
    }
}
