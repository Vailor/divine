/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "OpenGLESTexture.hpp"

namespace DivineAPI
{
	COpenGLESTexture::COpenGLESTexture() : ITexture(nullptr)
	{
		m_TextureID = 0;
		m_TextureSize = Vec2Di();
		m_Driver = nullptr;
		m_CurrentBoundTexture = 0;
		m_Size = 0;
		m_Format = CImage::Pixelformat::UNKNOWN;
	}

	COpenGLESTexture::COpenGLESTexture(COpenGLESDriver *Driver, IResourcemanager *mgr, const CImage &img) : COpenGLESTexture(Driver, mgr)
	{
        CImage Tmp = img;

        switch(img.GetPixelformat())
        {
			case CImage::Pixelformat::RGB_8_8_8:
            case CImage::Pixelformat::BGR_8_8_8:
			case CImage::Pixelformat::BGRA_8_8_8_8:
			{
				Tmp.ConvertToFormat(CImage::Pixelformat::RGBA_8_8_8_8);
			}break;
        }

		CreateEmptyTexture(Tmp.GetSize(), Tmp.GetPixelformat());
		CopySubTexture(Tmp, Vec2Di());

		Tmp.SaveImage("test324.bmp");
	}

	COpenGLESTexture::COpenGLESTexture(COpenGLESDriver *Driver, IResourcemanager *mgr) : COpenGLESTexture()
	{
		m_Rmgr = mgr;
		m_Driver = Driver;
	} 

	/*!
	 * @brief Creates an empty texture.
	 * 
	 * @param Size: Size of the texture.
	 * @param Format: Texture format. See also CImage::Pixelformat.
	 */
	void COpenGLESTexture::CreateEmptyTexture(Vec2Di Size, CImage::Pixelformat Format)
	{
		FreeTexture();

		m_TextureSize = Size;
		m_Size = m_TextureSize.x * m_TextureSize.y * (uint64_t)CImage::GetBytesPerPixel(Format);
		m_Format = Format;

		glGenTextures(1, &m_TextureID);
		BindTexture();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		int StorageFormat = GL_RGBA;

		if(Format == CImage::Pixelformat::ALPHA_8)
			StorageFormat = GL_ALPHA;

		//Loads the pixel data to the gpu.
		glTexImage2D(GL_TEXTURE_2D, 0, StorageFormat, Size.x, Size.y, 0, StorageFormat, GL_UNSIGNED_BYTE, nullptr);
		UnbindTexture();
	}

	/*!
	 * @brief Copies a subtexture to a specific position.
	 * 
	 * @param img: Image to copy.
	 * @param Pos: Position to insert.
	 */
	void COpenGLESTexture::CopySubTexture(const CImage &img, Vec2Di Pos)
	{
		BindTexture();
		glTexSubImage2D(GL_TEXTURE_2D, 0, Pos.x, Pos.y, img.GetSize().x, img.GetSize().y, ConvertPixelformatToGLFormat(img.GetPixelformat()), GL_UNSIGNED_BYTE, img.GetPixels());
		UnbindTexture();
	}

	/*!
	 * @brief Resizes the texture without scale the current data.
	 */
	void COpenGLESTexture::ResizeTexture(Vec2Di Size)
	{
		CImage img = GetImage();

		CreateEmptyTexture(Size, m_Format);
		CopySubTexture(img, Vec2Di());
	}

    /*!
     * @return Gets the image which is stored in this texture.
     */
	CImage COpenGLESTexture::GetImage()
	{
		CImage img;
		img.CreateEmptyImage(m_TextureSize, m_Format);

		BindTexture();

		GLuint fbuf = 0;

		glGenFramebuffers(1, &fbuf);
		glBindFramebuffer(GL_FRAMEBUFFER, fbuf);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_TextureID, 0);

		glReadPixels(0, 0, m_TextureSize.x, m_TextureSize.y, ConvertPixelformatToGLFormat(m_Format), GL_UNSIGNED_BYTE, img.GetPixels());

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glDeleteFramebuffers(1, &fbuf);

		UnbindTexture();

		return img;
	}

    /*!
     * @brief Binds this texture.
     */
	void COpenGLESTexture::BindTexture()
	{
		glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*)&m_CurrentBoundTexture);
		glBindTexture(GL_TEXTURE_2D, m_TextureID);
	}

    /*!
     * @brief Unbinds this texture.
     */
	void COpenGLESTexture::UnbindTexture()
	{
		glBindTexture(GL_TEXTURE_2D, m_CurrentBoundTexture);
	}

	GLint COpenGLESTexture::ConvertPixelformatToGLFormat(CImage::Pixelformat Format)
	{
		switch(Format)
		{
			case CImage::Pixelformat::ALPHA_8:
			{
				return GL_ALPHA;
			}break;

			case CImage::Pixelformat::RGBA_8_8_8_8:
			{
				return GL_RGBA;
			}break;
		}
	}

	void COpenGLESTexture::FreeTexture()
	{
		if(m_TextureID != 0)
			glDeleteTextures(1, &m_TextureID);
			
		m_TextureID = 0;
	}
} 