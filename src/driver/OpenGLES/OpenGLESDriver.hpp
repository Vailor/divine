/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef OPENGLDRIVER_HPP_
#define OPENGLDRIVER_HPP_

#include <driver/IDriver.hpp>
#include <GLES2/gl2.h>
#include <driver/IShaderManager.hpp>
#include <utils/System.hpp>

namespace DivineAPI
{
	/*!
	 * @brief OpenGLES driver and shader manager implementations.
	 */
	class COpenGLESDriver : public IDriver, public IShaderManager
	{
		public:
			COpenGLESDriver();

			/*!
			 * @brief Removes an ObjectBuffer.
			 * 
			 * @param Buffer: The buffer to remove.
			 */
			void RemoveObjectBuffer(IObjectBuffer *Buffer);

			/*!
			 * @brief Removes a texture.
			 * 
			 * @param Tex: The texture to remove.
			 */
			void RemoveTexture(ITexture *Tex);

			/*!
			 * @brief Sets the viewport.
			 * 
			 * @param Pos: Position of the viewport.
			 * @param Size: Size of the viewport.
			 */
			void SetViewport(Vec2Di Pos, Vec2Dui Size);

			/*!
			 * @brief Clears the screen.
			 * 
			 * @param color: The clearscreen color.
			 */
			void Clear(Color color);

			/*!
			 * @brief Creates a new object buffer.
			 */
			IObjectBuffer *CreateBuffer();

			/*!
			 * @return Gets informations about the gpu.
			 */
			GPUInfo GetGPUInfo();

			/*!
			 * @return Gets informations about the gpu memory usage.
			 */
			VRAMInfo GetVRAMInfo();

			/*!
			 * @brief Creates a new texture from an image.
			 * 
			 * @param mgr: The ressource manger which manages the new texture.
			 * @param img: The image to create the texture from.
			 * 
			 * @attention Never call this function outside of the ressourcemanager.
			 */
			ITexture* CreateTexture(IResourcemanager *mgr, const CImage &img); 
			
			/*!
			 * @brief Creates an empty texture.
			 */
			ITexture *CreateTexture(); 


			//------------Shadermanager--------------


            /*!
             * @brief Registers a new shader with its handler. See also ::IShaderHandler to see how to implement a shader.
             * 
             * @param Shaderhandler: The shaderhandler to register.
             * @return Returns a unique id for this shader.
             * 
             * @note The shader handler will be released from the shader manager.
             */
            uint32_t RegisterShader(IShaderHandler *Shaderhandler);

            /*!
             * @brief Applies the shader which should be used to render an object.
             * 
             * @param Shader: The shader id.
             * 
             * @throw std::invalid_argument Throws if the shader id doesn't exists.
             * @throw std::runtime_error Throws if the shader couldn't compile or link.
             */
            void ApplyShader(uint32_t Shader);

            /*!
             * @brief Sets the data for the given uniform id.
             */
            void SetUniform(uint32_t Uniform, const Vec3Df &vec);
            void SetUniform(uint32_t Uniform, const Vec2Df &vec);
            void SetUniform(uint32_t Uniform, const Matrix &m);
            void SetUniform(uint32_t Uniform, int value);
            void SetUniform(uint32_t Uniform, const Color &color);

			GLint GetAttribLocation(uint32_t AttribID) const; 

			virtual ~COpenGLESDriver() {}

		private:
			static std::string ReadFile(const std::string &Path);
			static GLuint CreateShader(GLenum Type, const std::string &Source);
			std::string GetUniformName(uint32_t Uniform);
			GLint GetUniformLocation(std::string Name);

			struct Shader
			{
				IShaderHandler *Handler;
				GLuint GLProgram;
			};

			std::vector<Shader> m_Shaders;
			std::vector<IObjectBuffer*> m_Objects;
			std::vector<ITexture*> m_Textures;

			uint32_t m_CurrentAppliedShader;
	};
}

#endif /* IDRIVER_HPP_ */
