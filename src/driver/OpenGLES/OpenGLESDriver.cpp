/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "OpenGLESDriver.hpp"
#include "OpenGLESObjectBuffer.hpp"
#include "OpenGLESTexture.hpp"
#include <string.h>
#include <algorithm>
#include <filesystem/IFile.hpp>

namespace DivineAPI
{
    COpenGLESDriver::COpenGLESDriver() 
    {
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);

		m_CurrentAppliedShader = -1;
    }

	/*!
	 * @brief Removes an ObjectBuffer.
	 * 
	 * @param Buffer: The buffer to remove.
	 */
	void COpenGLESDriver::RemoveObjectBuffer(IObjectBuffer *Buffer)
    {
        m_Objects.erase(std::remove(m_Objects.begin(), m_Objects.end(), Buffer), m_Objects.end());
    }

	/*!
	 * @brief Removes a texture.
	 * 
	 * @param Tex: The texture to remove.
	 */
	void COpenGLESDriver::RemoveTexture(ITexture *Tex)
    {
        m_Textures.erase(std::remove(m_Textures.begin(), m_Textures.end(), Tex), m_Textures.end());
    }

	/*!
	 * @brief Sets the viewport.
	 * 
	 * @param Pos: Position of the viewport.
	 * @param Size: Size of the viewport.
	 */
	void COpenGLESDriver::SetViewport(Vec2Di Pos, Vec2Dui Size)
    {
        glViewport(Pos.x, Pos.y, Size.x, Size.y);
    }

	/*!
	 * @brief Clears the screen.
	 * 
	 * @param color: The clearscreen color.
	 */
	void COpenGLESDriver::Clear(Color color)
    {
		glClearColor((float)color.R / 255.f, (float)color.G / 255.f, (float)color.B / 255, (float)color.A / 255);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	/*!
	 * @brief Creates a new object buffer.
	 */
	IObjectBuffer *COpenGLESDriver::CreateBuffer()
    {
		IObjectBuffer *Buffer = new COpenGLESObjectBuffer(this);
		m_Objects.push_back(Buffer);
		return Buffer;
	}

	/*!
	 * @return Gets informations about the gpu.
	 */
	GPUInfo COpenGLESDriver::GetGPUInfo()
    {
		GPUInfo Info;
		Info.Vendor = reinterpret_cast<const char*>(glGetString(GL_VENDOR));
		Info.GPUName = reinterpret_cast<const char*>(glGetString(GL_RENDERER));
		Info.GraphicsAPIVersion = reinterpret_cast<const char*>(glGetString(GL_VERSION));
		Info.ShaderVersion = reinterpret_cast<const char*>(glGetString(GL_SHADING_LANGUAGE_VERSION));
		Info.MemInfo = GetVRAMInfo();

		return Info;
	}

	/*!
	 * @return Gets informations about the gpu memory usage.
	 */
	VRAMInfo COpenGLESDriver::GetVRAMInfo()
    {
		VRAMInfo Info;
		memset(&Info, 0, sizeof(Info));

        Info.TotalMemory = System::GetSystemMemoryInfo().TotalMemory;

        for(size_t i = 0; i < m_Objects.size(); i++)
		{
			Info.EngineUsage.BufferMemory += m_Objects[i]->Size();
		}

		for(size_t i = 0; i < m_Textures.size(); i++)
		{
			Info.EngineUsage.TextureMemory += m_Textures[i]->Size();
		}

		Info.EngineUsage.UsedMemory = Info.EngineUsage.BufferMemory + Info.EngineUsage.TextureMemory;

		if(Info.UsedMemory == 0)
			Info.UsedMemory = Info.EngineUsage.UsedMemory;

		return Info;
    }

	/*!
	 * @brief Creates a new texture from an image.
	 * 
	 * @param mgr: The ressource manger which manages the new texture.
	 * @param img: The image to create the texture from.
	 * 
	 * @attention Never call this function outside of the ressourcemanager.
	 */
	ITexture* COpenGLESDriver::CreateTexture(IResourcemanager *mgr, const CImage &img)
    {
		ITexture *Tex = new COpenGLESTexture(this, mgr, img);
		m_Textures.push_back(Tex);
		return Tex;
	}

	/*!
	 * @brief Creates an empty texture.
	 */
	ITexture *COpenGLESDriver::CreateTexture()
	{
		ITexture *Tex = new COpenGLESTexture(this, nullptr);
		m_Textures.push_back(Tex);
		return Tex;
	} 

    /*!
     * @brief Registers a new shader with its handler. See also ::IShaderHandler to see how to implement a shader.
     * 
     * @param Shaderhandler: The shaderhandler to register.
     * @return Returns a unique id for this shader.
     * 
     * @note The shader handler will be released from the shader manager.
     */
    uint32_t COpenGLESDriver::RegisterShader(IShaderHandler *Shaderhandler)
    {
        Shader shader = {Shaderhandler, 0};
		m_Shaders.push_back(shader);

		return m_Shaders.size() - 1;
    }

    /*!
     * @brief Applies the shader which should be used to render an object.
     * 
     * @param Shader: The shader id.
     * 
     * @throw std::invalid_argument Throws if the shader id doesn't exists.
     * @throw std::runtime_error Throws if the shader couldn't compile or link.
     */
    void COpenGLESDriver::ApplyShader(uint32_t Shader)
	{
		if(Shader > m_Shaders.size() - 1)
			throw std::invalid_argument("The given shader id doesn't exists. ID: " + std::to_string(Shader));

		struct Shader &shader = m_Shaders[Shader];

		//Load and compile the shaders.
		if(shader.GLProgram == 0)
		{
			std::string ShaderSrc;
			bool IsMem = false;

			shader.Handler->OnLoadShaders(ShaderSrc, IShaderManager::ShaderType::VERTEX, IsMem);

			if(!IsMem)
				ShaderSrc = ReadFile(ShaderSrc);

			GLuint Vertex, Fragment;

			try
			{
				Vertex = CreateShader(GL_VERTEX_SHADER, ShaderSrc);
			}
			catch(const std::runtime_error &e)
			{
				throw;
			}

			IsMem = false;
			ShaderSrc.clear();
			shader.Handler->OnLoadShaders(ShaderSrc, IShaderManager::ShaderType::FRAGMENT, IsMem);

			if(!IsMem)
				ShaderSrc = ReadFile(ShaderSrc);

			try
			{
				Fragment = CreateShader(GL_FRAGMENT_SHADER, ShaderSrc);
			}
			catch(const std::runtime_error &e)
			{
				glDeleteShader(Vertex);
				throw;
			}

			GLuint Program = glCreateProgram();
			glAttachShader(Program, Vertex);
			glAttachShader(Program, Fragment);

			std::string Coord, UV;

			shader.Handler->OnAttribLocationNameRequest(ATTRIBUTE_VERTEX_COORDS, Coord);
			shader.Handler->OnAttribLocationNameRequest(ATTRIBUTE_UV_COORDS, UV);

			//Todo: Make variable.
			glBindAttribLocation(Program, 0, Coord.c_str());
			glBindAttribLocation(Program, 1, UV.c_str());
			glLinkProgram(Program);

			glDeleteShader(Vertex);
			glDeleteShader(Fragment);

			GLint Link_OK = GL_TRUE;

			glGetProgramiv(Program, GL_LINK_STATUS, &Link_OK);
			if (Link_OK != GL_TRUE)
			{
				int Len;
				std::string Log;

				glGetProgramiv(Program, GL_INFO_LOG_LENGTH, &Len);
				Log.resize(Len);

				glGetProgramInfoLog(Program, Len, nullptr, &Log[0]);
				glDeleteProgram(Program);

				throw std::runtime_error(Log);
			}

			shader.GLProgram = Program;
		}

		m_CurrentAppliedShader = Shader;

		//GLuint CurrentProgram;
		//glGetIntegerv(GL_CURRENT_PROGRAM, (GLint*)&CurrentProgram);

		//if(CurrentProgram != shader.GLProgram)
			glUseProgram(shader.GLProgram);
	}

    /*!
     * @brief Sets the data for the given uniform id.
     */
    void COpenGLESDriver::SetUniform(uint32_t Uniform, const Vec3Df &vec)
    {
		glUniform3fv(GetUniformLocation(GetUniformName(Uniform)), 1, vec.v);
	}

    void COpenGLESDriver::SetUniform(uint32_t Uniform, const Vec2Df &vec)
    {
		glUniform2fv(GetUniformLocation(GetUniformName(Uniform)), 1, vec.v);
	}

    void COpenGLESDriver::SetUniform(uint32_t Uniform, const Matrix &m)
    {
		Matrix tmp = m;
		tmp.Transpose();

		glUniformMatrix4fv(GetUniformLocation(GetUniformName(Uniform)), 1, GL_FALSE, tmp.m);
	}

    void COpenGLESDriver::SetUniform(uint32_t Uniform, int value)
    {
		glUniform1i(GetUniformLocation(GetUniformName(Uniform)), value);
	}

    void COpenGLESDriver::SetUniform(uint32_t Uniform, const Color &color)
    {
		float Color[4];

		Color[0] = color.R / 255.f;
		Color[1] = color.G / 255.f;
		Color[2] = color.B / 255.f;
		Color[3] = color.A / 255.f;

		glUniform4fv(GetUniformLocation(GetUniformName(Uniform)), 1, Color);
	}

	GLint COpenGLESDriver::GetAttribLocation(uint32_t AttribID) const
    {
        if(m_CurrentAppliedShader != -1)
		{
			Shader shader = m_Shaders[m_CurrentAppliedShader];
			std::string Name;

			shader.Handler->OnAttribLocationNameRequest(AttribID, Name);

			return glGetAttribLocation(shader.GLProgram, Name.c_str());
		}

		return -1;
    } 

    std::string COpenGLESDriver::ReadFile(const std::string &Path)
	{
		std::string Ret;

		IFile *fd = IFile::Create(Path, IFile::Openmode::READ);
		if(fd->IsOpen())
		{
			while(!fd->IsEOF())
			{
				Ret = fd->ReadLn();
				Ret += "\n";
			}
		}

		delete fd;
		return Ret;
	}

	GLuint COpenGLESDriver::CreateShader(GLenum Type, const std::string &Source)
	{
		GLuint Shader = glCreateShader(Type);

		const char *Src = Source.c_str();
		glShaderSource(Shader, 1, (const GLchar**)&Src, nullptr);
		glCompileShader(Shader);

		GLint Compile_OK = GL_FALSE;
		glGetShaderiv(Shader, GL_COMPILE_STATUS, &Compile_OK);
		if(Compile_OK == GL_FALSE)
		{
			int Len;
			std::string Log;

			glGetShaderiv(Shader, GL_INFO_LOG_LENGTH, &Len);

			Log.resize(Len);

			glGetShaderInfoLog(Shader, Len, nullptr, &Log[0]);
			glDeleteShader(Shader);

			throw std::runtime_error("Shader failed to compile. OpenGL error: " + Log);
		}

		return Shader;
	}

	std::string COpenGLESDriver::GetUniformName(uint32_t Uniform)
	{
		Shader &shader = m_Shaders[m_CurrentAppliedShader];
		std::string Name;
		shader.Handler->OnUniformNameRequest(Uniform, Name);

		return Name;
	}

	GLint COpenGLESDriver::GetUniformLocation(std::string Name)
	{
		Shader &shader = m_Shaders[m_CurrentAppliedShader];
		return glGetUniformLocation(shader.GLProgram, Name.c_str());
	}

}