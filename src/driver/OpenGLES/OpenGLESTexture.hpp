/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef OPENGLESTEXTURE_HPP_
#define OPENGLESTEXTURE_HPP_

#include <math/Vector2D.hpp>
#include <driver/ITexture.hpp>
#include <resource/IResourcemanager.hpp>
#include "OpenGLESDriver.hpp"

namespace DivineAPI
{
    class COpenGLESTexture : public ITexture
	{
		public:
			COpenGLESTexture();

			COpenGLESTexture(COpenGLESDriver *Driver, IResourcemanager *mgr, const CImage &img);

			COpenGLESTexture(COpenGLESDriver *Driver, IResourcemanager *mgr);

            /*!
             * @brief Creates an empty texture.
             * 
             * @param Size: Size of the texture.
             * @param Format: Texture format. See also CImage::Pixelformat.
             */
            void CreateEmptyTexture(Vec2Di Size, CImage::Pixelformat Format);

            /*!
             * @brief Copies a subtexture to a specific position.
             * 
             * @param img: Image to copy.
             * @param Pos: Position to insert.
             */
            void CopySubTexture(const CImage &img, Vec2Di Pos);

            /*!
             * @brief Resizes the texture without scale the current data.
             */
            void ResizeTexture(Vec2Di Size);

            /*!
             * @return Gets the image which is stored in this texture.
             */
            CImage GetImage(); 

            /*!
             * @brief Binds this texture.
             */
            void BindTexture();

            /*!
             * @brief Unbinds this texture.
             */
			void UnbindTexture();

            /*!
             * @return Gets the size of the texture.
             */
			Vec2Di TextureSize() const
			{
				return m_TextureSize;
			}

            /*!
             * @return Gets the size in bytes which is used by this texture.
             */
            uint64_t Size() const
			{
				return m_Size;
			}

            /*!
             * @return Gets a unique id.
             */
            uint32_t GetID() const
			{
				return m_TextureID;
			}

			virtual ~COpenGLESTexture()
			{
				FreeTexture();
			}
		protected:
			GLuint m_TextureID;
			GLuint m_CurrentBoundTexture;
			Vec2Di m_TextureSize;
			COpenGLESDriver *m_Driver;
			CImage::Pixelformat m_Format;

			uint64_t m_Size;

			static GLint ConvertPixelformatToGLFormat(CImage::Pixelformat Format);
		private:
			void FreeTexture();
	};
}

#endif