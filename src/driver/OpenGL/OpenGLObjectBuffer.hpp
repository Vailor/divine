/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef OPENGLOBJECTBUFFER_HPP
#define OPENGLOBJECTBUFFER_HPP

#include <driver/IDriver.hpp>
#include <string.h>
#include "OpenGLDriver.hpp"

namespace DivineAPI
{
	class COpenGLObjectBuffer : public IObjectBuffer
	{
		public:
            explicit COpenGLObjectBuffer(COpenGLDriver *Driver);

			/*!
			 * @brief Sets the render mode of this buffer. See also RenderModes.
			 * 
			 * @param Mode: The render mode.
 			 */
			void SetRenderMode(IObjectBuffer::RenderModes Mode)
            {
                m_Mode = Mode;
            }
			
			/*!
			 * @brief Uploads or updates vertices of this buffer.
			 * 
			 * @param Vertices: Vertices to upload.
			 * @param Attribute: The vertex attribute to update or upload. See also VertexAttributes.
			 * 
			 * @remarks If an attribute is to be update, care must be taken that the update buffer has the same size as the already existing buffer.
			 * @remark An exception to this is the update of all attributes that exists in this buffer.
			 * 
			 * @throw std::runtime_error Throws only if the first remark occurs.
			 */
			void UploadVertices(const std::vector<Vertex> &Vertices, VertexAttributes Attribute);

			/*!
			 * @brief Uploads or updates the indices.
			 * 
			 * @param Indices: The new indices for this buffer.
			 * @param ObjIndices: The count of vertices per object.
			 */
			void UploadIndices(const std::vector<unsigned short> &Indices, const std::vector<int> &ObjIndices = std::vector<int>());

			/*!
			 * @brief Renders the object buffer.
			 */
			void Render();

			/*!
			 * @brief Deletes this buffer on the gpu side.
			 */
			void ClearBuffer();

			/*!
			 * @return Gets the size in bytes of this buffer.
			 */
			uint64_t Size() const
			{
				return (m_Size + m_Indices * sizeof(unsigned short));
			}

			/*!
			 * @return Gets the total count of triangles in this buffer.
			 */
			uint64_t TrisCount() const
			{
				return m_TrisCount;
			}

			/*!
			 * @return Gets the vertex count of this buffer.
			 */
			uint64_t VertexCount() const
			{
				return m_VertexCount;
			}

			/*!
			 * @brief Deletes this object. You must use this to free the object instead of the delete operator.
			 */
			void Release();

            virtual ~COpenGLObjectBuffer();

        private:
            IObjectBuffer::RenderModes m_Mode;
            COpenGLDriver *m_Driver;

			uint64_t m_Size, m_Indices, m_TrisCount, m_VertexCount;
			VertexAttributes m_Attribute;

			std::vector<int> m_IndicesPerObj;

            GLuint m_VAO;
            GLuint m_VBO, m_IndicesBuffer;
	};
}

#endif