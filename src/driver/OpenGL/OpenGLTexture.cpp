/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "OpenGLTexture.hpp"
#include <utils/Log.hpp>

namespace DivineAPI
{
	COpenGLTexture::COpenGLTexture() : ITexture(nullptr)
	{
		m_TextureID = 0;
		m_TextureSize = Vec2Di();
		m_Driver = nullptr;
		m_CurrentBoundTexture = 0;
		m_Size = 0;
		m_Format = CImage::Pixelformat::UNKNOWN;
	}

	COpenGLTexture::COpenGLTexture(COpenGLDriver *Driver, IResourcemanager *mgr, const CImage &img) : COpenGLTexture(Driver, mgr)
	{
		CreateEmptyTexture(img.GetSize(), img.GetPixelformat());
		CopySubTexture(img, Vec2Di());
	}

	COpenGLTexture::COpenGLTexture(COpenGLDriver *Driver, IResourcemanager *mgr) : COpenGLTexture()
	{
		m_Rmgr = mgr;
		m_Driver = Driver;
	}

	/*!
	 * @brief Creates an empty texture.
	 * 
	 * @param Size: Size of the texture.
	 * @param Format: Texture format. See also CImage::Pixelformat.
	 */
	void COpenGLTexture::CreateEmptyTexture(Vec2Di Size, CImage::Pixelformat Format)
	{
		FreeTexture();

		m_TextureSize = Size;
		m_Size = m_TextureSize.x * m_TextureSize.y * (uint64_t)CImage::GetBytesPerPixel(Format);
		m_Format = Format;

		glGenTextures(1, &m_TextureID);
		BindTexture();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

		int StorageFormat = GL_RGBA;

		if(Format == CImage::Pixelformat::ALPHA_8)
			StorageFormat = GL_RED;

		//Loads the pixel data to the gpu.
		glTexImage2D(GL_TEXTURE_2D, 0, StorageFormat, Size.x, Size.y, 0, ConvertPixelformatToGLFormat(Format), GL_UNSIGNED_BYTE, nullptr);
		UnbindTexture();
	}

	/*!
	 * @brief Copies a subtexture to a specific position.
	 * 
	 * @param img: Image to copy.
	 * @param Pos: Position to insert.
	 */
	void COpenGLTexture::CopySubTexture(const CImage &img, Vec2Di Pos)
	{
		BindTexture();
		glTexSubImage2D(GL_TEXTURE_2D, 0, Pos.x, Pos.y, img.GetSize().x, img.GetSize().y, ConvertPixelformatToGLFormat(img.GetPixelformat()), GL_UNSIGNED_BYTE, img.GetPixels());
		UnbindTexture();
	}

	/*!
	 * @brief Resizes the texture without scale the current data.
	 */
	void COpenGLTexture::ResizeTexture(Vec2Di Size)
	{
		CImage img = GetImage();

		CreateEmptyTexture(Size, m_Format);
		CopySubTexture(img, Vec2Di());
	}

    /*!
     * @return Gets the image which is stored in this texture.
     */
	CImage COpenGLTexture::GetImage()
	{
		CImage img;
		img.CreateEmptyImage(m_TextureSize, m_Format);

		BindTexture();

		int StorageFormat = GL_RGBA;

		if(m_Format == CImage::Pixelformat::ALPHA_8)
			StorageFormat = GL_RED;

		glGetTexImage(GL_TEXTURE_2D, 0, StorageFormat, GL_UNSIGNED_BYTE, img.GetPixels());

		UnbindTexture();

		return img;
	}

    /*!
     * @brief Binds this texture.
     */
	void COpenGLTexture::BindTexture()
	{
		glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*)&m_CurrentBoundTexture);
		glBindTexture(GL_TEXTURE_2D, m_TextureID);
	}

    /*!
     * @brief Unbinds this texture.
     */
	void COpenGLTexture::UnbindTexture()
	{
		glBindTexture(GL_TEXTURE_2D, m_CurrentBoundTexture);
	}

	GLint COpenGLTexture::ConvertPixelformatToGLFormat(CImage::Pixelformat Format)
	{
		switch(Format)
		{
			case CImage::Pixelformat::ALPHA_8:
			{
				return GL_RED;
			}break;

			case CImage::Pixelformat::RGB_8_8_8:
			{
				return GL_RGB;
			}break;

			case CImage::Pixelformat::RGBA_8_8_8_8:
			{
				return GL_RGBA;
			}break;

			case CImage::Pixelformat::BGR_8_8_8:
			{
				return GL_BGR;
			}break;

			case CImage::Pixelformat::BGRA_8_8_8_8:
			{
				return GL_BGRA;
			}break;
		}
	}

	void COpenGLTexture::FreeTexture()
	{
		if(m_TextureID != 0)
			glDeleteTextures(1, &m_TextureID);
			
		m_TextureID = 0;
	}
} 

