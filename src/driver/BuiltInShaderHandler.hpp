/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef BUILTINSHADERHANDLER_HPP
#define BUILTINSHADERHANDLER_HPP

#include <driver/IShaderManager.hpp>
#include "../resource/resources.h"

namespace DivineAPI
{
    class BuiltInSpriteShaderHandler : public IShaderHandler
    {
        public:
            BuiltInSpriteShaderHandler() {}

            /*!
             * @brief Called if the shader is loaded the first time.
             * 
             * @param Path: The path or the shader data.
             * @param Type: Requested shader.
             * 
             * @param IsMemory: If is set to true, the path variable is interpreted as the shader data.
             */
            void OnLoadShaders(std::string &Path, IShaderManager::ShaderType Type, bool &IsMemory)
            {
                IsMemory = true;

                switch(Type)
                {
                    case IShaderManager::ShaderType::VERTEX:
                    {
                        Path = spritevs;
                    }break;

                    case IShaderManager::ShaderType::FRAGMENT:
                    {
                        Path = spritefs;
                    }break;
                }
            }

            /*!
             * @brief Called if a uniform name is required.
             * 
             * @param ReqUniform: The id which identify the requested uniform.
             * @param Name: The name of the uniform.
             */
            void OnUniformNameRequest(uint32_t ReqUniform, std::string &Name)
            {
                switch(ReqUniform)
                {
                    case UNIFORM_TRANSLATION:
                    {
                        Name = "Translation";
                    }break;

                    case UNIFORM_ROTATION:
                    {
                        Name = "Rotation";
                    }break;

                    case UNIFORM_SCALE:
                    {
                        Name = "Scale";
                    }break;

                    case UNIFORM_PROJECTION:
                    {
                        Name = "Projection";
                    }break;
                }
            }

            /*!
             * @brief Called if an attribute name is required.
             * 
             * @param ReqAttrib: The id which identify the requested attribute.
             * @param Name: The name of the attribute.
             */
            void OnAttribLocationNameRequest(uint32_t ReqAttrib, std::string &Name)
            {
                switch(ReqAttrib)
                {
                    case ATTRIBUTE_VERTEX_COORDS:
                    {
                        Name = "Coord";
                    }break;

                    case ATTRIBUTE_UV_COORDS:
                    {
                        Name = "UVCoord";
                    }break;
                }
            }

            virtual ~BuiltInSpriteShaderHandler() {}
    };

    class BuiltInTextShaderHandler : public IShaderHandler
    {
        public:
            BuiltInTextShaderHandler() {}

            /*!
             * @brief Called if the shader is loaded the first time.
             * 
             * @param Path: The path or the shader data.
             * @param Type: Requested shader.
             * 
             * @param IsMemory: If is set to true, the path variable is interpreted as the shader data.
             */
            void OnLoadShaders(std::string &Path, IShaderManager::ShaderType Type, bool &IsMemory)
            {
                IsMemory = true;

                switch(Type)
                {
                    case IShaderManager::ShaderType::VERTEX:
                    {
                        Path = textvs;
                    }break;

                    case IShaderManager::ShaderType::FRAGMENT:
                    {
                        Path = textfs;
                    }break;
                }
            }

            /*!
             * @brief Called if a uniform name is required.
             * 
             * @param ReqUniform: The id which identify the requested uniform.
             * @param Name: The name of the uniform.
             */
            void OnUniformNameRequest(uint32_t ReqUniform, std::string &Name)
            {
                switch(ReqUniform)
                {
                    case UNIFORM_TRANSLATION:
                    {
                        Name = "Translation";
                    }break;

                    case UNIFORM_ROTATION:
                    {
                        Name = "Rotation";
                    }break;

                    case UNIFORM_SCALE:
                    {
                        Name = "Scale";
                    }break;

                    case UNIFORM_PROJECTION:
                    {
                        Name = "Projection";
                    }break;

                    case UNIFORM_COLOR:
                    {
                        Name = "TextColor";
                    }break;
                }
            }

            /*!
             * @brief Called if an attribute name is required.
             * 
             * @param ReqAttrib: The id which identify the requested attribute.
             * @param Name: The name of the attribute.
             */
            void OnAttribLocationNameRequest(uint32_t ReqAttrib, std::string &Name)
            {
                switch(ReqAttrib)
                {
                    case ATTRIBUTE_VERTEX_COORDS:
                    {
                        Name = "Coord";
                    }break;

                    case ATTRIBUTE_UV_COORDS:
                    {
                        Name = "UVCoord";
                    }break;
                }
            }

            virtual ~BuiltInTextShaderHandler() {}
    };
}

#endif