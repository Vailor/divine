/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <driver/IDriver.hpp>
#include <platform.h>
#include <stdexcept>

#if (defined(DIVINE_WINDOWS) || defined(DIVINE_UNIX)) && !defined(DIVINE_ANDROID)
	#include "OpenGL/OpenGLDriver.hpp"
#elif defined(DIVINE_ANDROID)
	#include "OpenGLES/OpenGLESDriver.hpp"
#endif

namespace DivineAPI
{
	/*!
	 * @brief Creates a driver for the current platform.
	 * 
	 * @return Returns a driver.
	 * @throw std::runtime_error Throws if no driver for this platform exists.
	 */
	IDriver *IDriver::CreateDriver()
	{
#if (defined(DIVINE_WINDOWS) || defined(DIVINE_UNIX)) && !defined(DIVINE_ANDROID)
		return new COpenGLDriver();
#elif defined(DIVINE_ANDROID)
		return new COpenGLESDriver();
#else
		#error "Platform is not supported!";
#endif
	}
}


