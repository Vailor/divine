/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <scenemanager/SceneNode.hpp>
#include <scenemanager/SceneManager.hpp>
#include <math.h>

namespace DivineAPI
{
	/*!
	 * @param smgr: The scenemanager which should be managed this node.
	 * @param Parent: The parent node of this node.
	 * @param ZOrder: The rendering order. Negative values will be rendered first and positive values are rendered at last.
	 */
	CSceneNode::CSceneNode(CSceneManager *smgr, CSceneNode *Parent, int ZOrder)
	{
		m_SceneManager = smgr;
		m_Parent = Parent;
		m_ZOrder = ZOrder;
		m_Visible = true;
        
		if(m_Parent)
			m_Parent->AddNode(this, m_ZOrder);
	}

	/*!
	 * @brief Sets the parent of this CSceneNode.
	 * 
	 * @param Parent: The new parent Scene.
	 */
	void CSceneNode::SetParent(CSceneNode *Parent)
	{
		if(m_Parent)
		{
			m_Parent->RemoveNode(this);
			UpdateWorldCoordinate(Vec2Df());
		}

		m_Parent = Parent;
		if(m_Parent)
			m_Parent->AddNode(this, m_ZOrder);
	}

	/*!
	 * @brief Adds a new child node to this node.
	 * 
	 * @param Node: Child to add.
	 * @param ZOrder: The rendering order. Negative values will be rendered first and positive values are rendered at last.
	 */
	void CSceneNode::AddNode(CSceneNode *Node, int ZOrder)
	{
		Node->m_ZOrder = ZOrder;
		Node->m_Parent = this;
		Node->UpdateWorldCoordinate(m_WorldCoordinate);

		if(ZOrder < 0)
			AddNodeInternal(m_Left, Node, ZOrder);
		else
			AddNodeInternal(m_Right, Node, ZOrder);
	}

	void CSceneNode::UpdateWorldCoordinate(Vec2Df ParentWorldCoordinate)
	{
		m_WorldCoordinate = ParentWorldCoordinate + Pos;
		m_ParentWorldCoordinate = ParentWorldCoordinate;

		for(size_t i = 0; i < m_Left.size(); i++)
		{
			m_Left[i]->UpdateWorldCoordinate(m_WorldCoordinate);
		}

		for(size_t i = 0; i < m_Right.size(); i++)
		{
			m_Right[i]->UpdateWorldCoordinate(m_WorldCoordinate);
		}
	}

	void CSceneNode::AddNodeInternal(std::vector<CSceneNode*> &Side, CSceneNode *Node, int ZOrder)
	{
		if(Side.empty())
		{
			Side.push_back(Node);
			return;
		}

		for (int i = Side.size() - 1; i >= 0; i--)
		{
			if(abs(ZOrder) > abs(Side[i]->GetZOrder()))
			{
				int Pos = i + 1;
				Side.insert(Side.begin() + Pos, Node);
				break;
			}
			else if(abs(ZOrder) == abs(Side[i]->GetZOrder()))
			{
				Side.insert(Side.begin() + i + 1, Node);
				break;
			}
			else if(i == 0)
			{
				Side.insert(Side.begin(), Node);
				break;
			}
		}
	}

	/*!
	 * @brief Removes a child node.
	 * 
	 * @param Node: The child to remove.
	 */
	void CSceneNode::RemoveNode(CSceneNode *Node)
	{
		if(Node->GetZOrder() < 0)
			RemoveNodeInternal(m_Left, Node);
		else
			RemoveNodeInternal(m_Right, Node);
	}

	void CSceneNode::RemoveNodeInternal(std::vector<CSceneNode*> &Side, CSceneNode *Node)
	{
		for(size_t i = 0; i < Side.size(); i++)
		{
			if(Side[i] == Node)
			{
				Side.erase(Side.begin() + i);
				break;
			}
		}
	}

	/*!
	 * @brief Specifies the order in which the element should be rendered.
	 * @param ZOrder: The rendering order. Negative values will be rendered first and positive values are rendered at last.
	 */
	void CSceneNode::SetZOrder(int ZOrder)
	{
		if(m_Parent)
		{
			m_Parent->RemoveNode(this);
			m_Parent->AddNode(this, ZOrder);
		}
	}

	/*!
	 * @brief For the logic of inherited nodes.
	 */
	void CSceneNode::Update()
	{
		for(size_t i = 0; i < m_Left.size(); i++)
		{
			m_Left[i]->Update();
		}

		for(size_t i = 0; i < m_Right.size(); i++)
		{
			m_Right[i]->Update();
		}
	}

	/*!
	 * @brief Draws this node with all its children.
	 */
	void CSceneNode::Render()
	{
		for(size_t i = 0; i < m_Left.size(); i++)
		{
			if(m_Left[i]->IsVisible())
				m_Left[i]->Render();
		}

		for(size_t i = 0; i < m_Right.size(); i++)
		{
			if(m_Right[i]->IsVisible())
				m_Right[i]->Render();
		}
	}

	/*!
	 * @brief Cleanups all child nodes.
	 */
	void CSceneNode::Cleanup()
	{
		std::vector<CSceneNode*>::iterator IT = m_Left.begin();
		while(IT != m_Left.end())
		{
			CSceneNode *Node = (*IT);
			IT = m_Left.erase(IT);

			Node->Cleanup();
			delete Node;
		}

		IT = m_Right.begin();
		while(IT != m_Right.end())
		{
			CSceneNode *Node = (*IT);
			IT = m_Right.erase(IT);

			Node->Cleanup();
			delete Node;
		}
	}

	/*!
	 * @return Returns the local matrix of this node.
	 */
	Matrix CSceneNode::GetLocalMatrix()
	{
		Matrix Local;
		Local = Local * Translate(Vec3Di((int)Pos.x, (int)Pos.y, 0));
		Local = Local * Translate(Vec3Df(0.5f * m_Size.x, 0.5f * m_Size.y, 0.f));
		Local = Local * m_Rotation;
		Local = Local * Translate(Vec3Df(-0.5f * m_Size.x, -0.5f * m_Size.y, 0.f));
		Local = Local * m_Scale;

		return Local;
	}

	/*!
	 * @brief Rotates a node.
	 * 
	 * @param Angle: The angle to rotate.
	 * @param Axis: The axis to rotate around.
	 */
	void CSceneNode::Rotate(float Angle, Vec3Df Axis)
	{
		m_Rotation = DivineAPI::Rotate(Angle, Axis);
	}

	/*!
	 * @brief Scales the node.
	 */
	void CSceneNode::Scale(Vec2Df ScaleVal)
	{
		m_Scale = DivineAPI::Scale(Vec3Df(ScaleVal));
	}

	void CSceneNode::UpdateWorldMatrix(Matrix World)
	{
		for(size_t i = 0; i < m_Left.size(); i++)
			m_Left[i]->UpdateWorldMatrix(World * GetLocalMatrix());

		for(size_t i = 0; i < m_Right.size(); i++)
			m_Right[i]->UpdateWorldMatrix(World * GetLocalMatrix());

		m_World = World;
	}

	CSceneNode::~CSceneNode()
	{
		Cleanup();
		
		if(m_Parent)
			m_Parent->RemoveNode(this);
	}
}  // namespace DivineAPI

