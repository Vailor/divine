/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <timer/BaseTimer.hpp>
#include <SDL2/SDL.h>
#include <engine/Engine.hpp>

namespace DivineAPI
{
	/*!
	 * @brief Starts the timer.
	 */
	void CBaseTimer::Start()
	{
		if(!IsRunning())
		{
			m_Running = true;
			m_Start = SDL_GetTicks();

			Divine::RegisterTimer(this);
		}
	}

	/*!
	 * @brief Resets the start and elapsed time, without stopping the timer.
	 */
	void CBaseTimer::Reset()
	{
		m_Start = SDL_GetTicks();
		m_Elapsed = 0.f;
	}

	/*!
	 * @brief Stops the timer.
	 */
	void CBaseTimer::Stop()
	{
		m_Running = false;
		Divine::RemoveTimer(this);
	}

	/*!
	 * @brief Updates the timer.
	 * 
	 * @attention Is called by the engine main loop.
	 */
	void CBaseTimer::Update()
	{
		m_Elapsed = (SDL_GetTicks() - m_Start) / 1000.f;
		m_Start = SDL_GetTicks();
	}
}  // namespace DivineAPI

