/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef WINDOW_HPP_
#define WINDOW_HPP_

#include <SDL2/SDL.h>
#include <vector>

#include <window/IWindow.hpp>

namespace DivineAPI
{
	class Window : public IWindow
	{
		public:
			Window() 
            {
                m_Driver = nullptr;
                m_Window = nullptr;
                m_Scmgr = nullptr;
                m_Rmgr = nullptr;
                m_IsFullscreen = false;
                m_Closed = true;
                m_Context = nullptr;
            }

            /*!
             * @brief Creates a window.
             * 
             * @param Title: Window title.
			 * @param Pos: Position of the window.
			 * @param Size: Size of the window.
             * @param Flags: The window flags. See also WindowFlags.
             * 
             * @throw std::runtime_error Throws if an error occures.
             */
			Window(const std::string &Title, Vec2Di Pos, Vec2Dui Size, IWindow::WindowFlags Flags) : Window()
			{
				Create(Title, Pos, Size, Flags);
			}

            /*!
             * @brief Creates a window.
             * 
             * @param Title: Window title.
			 * @param Pos: Position of the window.
			 * @param Size: Size of the window.
             * @param Flags: The window flags. See also WindowFlags.
             * 
             * @throw std::runtime_error Throws if an error occures.
             */
			void Create(std::string Title, Vec2Di Pos, Vec2Dui Size, IWindow::WindowFlags Flags);

            /*!
             * @brief Sets the visibility state of the window.
             * 
             * @param show: If true the window is visible otherwise the window is hidden.
             */
			void Show(bool show = true);

			/*!
             * @brief Close the window.
             */
			void Close();

            /*!
             * @brief Destorys the window.
             */
			void Destroy();

			/*!
             * @brief Adds an input handler for this window.
			 * 
             * @throw std::invalid_argument Throws if the handler is null.
             */
			void AddInputHandler(IInputHandler *Handler);

            /*!
             * @brief Removes an input handler.
			 * 
             * @throw std::invalid_argument Throws if the handler is null.
             */
			void RemoveInputHandler(IInputHandler *Handler);

            /*!
             * @throw std::runtime_error Throws if fullscreen mode failed
             */
			void SetFullscreen(bool Fullscreen);

			/*!
             * @brief Sets the minimum size of the window.
             */
			void SetMinSize(Vec2Di MinSize);

			/*!
             * @brief Sets the maximum size of the window.
             */
			void SetMaxSize(Vec2Di MaxSize);

			/*!
             * @brief Sets the size of the window.
             */
            void SetWindowSize(Vec2Dui Size);

			/*!
             * @brief Sets the title of the window.
             */
            void SetWindowTitle(std::string Title);

            /*!
             * @return Gets the title of the window.
             */
            std::string GetWindowTitle();

            /*!
             * @return Gets the minimum size of the window.
             */
			Vec2Di GetMinSize();

            /*!
             * @return Gets the maximum size of the window.
             */
			Vec2Di GetMaxSize();
            
            /*!
             * @brief Binds the current render context.
             */
            void BindContext();
            
            /*!
             * @return Gets the current window size.
             */
            Vec2Di GetWindowSize();

			/*!
			 * @brief Enables vsync. 
			 */
			void EnableVSync(bool Enable);

			/*!
             * @brief Sets the background color of the window.
             */
			void SetBackgroundColor(Color Color)
			{
				m_Color = Color;
			}

			/*!
             * @return Gets the current used driver for rendering objects on the screen.
             */
            IDriver *GetDriver()
			{
				return m_Driver;
			}
			
			/*!
             * @return Gets the background color.
             */
			Color GetBackgroundColor()
			{
				return m_Color;
			}

			/*!
             * @brief Checks if the window is closed.
             */ 
			bool IsClosed()
			{
				return m_Closed;
			}

			/*!
             * @brief Checks if the window is in fullscreen mode.
             */ 
			bool IsFullscreen()
			{
				return m_IsFullscreen;
			}

            /*!
             * @return Gets the scene manager.
             */
			CSceneManager *GetSceneManager()
			{
				return m_Scmgr;
			}

            /*!
             * @return Gets the ressource manager.
             */
			IResourcemanager *GetResourceManager()
			{
				return m_Rmgr;
			}

            /*!
             * @return Gets the shader manager.
             */
            IShaderManager *GetShaderManager()
			{
				return dynamic_cast<IShaderManager*>(m_Driver);
			}

            /*!
             * @return Gets the window id of this window.
			 * 
             * @throw std::runtime_error Thrown if the window is not initialized.
             */ 
			uint32_t GetWindowID()
			{
				if(!m_Window)
					throw std::runtime_error("Error couldn't get window id, because window is null!");

				return SDL_GetWindowID(m_Window);
			}
			
            virtual ~Window()
			{
				Destroy();
			}

		protected:
			Color m_Color;

			std::vector<IInputHandler*> m_Handlers;

			SDL_Window *m_Window;

			Matrix m_Ortho;
			CSceneManager *m_Scmgr;
			IResourcemanager *m_Rmgr;

			bool m_IsFullscreen;

		private:
            /*!
             * @brief Called by the engine main loop.
             */
			void Run();

			void ProcessResize(Vec2Dui Size);
			void ProcessKeyboardEvent(CKeyboardEvent ev);
			void ProcessMouseEvent(CMouseEvent ev);

			bool m_Closed;

			SDL_GLContext m_Context;
			IDriver *m_Driver;
	};
}  // namespace DivineAPI

#endif /* WINDOW_HPP_ */
