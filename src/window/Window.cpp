/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "Window.hpp"
#include "../resource/resources.h"
#include "../resource/Resourcemanager.hpp"
#include "../driver/BuiltInShaderHandler.hpp"
#include "Windowmanager.hpp"
#include <scenemanager/SceneManager.hpp>
#include <utils/Log.hpp>
#include <stdexcept>
#include <exception>

namespace DivineAPI
{
    /*!
     * @brief Creates a window.
     * 
     * @param Title: Window title.
	 * @param Pos: Position of the window.
	 * @param Size: Size of the window.
     * @param Flags: The window flags. See also WindowFlags.
     * 
     * @throw std::runtime_error Throws if an error occures.
     */
	void Window::Create(std::string Title, Vec2Di Pos, Vec2Dui Size, IWindow::WindowFlags Flags)
	{
		if(m_Window)
		{
			LOGE("Window already created!");
			throw std::runtime_error(std::string("Window already created!"));
		}

		Uint32 SDLFlags = SDL_WINDOW_OPENGL; 

		if((Flags & IWindow::WindowFlags::HIDDEN) == IWindow::WindowFlags::HIDDEN)
			SDLFlags |= SDL_WINDOW_HIDDEN;

		if((Flags & IWindow::WindowFlags::POPUP) == IWindow::WindowFlags::POPUP)
			SDLFlags |= SDL_WINDOW_BORDERLESS;

		if((Flags & IWindow::WindowFlags::MINIMIZED) == IWindow::WindowFlags::MINIMIZED)
			SDLFlags |= SDL_WINDOW_MINIMIZED;

		if((Flags & IWindow::WindowFlags::MAXIMIZED) == IWindow::WindowFlags::MAXIMIZED)
			SDLFlags |= SDL_WINDOW_MAXIMIZED;

		if((Flags & IWindow::WindowFlags::RESIZEABLE) == IWindow::WindowFlags::RESIZEABLE)
			SDLFlags |= SDL_WINDOW_RESIZABLE;

		//Creates the SDL window.
		m_Window = SDL_CreateWindow(Title.c_str(), Pos.x, Pos.y, (int) Size.x, (int) Size.y, SDLFlags);
		if (!m_Window)
			throw std::runtime_error(std::string("SDL_CreateWindow() failed: ") + std::string(SDL_GetError()));

		LOGI("Window created.");

		//Configures the OpenGL context.
#ifndef DIVINE_ANDROID
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
#else
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#endif

		//Creates the context.
		m_Context = SDL_GL_CreateContext(m_Window);
		if(!m_Context)
		{
			SDL_DestroyWindow(m_Window);
			m_Window = nullptr;

			throw std::runtime_error(std::string("SDL_CreateWindow() failed: ") + std::string(SDL_GetError()));
		}

		LOGI("OpenGL context created.");

		try
		{
			m_Driver = IDriver::CreateDriver();
		} 
		catch(std::runtime_error &e) 
		{
			SDL_GL_DeleteContext(m_Context);
			SDL_DestroyWindow(m_Window);

			if(m_Driver)
			{
				delete m_Driver;
				m_Driver = nullptr;
			}

			throw;
		}

		LOGI("Driver created.");

		dynamic_cast<IShaderManager*>(m_Driver)->RegisterShader(new BuiltInSpriteShaderHandler());
		dynamic_cast<IShaderManager*>(m_Driver)->RegisterShader(new BuiltInTextShaderHandler());

		m_Color = Color(0, 0, 0, 255);
		m_Ortho = Ortho(0, Size.x, Size.y, 0, 1, -1);

		RegisterWindow(this);

		m_Scmgr = new CSceneManager(this);
		m_Rmgr = new CResourcemanager(this);
	}

    /*!
     * @brief Sets the visibility state of the window.
     * 
     * @param show: If true the window is visible otherwise the window is hidden.
     */
	void Window::Show(bool show)
	{
		if (show)
			SDL_ShowWindow(m_Window);
		else
			SDL_HideWindow(m_Window);
	}

    /*!
     * @brief Close the window.
     */
	void Window::Close()
	{
		Show(false);
	}

    /*!
     * @brief Destorys the window.
     */
	void Window::Destroy()
	{
		UnregisterWindow(this, true);

		if(m_Scmgr)
		{
			delete m_Scmgr;
			m_Scmgr = nullptr;
		}

		if(m_Rmgr)
		{
			delete m_Rmgr;
			m_Rmgr = nullptr;
		}

		if(m_Driver)
		{
			delete m_Driver;
			m_Driver = nullptr;
		}

		SDL_GL_DeleteContext(m_Context);
		SDL_DestroyWindow(m_Window);

		m_Context = nullptr;
		m_Window = nullptr;
	}

    /*!
     * @brief Adds an input handler for this window.
     
	 * @throw std::invalid_argument Throws if the handler is null.
     */
	void Window::AddInputHandler(IInputHandler *Handler)
	{
        if(Handler)
            m_Handlers.push_back(Handler);
        else
        {
            LOGE("Thrown exception std::invalid_argument. Line: " + std::to_string(__LINE__) + " Function: " + std::string(__FUNCTION__));
            throw std::invalid_argument(std::string(__FUNCTION__) + ": Given input handler is null.");
        }
	}

    /*!
     * @brief Removes an input handler.
	 * 
     * @throw std::invalid_argument Throws if the handler is null.
     */
	void Window::RemoveInputHandler(IInputHandler *Handler)
	{
        if(!Handler)
        {
            LOGE("Thrown exception std::invalid_argument. Line: " + std::to_string(__LINE__) + " Function: " + std::string(__FUNCTION__));
            throw std::invalid_argument(std::string(__FUNCTION__) + ": Given input handler is null.");
        }
        
		for (size_t i = 0; i < m_Handlers.size(); ++i)
		{
			if (m_Handlers[i] == Handler)
				m_Handlers.erase(m_Handlers.begin() + i);
		}
	}

    /*!
     * @throw std::runtime_error Throws if fullscreen mode failed
     */	
	void Window::SetFullscreen(bool Fullscreen)
	{
		Uint32 Mode = 0;
		m_IsFullscreen = Fullscreen;

		if(Fullscreen)
			Mode = SDL_WINDOW_FULLSCREEN;

		if(SDL_SetWindowFullscreen(m_Window, Mode) < 0)
        {
            LOGE("Thrown exception std::invalid_argument. Line: " + std::to_string(__LINE__) + " Function: " + std::string(__FUNCTION__));
            throw std::runtime_error(std::string(SDL_GetError()));
        }
	}

    /*!
     * @brief Sets the minimum size of the window.
     */
	void Window::SetMinSize(Vec2Di MinSize)
	{
		SDL_SetWindowMinimumSize(m_Window, MinSize.x, MinSize.y);
	}

    /*!
     * @brief Sets the maximum size of the window.
     */
	void Window::SetMaxSize(Vec2Di MaxSize)
	{
		SDL_SetWindowMaximumSize(m_Window, MaxSize.x, MaxSize.y);
	}

	/*!
	 * @brief Sets the size of the window.
	 */
	void Window::SetWindowSize(Vec2Dui Size)
	{
		SDL_SetWindowSize(m_Window, Size.x, Size.y);
		ProcessResize(Size);
	}

	/*!
	 * @brief Sets the title of the window.
	 */
	void Window::SetWindowTitle(std::string Title)
	{
		SDL_SetWindowTitle(m_Window, Title.c_str());
	}

	/*!
	 * @return Gets the title of the window.
	 */
	std::string Window::GetWindowTitle()
	{
		return std::string(SDL_GetWindowTitle(m_Window));
	}
	
    /*!
     * @return Gets the minimum size of the window.
     */
	Vec2Di Window::GetMinSize()
	{
		int w, h;
		SDL_GetWindowMinimumSize(m_Window, &w, &h);

		return Vec2Di(w, h);
	}
	
    /*!
     * @return Gets the maximum size of the window.
     */
	Vec2Di Window::GetMaxSize()
	{
		int w, h;
		SDL_GetWindowMaximumSize(m_Window, &w, &h);

		return Vec2Di(w, h);
	}

    /*!
     * @brief Called by the engine main loop.
     */
	void Window::Run()
	{
		BindContext();
		/*if(m_Program.Use() == -1)
			throw std::runtime_error("Shader linker: " +  m_Program.GetLinkError());*/

		m_Driver->Clear(m_Color);

		m_Scmgr->UpdateWorldMatrix(m_Ortho);
		m_Scmgr->Update();
		m_Scmgr->Render();

		SDL_GL_SwapWindow(m_Window);
	}

	void Window::ProcessResize(Vec2Dui Size)
	{
		BindContext();
		m_Ortho = Ortho(0, Size.x, Size.y, 0, 1, -1);

		m_Driver->SetViewport(Vec2Di(), Size);
	}

	void Window::ProcessKeyboardEvent(CKeyboardEvent ev)
	{
		for (size_t i = 0; i < m_Handlers.size(); ++i)
		{
			CKeyboardEvent event = ev;
			m_Handlers[i]->ProcessKeyboardEvent(event);

			if(event.IsSkipped())
				break;
		}
	}

	void Window::ProcessMouseEvent(CMouseEvent ev)
	{
		for (size_t i = 0; i < m_Handlers.size(); ++i)
		{
			CMouseEvent event = ev;
			m_Handlers[i]->ProcessMouseEvent(event);

			if(event.IsSkipped())
				break;
		}
	}

    /*!
     * @brief Binds the current render context.
     */
	void Window::BindContext()
	{
		SDL_GL_MakeCurrent(m_Window, m_Context);
	}
	
    /*!
     * @return Gets the current window size.
     */
    Vec2Di Window::GetWindowSize()
    {
        Vec2Di Ret;
        SDL_GetWindowSize(m_Window, &Ret.x, &Ret.y);
        
        return Ret;
    }

	/*!
	 * @brief Enables vsync. 
	 */
	void Window::EnableVSync(bool Enable)
	{
		if(Enable)
		{
			//Set V-Sync as adaptive vsync, otherwise as normal vsync
			if(SDL_GL_SetSwapInterval(-1) == -1)
				SDL_GL_SetSwapInterval(1);
		}
		else
			SDL_GL_SetSwapInterval(0);
	}
}  // namespace DivineAPI
