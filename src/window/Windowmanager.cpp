/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "Windowmanager.hpp"

namespace DivineAPI
{
	vector<IWindow*> g_Windows, g_DeleteList;

	/*!
	 * @brief Registers a new window.
	 */
	void RegisterWindow(IWindow *win)
	{
		bool Exists = false;

		//Check if the window already exists.
		for (size_t i = 0; i < g_Windows.size(); ++i)
		{
			if(g_Windows[i] == win)
			{
				Exists = true;
				break;
			}
		}

		if(!Exists)
			g_Windows.push_back(win);
	}

	vector<IWindow*> &GetDeleteList()
	{
		return g_DeleteList;
	}

	vector<IWindow*> &GetWindowList()
	{
		return g_Windows;
	}

	/*!
	 * @brief Unregister a window.
	 */
	void UnregisterWindow(IWindow *win, bool Delete)
	{
		for (size_t i = 0; i < g_Windows.size(); ++i)
		{
			if(g_Windows[i] == win)
			{
				if(Delete)
					g_DeleteList.push_back(g_Windows[i]);
				g_Windows.erase(g_Windows.begin() + i);

				break;
			}
		}
	}
}  // namespace DivineAPI
