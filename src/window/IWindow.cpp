/*
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <window/IWindow.hpp>
#include "Window.hpp"

namespace DivineAPI
{
    /*!
     * @brief Creates a window.
     * 
     * @param Title: Window title.
	 * @param Pos: Position of the window.
	 * @param Size: Size of the window.
     * @param Flags: The window flags. See also WindowFlags.
     * 
     * @throw std::runtime_error Throws if an error occures.
     */
    IWindow *IWindow::Create(std::string Title, Vec2Di Pos, Vec2Dui Size, IWindow::WindowFlags Flags)
    {
        return new Window(Title, Pos, Size, Flags);
    }
}