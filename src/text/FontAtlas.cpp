/*
 * zlib License
 *
 * (C) 2019 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#define STB_TRUETYPE_IMPLEMENTATION

#include <text/FontAtlas.hpp>
#include <string.h>
#include <window/IWindow.hpp>
#include <sstream>
#include <algorithm>

#include <iostream>

namespace DivineAPI
{
	/*!
	 * @brief Converts an utf8 string to an utf32 string.
	 */
	std::u32string utf8str_to_utf32str(const std::string &Text)
	{
		std::u32string ret;
		size_t Pos = 0;

		while(Pos < Text.size())
		{
			if((unsigned char)Text[Pos] <= (unsigned char)0x7F)
			{
				ret += (char32_t)Text[Pos];
				Pos++;
			}
			else if(((unsigned char)Text[Pos] & (unsigned char)0x80) == (unsigned char)0x80 && (Pos + 1) < Text.size())
			{
				unsigned char Byte0 = ((((unsigned char)Text[Pos] & 0x3) << 6) | ((unsigned char)Text[Pos + 1] & 0x3F));
				unsigned char Byte1 = (((unsigned char)Text[Pos] & 0x1C) >> 2);
				ret += ((Byte1 << 8) | Byte0);
				Pos += 2;
			}
			else if(((unsigned char)Text[Pos] & (unsigned char)0xE0) == (unsigned char)0xE0 && (Pos + 2) < Text.size())
			{
				unsigned char Byte0 = ((((unsigned char)Text[Pos + 1] & 0x3) << 6) | ((unsigned char)Text[Pos + 2] & 0x3F));
				unsigned char Byte1 = ((((unsigned char)Text[Pos] & 0xF) << 4) | (((unsigned char)Text[Pos + 1] & 0x3C) >> 2));
				ret += ((Byte1 << 8) | Byte0);
				Pos += 3;
			}
			else if(((unsigned char)Text[Pos] & (unsigned char)0xF0) == (unsigned char)0xF0 && (Pos + 3) < Text.size())
			{
				unsigned char Byte0 = ((((unsigned char)Text[Pos + 2] & 0x3) << 6) | ((unsigned char)Text[Pos + 3] & 0x3F));
				unsigned char Byte1 = ((((unsigned char)Text[Pos + 1] & 0xF) << 4) | (((unsigned char)Text[Pos + 2] & 0x3C) >> 2));
				unsigned char Byte2 = (((unsigned char)Text[Pos] & 0xF) << 2) | (((unsigned char)Text[Pos + 1] & 0x30) >> 4);
				ret += ((Byte2 << 16) | (Byte1 << 8) | Byte0);
				Pos += 4;
			}
			else
				break;
		}

		return ret;
	}

    /*!
     * @brief Loads a font from a file.
     * 
     * @param Path: Font to load.
     * @param FontSize: Size of the font in points.
     * 
     * @throw std::runtime_error Throws on error.
     */
    void CFontAtlas::LoadFont(const std::string &Path, int FontSize)
    {
        m_ConstMem = false;

        if(m_Data && !m_ConstMem)
        {
            delete[] m_Data;
            m_Data = nullptr;
        }

        IFile *File = IFile::Create(Path, IFile::Openmode::READ);

        size_t Size = File->Size();
        m_Data = new uint8_t[Size];

        File->Read((char*)m_Data, Size);

        delete File;
        m_FontSize = FontSize;

        InitFont();
    }

    /*!
     * @brief Loads a font from memory.
     * 
     * @param Data: Font data.
     * @param Size: Size of the data.
     * @param FontSize: Size of the font in points.
     * 
     * @throw std::runtime_error Throws on error.
     */
    void CFontAtlas::LoadFont(const uint8_t *Data, size_t Size, int FontSize)
    {
        m_ConstMem = true;

        if(m_Data && !m_ConstMem)
        {
            delete[] m_Data;
            m_Data = nullptr;
        }

        m_FontSize = FontSize;
        m_Data = const_cast<uint8_t*>(Data);

        InitFont();
    }

    std::vector<CFontAtlas::Glyph> CFontAtlas::RenderText(const std::string &Text)
    {
        std::vector<CFontAtlas::Glyph> Glyphs;
        std::u32string UTF32Text = utf8str_to_utf32str(Text);

        Fontresolution &Current = m_Resolutions[m_FontSize];
        
        for(size_t i = 0; i < UTF32Text.size(); i++)
        {
            if(Current.Glyphs.find(UTF32Text[i]) == Current.Glyphs.end())
                LoadGlyph(UTF32Text[i]);

            Glyphs.push_back(Current.Glyphs[UTF32Text[i]]);
        }
        
        return Glyphs;
    }

    void CFontAtlas::SetFontSize(int FontSize)
    {
        m_FontSize = FontSize;

        if(m_Resolutions.find(m_FontSize) == m_Resolutions.end())
            InitFont();
    }

    CFontAtlas::~CFontAtlas()
    {
        if(!m_ConstMem)
            delete[] m_Data;
    }

    std::string CFontAtlas::LoadInfo(int ID)
    {
        uint16_t Count = ttSHORT(m_Data + m_Info.name + 2);
        uint16_t StrOffset = ttSHORT(m_Data + m_Info.name + 4);

        for(uint16_t i = 0; i < Count; i++)
        {
            if(ttSHORT(m_Data + m_Info.name + 6 + (i * 12) + 6) == ID)
            {
                uint16_t Offset = ttSHORT(m_Data + m_Info.name + 6 + (i * 12) + 10);
                uint16_t Size = ttSHORT(m_Data + m_Info.name + 6 + (i * 12) + 8);
            
                std::string Ret;
                
                for(size_t j = 0; j < Size; j++)
                {
                    uint8_t Te = *(m_Data + m_Info.name + StrOffset + Offset + j);
                    Ret += (char)(*(m_Data + m_Info.name + StrOffset + Offset + j));
                }
    
                return Ret;
            }
        }

        return "";
    }

    float CFontAtlas::PointsToPixel(int PtSize)
    {
        return PtSize * 96.f / (72.f * m_Info.unitsPerEm);
    }

    void CFontAtlas::InitFont()
    {
        int Offset = stbtt_GetFontOffsetForIndex(m_Data, 0);
        if(Offset == -1)
            throw std::runtime_error("Invalid font format.");

        stbtt_InitFont(&m_Info, m_Data, Offset);
        CalculateHeightAndLinegap();  

        m_Copyright = LoadInfo(COPYRIGHT);
        m_Author = LoadInfo(AUTHOR);
        m_Fontname = LoadInfo(FONTFAMILIENAME);

        m_Resolutions[m_FontSize].Atlas = m_Rmgr->GetParent()->GetDriver()->CreateTexture();
        m_Resolutions[m_FontSize].Atlas->CreateEmptyTexture(Vec2Di(256, 256), CImage::Pixelformat::ALPHA_8);

        for(int i = 32; i < 127; i++)
        {
            LoadGlyph(i);
        }
    }

    void CFontAtlas::LoadGlyph(int Codepoint)
    {
        float Scale = PointsToPixel(m_FontSize);

        int asc;
        stbtt_GetFontVMetrics(&m_Info, &asc, 0, 0);
        int Baseline = (int)(asc * Scale);

        Fontresolution &Current = m_Resolutions[m_FontSize];

        Vec2Dui Size;
        uint8_t *Bmp = stbtt_GetCodepointBitmap(&m_Info, 0, Scale, Codepoint, (int*)&Size.x, (int*)&Size.y, 0, 0);
        Glyph glyph;
        glyph.Codepoint = Codepoint;
        glyph.Baseline = Baseline;

        stbtt_GetCodepointHMetrics(&m_Info, Codepoint, &glyph.Advance, &glyph.Bearing.x);
        stbtt_GetCodepointBox(&m_Info, Codepoint, 0, 0, 0, &glyph.Bearing.y);

        glyph.Advance = (int)(glyph.Advance * Scale);

        glyph.Bearing.y *= -1;
        glyph.Bearing.x = (int)(glyph.Bearing.x * Scale);
        glyph.Bearing.y = (int)(glyph.Bearing.y * Scale);

        if(Current.XPos + Size.x >= Current.Atlas->TextureSize().x)
        {
            Current.XPos = 0;
            Current.YPos += Current.RowH;
            Current.RowH = 0;
        }

        if(Current.YPos + Size.y >= Current.Atlas->TextureSize().y)
        {
            Vec2Di TexSize = Current.Atlas->TextureSize();
            Current.Atlas->ResizeTexture(TexSize * 2);
            Current.YPos = 0;
            Current.XPos = TexSize.x;
        }

        Current.RowH = std::max(Current.RowH, Size.y);

        CImage Img;

        glyph.Pos = Vec2Dui(Current.XPos, Current.YPos);
        glyph.Size = Size;

        if(Size != Vec2Dui())
        {
            Img.CreateEmptyImage(Size, CImage::Pixelformat::ALPHA_8);   
            
            /*for(size_t x = 0; x < Size.x; x++)
                for(size_t y = 0; y < Size.y; y++)
                    Img.SetPixel(Vec2Di(x, y), Color(255, 255, 255, Bmp[x + Size.x * y]));*/
            

            memcpy(Img.GetPixels(), Bmp, Size.x * Size.y);
            Current.Atlas->CopySubTexture(Img, glyph.Pos);
            Current.XPos += Size.x;
        }

        Current.Glyphs[Codepoint] = glyph;

        free(Bmp);
    }

    void CFontAtlas::CalculateHeightAndLinegap()
    {
        float Scale = PointsToPixel(m_FontSize);

        int asc, desc;

        stbtt_GetFontVMetrics(&m_Info, &asc, &desc, &m_Resolutions[m_FontSize].Linegap);

        m_Resolutions[m_FontSize].Height = (float)(asc - desc) * Scale;
        m_Resolutions[m_FontSize].Linegap = (float)m_Resolutions[m_FontSize].Linegap * Scale;
    }
} // DivineAPI
