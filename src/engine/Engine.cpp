/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <SDL2/SDL.h>

#include <engine/Engine.hpp>
#include "../window/Windowmanager.hpp"
#include <window/IWindow.hpp>
#include <utils/Log.hpp>
#include <input/InputManager.hpp>

#include <stdexcept>

#define DIVINE_VERSION "0.1.0-alpha"

namespace DivineAPI
{
	IInputHandler *Divine::m_Handler = nullptr;
	IGameLoop *Divine::m_Loop = nullptr;
	std::vector<CBaseTimer*> Divine::m_Timers;

	/*!
	 * @brief Initialize the engine.
	 * 
	 * @throw std::runtime_error Throws if the engine couldn't initialized.
	 */
	void Divine::Init()
	{
		if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
			throw std::runtime_error(SDL_GetError());
	}

	/*!
	 * @brief Handles the logic.
	 */
	void Divine::Run()
	{
		vector<IWindow*> WinList;
		unsigned int FPS = 0, Counter = 0;
		float Time = 0;

		do
		{
			float Begin = SDL_GetTicks();

			InputManager->ProcessMessages();
			UpdateTimers();

			WinList = GetWindowList();

			if (m_Loop && !WinList.empty())
				m_Loop->MainLoop(FPS);

			for (size_t i = 0; i < WinList.size(); ++i)
			{
				WinList[i]->Run();
			}

			Counter++;

			float dt = SDL_GetTicks() - Begin;
			Time += dt;
			if(Time / 1000.f >= 1.f)
			{
				FPS = Counter;
				Counter = 0;
				Time = 0;
			}
		} while (!WinList.empty());
	}

	/*!
	 * @brief Deinitialize the engine and cleanup.
	 */
	void Divine::Quit()
	{
		vector<IWindow*> WinList = GetWindowList();

		for (size_t i = 0; i < WinList.size(); ++i)
			delete WinList[i];

		LOG->Del();
		InputManager->Del();
		SDL_Quit();
	}

	/*!	
		http://semver.org/
	*/
	std::string Divine::GetVersion()
	{
		return DIVINE_VERSION;
	}

	void Divine::UpdateTimers()
	{
		for(size_t i = 0; i < m_Timers.size(); i++)
		{
			m_Timers[i]->Update();
		}
	}

	void Divine::RegisterInputHandler(IInputHandler *Handler)
	{
		m_Handler = Handler;
	}

	void Divine::RegisterGameLoop(IGameLoop *Loop)
	{
		m_Loop = Loop;
	}

	void Divine::RegisterTimer(CBaseTimer *tmr)
	{
		m_Timers.push_back(tmr);
	}

	void Divine::RemoveTimer(CBaseTimer *tmr)
	{
		for (size_t i = 0; i < m_Timers.size(); ++i)
		{
			if(m_Timers[i] == tmr)
			{
				m_Timers.erase(m_Timers.begin() + i);
				break;
			}
		}
	}
}  // namespace DivineAPI
