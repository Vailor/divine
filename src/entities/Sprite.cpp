/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <entities/Sprite.hpp>
#include <window/IWindow.hpp>
#include <stdlib.h>

namespace DivineAPI
{
	CSprite::CSprite(CSceneManager *smgr, CSceneNode *Parent, int ZOrder) : CRenderNode(smgr, Parent, ZOrder)
	{
		m_Texture = nullptr;
		m_IsAnimated = false;
	}

	/*!
	 * @brief Loads an non-animated sprite.
	 * 
	 * @param Path: Path to the sprite file.
	 * @throw std::runtime_error Throws if the sprite couldn't load.
	 * 
	 * @attention All image formats supported by the CImage class can be loaded.
	 */
	void CSprite::LoadSprite(std::string Path)
	{
		LoadSprite(CImage(Path));
	}

	/*!
	 * @brief Loads an animated sprite.
	 * 
	 * @param Path: Path to the sprite file.
	 * @param Tilesize: Size of each tile in the spritesheet.
	 * @param Pos: Animationposition to render.
	 * 
	 * @throw std::runtime_error Throws if the sprite couldn't load.
	 * 
	 * @attention All image formats supported by the CImage class can be loaded.
	 */
	void CSprite::LoadSprite(std::string Path, Vec2Di Tilesize, unsigned int Pos)
	{
		LoadSprite(CImage(Path), Tilesize, Pos);
	}

	/*!
	 * @brief Loads an non-animated sprite.
	 * 
	 * @param img: Loaded sprite file.
	 * @throw std::runtime_error Throws if the sprite couldn't load.
	 * 
	 * @attention All image formats supported by the CImage class can be loaded.
	 */
	void CSprite::LoadSprite(const CImage &img)
	{
		LoadSprite(img, Vec2Di(), 0);
	}

	/*!
	 * @brief Loads an animated sprite.
	 * 
	 * @param img: Loaded sprite file.
	 * @param Tilesize: Size of each tile in the spritesheet.
	 * @param Pos: Animationposition to render.
	 * 
	 * @throw std::runtime_error Throws if the sprite couldn't load.
	 * 
	 * @attention All image formats supported by the CImage class can be loaded.
	 */
	void CSprite::LoadSprite(const CImage &img, Vec2Di Tilesize, unsigned int Pos)
	{
		IWindow *win = GetSceneManager()->GetWindow();

		//Throws exception.
		m_Texture = win->GetResourceManager()->CreateTextureFromImage(img);

		win->BindContext();
		m_ImgSize = m_Texture->TextureSize();

		if(Tilesize.x != 0 && Tilesize.y != 0)
		{
			m_IsAnimated = true;
			SetSize(Tilesize);
		}
		else
		{
			SetSize(m_ImgSize);
			m_IsAnimated = false;
		}

		CreateSprite(Pos);
	}

	void CSprite::CreateSprite(unsigned int Pos)
	{
		std::vector<Vertex> Vertices;
		int Column = Pos % (int)(m_ImgSize.x / GetSize().x);
		int Row = Pos / (m_ImgSize.x / GetSize().x);

		Vertices.push_back({Vec3Df(0.f, 0.f, 0.f), Vec2Df((float)(Column * GetSize().x) / m_ImgSize.x, (float)(Row * GetSize().y) / m_ImgSize.y)});
		Vertices.push_back({Vec3Df(GetSize().x, 0.f, 0.f), Vec2Df((float)(Column * GetSize().x + GetSize().x) / m_ImgSize.x, (float)(Row * GetSize().y) / m_ImgSize.y)});
		Vertices.push_back({Vec3Df(0.f, GetSize().y, 0.f), Vec2Df((float)(Column * GetSize().x) / m_ImgSize.x, (float)(Row * GetSize().y + GetSize().y) / m_ImgSize.y)});
		Vertices.push_back({Vec3Df(GetSize().x, GetSize().y, 0.f), Vec2Df((float)(Column * GetSize().x + GetSize().x) / m_ImgSize.x, (float)(Row * GetSize().y + GetSize().y) / m_ImgSize.y)});


		std::vector<unsigned short> Indices;
		Indices.push_back(0);
		Indices.push_back(1);
		Indices.push_back(2);

		Indices.push_back(1);
		Indices.push_back(2);
		Indices.push_back(3);

		std::vector<int> IndicesPerObj;
		IndicesPerObj.push_back(6);

		m_Buffer->UploadVertices(Vertices, VERTEX_POS_UV);
		m_Buffer->UploadIndices(Indices, IndicesPerObj);	

		SetPosition(Vec2Df());
	}

	/*!
	 * @brief Sets the current animationposition.
	 * 
	 * @param Pos: Animationposition to render.
	 */
	void CSprite::SetAnimationPos(int Pos)
	{
		if(!m_IsAnimated)
			return;

		int Column = Pos % (int)(m_ImgSize.x / GetSize().x);
		int Row = Pos / (m_ImgSize.x / GetSize().x);

		std::vector<Vertex> Data;
		Data.push_back({Vec3Df(), Vec2Df((float)(Column * GetSize().x) / m_ImgSize.x, (float)(Row * GetSize().y) / m_ImgSize.y)});
		Data.push_back({Vec3Df(), Vec2Df((float)(Column * GetSize().x + GetSize().x) / m_ImgSize.x, (float)(Row * GetSize().y) / m_ImgSize.y)});
		Data.push_back({Vec3Df(), Vec2Df((float)(Column * GetSize().x) / m_ImgSize.x, (float)(Row * GetSize().y + GetSize().y) / m_ImgSize.y)});
		Data.push_back({Vec3Df(), Vec2Df((float)(Column * GetSize().x + GetSize().x) / m_ImgSize.x, (float)(Row * GetSize().y + GetSize().y) / m_ImgSize.y)});

		m_Buffer->UploadVertices(Data, VERTEX_UV);
	}

	void CSprite::Render()
	{
		if(m_Texture)
		{
			m_Texture->BindTexture();
			CRenderNode::Render();
			//m_Texture->UnbindTexture();
		}
	}

	void CSprite::Cleanup()
	{
		DeleteSprite();
		CRenderNode::Cleanup();
	}

	/*!
	 * @brief Clears the buffer and deletes the texture.
	 */
	void CSprite::DeleteSprite()
	{
		m_Buffer->ClearBuffer();
		if(m_Texture)
		{
			m_Texture->Ungrab();
			m_Texture = nullptr;
		}
	}
} /* namespace DivineAPI */
