/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <entities/Text.hpp>
#include <window/IWindow.hpp>
#include <algorithm>

namespace DivineAPI
{
	CText::CText(CSceneManager *smgr, CSceneNode *Parent, int ZOrder) : CRenderNode(smgr, Parent, ZOrder), m_FontSize(14) 
	{
		m_Atlas = smgr->GetWindow()->GetResourceManager()->GetDefaultFont();
		ApplyShader(IShaderManager::BUILTIN_TEXT_SHADER);
	}

	void CText::Render()
	{
		m_Atlas->SetFontSize(m_FontSize);
		ITexture *Tex = m_Atlas->GetTexture(); 

		if(Tex)
		{
			m_Shader->ApplyShader(m_ShaderID);
			m_Shader->SetUniform(UNIFORM_COLOR, m_TextColor);
			Tex->BindTexture();
			CRenderNode::Render();
			Tex->UnbindTexture();
		}
	}

	/*!
	 * @brief Sets the text for rendering.
	 * 
	 * @param CText: UTF-8 String.
	 */
	void CText::SetText(std::string Text)
	{
		m_Text = Text;

		m_Atlas->SetFontSize(m_FontSize);
		m_Buffer->ClearBuffer();
		m_Buffer->SetRenderMode(IObjectBuffer::RenderModes::TRIANGLES);

		std::vector<CFontAtlas::Glyph> TextGlyphs;
		TextGlyphs = m_Atlas->RenderText(Text);

		std::vector<Vertex> Vertices;
		std::vector<Vec2Df> TexCoords;
		std::vector<unsigned short> Indices;

		std::vector<int> IndicesPerObj;

		int advance = 0;
		int Count = 0;
		int Top = 0;
		Vec2Di Size;
		
		for(size_t i = 0; i < TextGlyphs.size(); i++)
		{
			CFontAtlas::Glyph Glyph = TextGlyphs[i];

			if(Glyph.Codepoint == '\t')
			{
				Glyph = m_Atlas->RenderText(" ").back();
				advance += Glyph.Advance * 4;
				continue;
			}
			else if(Glyph.Codepoint == '\n')
			{
				Size.x = std::max(Size.x, advance);
				advance = 0;
				Top += m_Atlas->GetHeigth() + m_Atlas->GetLinegap();

				Size.y += Top;

				continue;
			}
			else if(Glyph.Size == Vec2Dui())
			{
				advance += Glyph.Advance;
				continue;
			}
			
			Vec2Df Pos = Vec2Df((float)Glyph.Pos.x / m_Atlas->GetTexture()->TextureSize().x, (float)Glyph.Pos.y / m_Atlas->GetTexture()->TextureSize().y);
			Vec2Df Size = Vec2Df((float)Glyph.Size.x / m_Atlas->GetTexture()->TextureSize().x, (float)Glyph.Size.y / m_Atlas->GetTexture()->TextureSize().y);

			Vertices.push_back(Vertex{Vec3Df(advance + Glyph.Bearing.x, Top + Glyph.Baseline + Glyph.Bearing.y, 0.f), Vec2Df(Pos.x, Pos.y)});
			Vertices.push_back(Vertex{Vec3Df(advance + Glyph.Bearing.x + Glyph.Size.x, Top + Glyph.Baseline + Glyph.Bearing.y, 0.f), Vec2Df(Pos.x + Size.x, Pos.y)});
			Vertices.push_back(Vertex{Vec3Df(advance + Glyph.Bearing.x, Top + Glyph.Baseline + Glyph.Bearing.y + Glyph.Size.y, 0.f), Vec2Df(Pos.x, Pos.y + Size.y)});
			Vertices.push_back(Vertex{Vec3Df(advance + Glyph.Bearing.x + Glyph.Size.x, Top + Glyph.Baseline + Glyph.Bearing.y + Glyph.Size.y, 0.f), Vec2Df(Pos.x + Size.x, Pos.y + Size.y)});

			Indices.push_back(Count + 0);
			Indices.push_back(Count + 1);
			Indices.push_back(Count + 2);

			Indices.push_back(Count + 1);
			Indices.push_back(Count + 2);
			Indices.push_back(Count + 3);

			IndicesPerObj.push_back(6);

			advance += Glyph.Advance;
			Count += 4;
		}

		Size.x = std::max(Size.x, advance);
		Size.y += m_Atlas->GetHeigth();

		SetSize(Size);

		m_Buffer->UploadVertices(Vertices, VERTEX_POS_UV);
		m_Buffer->UploadIndices(Indices, IndicesPerObj);
	}
}
