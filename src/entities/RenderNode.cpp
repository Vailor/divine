/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <entities/RenderNode.hpp>
#include <image/Image.hpp>
#include <utils/Log.hpp>
#include <window/IWindow.hpp>
#include <scenemanager/SceneManager.hpp>

#include <iostream>

using namespace std;

namespace DivineAPI
{
	CRenderNode::CRenderNode(CSceneManager *smgr, CSceneNode *Parent, int ZOrder) : CSceneNode(smgr, Parent, ZOrder)
	{
		m_Shader = 0;

		m_Buffer = GetSceneManager()->GetWindow()->GetDriver()->CreateBuffer();

		IWindow *win = GetSceneManager()->GetWindow();
		m_Shader = win->GetShaderManager();

		m_ShaderID = IShaderManager::BUILTIN_SPRITE_SHADER;
		m_Shader->ApplyShader(m_ShaderID);
	}

	/*!
	 * @brief Applies the shader to be used for this entity.
	 * 
	 * @param Shader: The shader id to use.
	 * 
	 * @throw std::invalid_argument Throws if the shader id doesn't exists.
	 * @throw std::runtime_error Throws if the shader couldn't compile or link.
	 */
	void CRenderNode::Render()
	{
		Matrix Translation = Translate(Vec3Df(Pos));
		m_Shader->ApplyShader(m_ShaderID);

		m_Shader->SetUniform(UNIFORM_PROJECTION, GetWorldMatrix());
		m_Shader->SetUniform(UNIFORM_TRANSLATION, Translation);
		m_Shader->SetUniform(UNIFORM_ROTATION, m_Rotation);
		m_Shader->SetUniform(UNIFORM_SCALE, m_Scale);

		m_Buffer->Render();

		CSceneNode::Render();
	}

	void CRenderNode::ApplyShader(uint32_t Shader)
	{
		m_Shader->ApplyShader(Shader);
		m_ShaderID = Shader;
	}

	void CRenderNode::Cleanup()
	{
		if(m_Buffer)
		{
			m_Buffer->Release();
			m_Buffer = nullptr;
		}

		CSceneNode::Cleanup();
	}
}  // namespace DivineAPI
