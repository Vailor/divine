#include <input/InputManager.hpp>
#include <input/Event.hpp>
#include <input/KeyboardEvent.hpp>
#include <input/MouseEvent.hpp>
#include "../window/Windowmanager.hpp"
#include <SDL2/SDL_events.h>
#include <string.h>
#include <algorithm>
#include <stdexcept>

namespace DivineAPI
{
    CInputManager::CInputManager()
    {
		m_Handler = nullptr;
        m_Keymap = SDL_GetKeyboardState(nullptr);
        memset(m_ButtonMap, 0, sizeof(m_ButtonMap));
    }

    /*!
     * @brief Checks if the given key is pressed.
     * @return Returns true if the given key is pressed.
     */
    bool CInputManager::IsKeyPressed(int Key)
    {
        return static_cast<bool>(m_Keymap[SDL_GetScancodeFromKey(Key)]);
    }

    /*!
     * @brief Checks if the given button is pressed.
     * @return Returns true if the given button is pressed.
     */
    bool CInputManager::IsButtonPressed(int Button)
    {
        return static_cast<bool>(m_ButtonMap[Button - 1]);
    }

    /*!
     * @brief Registers a new input callback.
     * 
     * @attention The callback will be released by the input manager.
     */
    void CInputManager::RegisterInputHandler(IInputHandler *Handler)
    {
        if(m_Handler)
            delete m_Handler;

        m_Handler = Handler;
    }  

    CInputManager::~CInputManager()
    {
        if(m_Handler)
            delete m_Handler;
    }

    /*!
     * @brief Processes all messages.
     */
    void CInputManager::ProcessMessages()
    {
		SDL_Event ev;

		//SDL_StartTextInput();
		while (SDL_PollEvent(&ev))
		{
			switch (ev.type)
			{
				case SDL_WINDOWEVENT:
				{
					switch (ev.window.event)
					{
					case SDL_WINDOWEVENT_CLOSE:
					{
						vector<IWindow*> WinList = GetWindowList();

						for (size_t i = 0; i < WinList.size(); ++i)
						{
							if (WinList[i]->GetWindowID() == ev.window.windowID)
							{
								WinList[i]->Destroy();
								break;
							}
						}
					}break;

					case SDL_WINDOWEVENT_MAXIMIZED:
					case SDL_WINDOWEVENT_RESIZED:
					{
						vector<IWindow*> WinList = GetWindowList();

						for (size_t i = 0; i < WinList.size(); ++i)
						{
							if (WinList[i]->GetWindowID() == ev.window.windowID)
							{
								WinList[i]->ProcessResize(Vec2Dui(ev.window.data1, ev.window.data2));
								break;
							}
						}
					}
						break;
					}
				}break;

				case SDL_TEXTINPUT:
				case SDL_KEYDOWN:
				case SDL_KEYUP:
				{
					CKeyboardEvent event;
					event.SetType(ev.type);
					event.SetTimestamp(ev.key.timestamp);
					event.SetKeycode(ev.key.keysym.sym);
					event.SetScancode(ev.key.keysym.scancode);
					event.SetMod(ev.key.keysym.mod);
					event.SetState(ev.key.state);
					event.SetText(ev.text.text);

					if (m_Handler)
					{
						m_Handler->ProcessKeyboardEvent(event);
					}

					if(!event.IsSkipped())
					{
						vector<IWindow*> WinList = GetWindowList();

						for (size_t i = 0; i < WinList.size(); ++i)
						{
							if (WinList[i]->GetWindowID() == ev.key.windowID)
							{
								WinList[i]->ProcessKeyboardEvent(event);
								break;
							}
						}
					}
				}break;

				case SDL_MOUSEBUTTONDOWN:
				case SDL_MOUSEBUTTONUP:
				case SDL_MOUSEMOTION:
				case SDL_MOUSEWHEEL:
				{
					CMouseEvent event;
					event.SetType(ev.type);

					switch(ev.type)
					{
						case SDL_MOUSEBUTTONDOWN:
						case SDL_MOUSEBUTTONUP:
						{
							event.SetWhich(ev.button.which);
							event.SetButton(ev.button.button);
							event.SetButtonState(ev.button.state);
							event.SetClickCount(ev.button.clicks);
							event.SetMousePos(Vec2Di(ev.button.x, ev.button.y));
							event.SetTimestamp(ev.button.timestamp);

							m_ButtonMap[ev.button.button - 1] = ev.button.state;
							m_MousePos = Vec2Di(ev.button.x, ev.button.y);
						}break;

						case SDL_MOUSEMOTION:
						{
							event.SetWhich(ev.motion.which);
							event.SetMousePos(Vec2Di(ev.motion.x, ev.motion.y));
							event.SetTimestamp(ev.motion.timestamp);
							event.SetRelativePos(Vec2Di(ev.motion.xrel, ev.motion.yrel));
							event.SetButton(ev.motion.state);

							m_MousePos = Vec2Di(ev.motion.x, ev.motion.y);
						}break;

						case SDL_MOUSEWHEEL:
						{
							int x = ev.wheel.x, y = ev.wheel.y;

							if(ev.wheel.direction == SDL_MOUSEWHEEL_FLIPPED)
							{
								x *= -1;
								y *= -1;
							}

							event.SetWhich(ev.wheel.which);
							event.SetScrollValue(Vec2Di(x, y));
							event.SetTimestamp(ev.wheel.timestamp);
						}break;
					}

					if (m_Handler)
					{
						m_Handler->ProcessMouseEvent(event);
					}

					if(!event.IsSkipped())
					{
						vector<IWindow*> WinList = GetWindowList();

						for (size_t i = 0; i < WinList.size(); ++i)
						{
							Uint32 ID = WinList[i]->GetWindowID();
							if (ID == ev.motion.windowID || ID == ev.button.windowID || ID == ev.wheel.windowID)
							{
								WinList[i]->ProcessMouseEvent(event);
								break;
							}
						}
					}
				}break;
			}
		}
    }
}