/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include <utils/System.hpp>
#include <utils/Log.hpp>
#include <filesystem/IFile.hpp>
#include <SDL2/SDL.h>
#include <stdexcept>

#if defined(DIVINE_UNIX)
	#include <sys/statvfs.h>
	#include <string.h>
#endif

namespace DivineAPI
{
	/*!
	 * @return Gets informations about the free and total ram of the system.
	 * 
	 * @throw std::runtime_error Throws on error.
	 */
	System::Memory System::GetSystemMemoryInfo()
	{
		Memory Ret = {0};

#if defined(DIVINE_UNIX)
		IFile *meminfo = IFile::Create("/proc/meminfo", IFile::Openmode::READ);
		if(meminfo->IsOpen())
		{
			std::string Line;
			while(!meminfo->IsEOF())
			{
				Line = meminfo->ReadLn();

				if(Line.find("MemTotal") == 0)
				{
					size_t Pos = FindNumeric(Line);
					Ret.TotalMemory = std::stol(Line.substr(Pos, Line.find(" ", Pos) - Pos)) * 1024;
				}
				else if(Line.find("MemAvailable") == 0)
				{
					size_t Pos = FindNumeric(Line);
					Ret.FreeMemory = std::stol(Line.substr(Pos, Line.find(" ", Pos) - Pos)) * 1024;
				}
			}

			delete meminfo;
		}
		else
		{
			delete meminfo;
			throw std::runtime_error("Couldn't open /proc/meminfo");
		}
#elif defined(DIVINE_WINDOWS)
		MEMORYSTATUSEX mem;
		mem.dwLength = sizeof(MEMORYSTATUSEX);

		if(!GlobalMemoryStatusEx(&mem))
			throw std::runtime_error("Couldn't determine memory informations.");

		Ret.TotalMemory = mem.ullTotalPhys;
		Ret.FreeMemory = mem.ullAvailPhys;
#endif
		return Ret;
	}

	/*!
	 * @brief Gets informations about all mounted disks.
	 * 
     * @return Returns a list of all mounted disks.
	 * 
	 * @throw std::runtime_error Throws on error.
	 */
	std::vector<System::DiskInfo> System::GetDisksInfos()
	{
		std::vector<System::DiskInfo> Infos;
#if defined(DIVINE_UNIX)
		IFile *mounts = IFile::Create("/proc/mounts", IFile::Openmode::READ);
		if(mounts->IsOpen())
		{
			std::string Line;
			while(!mounts->IsEOF())
			{
				Line = mounts->ReadLn();

				size_t Pos = Line.find(" ");
				std::string Device = Line.substr(0, Pos);

				if(Device.find("/dev") != std::string::npos /*&& Device.find("loop") == std::string::npos*/)
				{
					size_t PathPos = Line.find(" ", Pos + 1);
					System::DiskInfo Info;

					Info.Path = Line.substr(Pos + 1, PathPos - Pos - 1);
					struct statvfs VFSInfo;
					if(::statvfs(Info.Path.c_str(), &VFSInfo) == 0)
					{
						Info.TotalSpace = VFSInfo.f_blocks * VFSInfo.f_frsize;
						Info.FreeSpace = VFSInfo.f_bfree * VFSInfo.f_frsize;
						Info.Available = VFSInfo.f_bavail * VFSInfo.f_frsize;

						Infos.push_back(Info);
					}
					else
					{
						//delete mounts;
						LOGE("An error occured by getting disk informations about: " + Info.Path + " ERRNO: " + strerror(errno));
					}
				}
			}

			delete mounts;
		}
		else
		{
			delete mounts;
			throw std::runtime_error("Couldn't open /proc/mounts");
		}
#elif defined(DIVINE_WINDOWS)
		DWORD Drives = GetLogicalDrives();
		for(int i = 0; i < sizeof(DWORD); i++)
		{
			if(Drives & (1 << i))
			{
				System::DiskInfo Info;
				Info.Path = std::string(1, (char)(65 + i)) + ":";

				ULARGE_INTEGER Available, TotalSpace, FreeSpace;

				if(GetDiskFreeSpaceExA(Info.Path.c_str(), &Available, &TotalSpace, &FreeSpace))
				{
					Info.Available = Available.QuadPart;
					Info.TotalSpace = TotalSpace.QuadPart;
					Info.FreeSpace = FreeSpace.QuadPart;

					Infos.push_back(Info);
				}
				else
					LOGE("An error occured by getting disk informations about: " + Info.Path);
			}
		}
#endif

		return Infos;
	}

	/*!
	 * @return Gets a list of all monitors which are connected to this system.
	 * 
	 * @throw std::runtime_error Throws on error.
	 */
	std::vector<System::MonitorInfo> System::GetMonitors()
	{
		std::vector<System::MonitorInfo> Ret;

		int DisplayNum = SDL_GetNumVideoDisplays();
		if(DisplayNum >= 1)
		{
			for (int i = 0; i < DisplayNum; ++i)
			{
				MonitorInfo Monitor = {};
				Monitor.Index = i;
				Monitor.Name = SDL_GetDisplayName(i);

				SDL_DisplayMode Mode;
				if(SDL_GetDisplayMode(i, 0, &Mode) != 0)
					throw std::runtime_error(std::string(SDL_GetError()));

				Monitor.Resolution.x = Mode.w;
				Monitor.Resolution.y = Mode.h;
				Monitor.RefreshRate = Mode.refresh_rate;
				Monitor.Depth = SDL_BITSPERPIXEL(Mode.format);


				if(SDL_GetDisplayDPI(i, nullptr, &Monitor.DPI.x, &Monitor.DPI.y) < 0)
					Monitor.DPI = Vec2Df(96, 96);

				Ret.push_back(Monitor);
			}
		}
		else
			throw std::runtime_error("Couldn't get display informations. SDL: " + std::string(SDL_GetError()));

		return Ret;
	}

#ifdef DIVINE_UNIX
	size_t System::FindNumeric(std::string Str)
	{
		size_t Pos = -1;

		for(size_t i = 0; i < Str.length(); i++)
		{
			if(isdigit(Str[i]))
			{
				Pos = i;
				break;
			}
		}

		return Pos;
	}
#endif
}  // namespace DivineAPI


