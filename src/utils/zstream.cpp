#include <utils/zstream.hpp>
#include <string.h>
#include <algorithm>
#include "zlib-1.2.11/zlib.h"

namespace DivineAPI
{
    ZStream_Base::ZStream_Base() :  
        m_DictionaryPos(0),
        m_BufferSize(32768UL)
    {
        m_Dictionary.reserve(32768UL);
    }

    ZStream_Base::ZStream_Base(size_t BufferSize) : 
        m_DictionaryPos(0),
        m_BufferSize(BufferSize)
    {
        m_Dictionary.reserve(BufferSize);
    }

    void ZStream_Base::Flush()
    {
        m_DictionaryPos = 0;

        if(!m_OutData.empty())
            m_OutData.clear();
    }

    //--------------------------------------------------------------------
    //                            Deflate
    //--------------------------------------------------------------------

    ZStream_Base &ZCompressionStream::operator<<(const std::string &Data)
    {
        Write(reinterpret_cast<const uint8_t*>(Data.data()), Data.size());
        return *this;
    }

    ZStream_Base &ZCompressionStream::operator<<(std::istream &Data)
    {
        size_t Size = 0;
        size_t CurPos = Data.tellg();

        Data.seekg(0, Data.end);
        Size = Data.tellg();
        Data.seekg(CurPos, Data.beg);

        std::vector<uint8_t> InData;
        InData.reserve(m_BufferSize);

        size_t Datasize = Size;
        while(Datasize != 0)
        {
            Data.read(reinterpret_cast<char*>(InData.data()), m_BufferSize);
            Datasize -= Data.gcount();

            Compress(InData);
        }

        return *this;
    }

    ZStream_Base &ZCompressionStream::operator<<(IFile *Data)
    {
        std::vector<uint8_t> InData;
        InData.reserve(m_BufferSize);

        size_t Datasize = Data->Size();
        while(Datasize != 0)
        {
            Datasize -= Data->Read(reinterpret_cast<char*>(InData.data()), m_BufferSize);
            Compress(InData);
        }

        return *this;
    }

    std::string &ZCompressionStream::operator>>(std::string &Data)
    {
        Data.insert(Data.end(), m_OutData.begin(), m_OutData.end());
        return Data;
    }

    std::ostream &ZCompressionStream::operator>>(std::ostream &Data)
    {
        Data.write(reinterpret_cast<char*>(m_OutData.data()), m_OutData.size());
        return Data;
    }

    IFile *ZCompressionStream::operator>>(IFile *Data)
    {
        Data->Write(reinterpret_cast<char*>(m_OutData.data()), m_OutData.size());
        return Data;
    }

    /*!
     * @brief Writes data to the stream.
     * 
     * @param Data: Data to write.
     * @param Size: Datasize
     */
    void ZCompressionStream::Write(const uint8_t *Data, size_t Size)
    {
        std::vector<uint8_t> InData;
        InData.reserve(Size);
        std::copy(Data, Data + Size, InData.begin());

        Compress(InData);
    }

    /*!
     * @brief Reads data from the stream.
     * 
     * @param Buf: Buffer to store the data.
     * @param Size: Buffersize.
     */
    void ZCompressionStream::Read(uint8_t *Buf, size_t Size)
    {
        size_t CopySize = Size;

        if(CopySize > m_OutData.size())
            CopySize = m_OutData.size();

        memcpy(Buf, m_OutData.data(), CopySize);
    }

    void ZCompressionStream::Compress(std::vector<uint8_t> &Data)
    {
        z_stream Stream;
        int Flush = 0, Ret = 0;
        size_t DataPos = 0;

        Stream.zalloc = Z_NULL;
        Stream.zfree = Z_NULL;
        Stream.opaque = Z_NULL;

        if(deflateInit(&Stream, m_Level) != Z_OK)
            throw std::runtime_error("Failed to init deflate stream");

        deflateSetDictionary(&Stream, m_Dictionary.data(), m_DictionaryPos);

        size_t TotalSize = Data.size();
        uint8_t *OutBuf = new uint8_t[m_BufferSize];

        do
        {
            if(Data.size() > m_BufferSize)
                Stream.avail_in = m_BufferSize;
            else
                Stream.avail_in = Data.size();

            Stream.next_in = Data.data() + DataPos;

            Flush = (TotalSize == 0) ? Z_FINISH : Z_NO_FLUSH;

            TotalSize -= Stream.avail_in;
            DataPos += Stream.avail_in;

            do
            {
                Stream.avail_out = m_BufferSize;
                Stream.next_out = OutBuf;

                if((Ret = deflate(&Stream, Flush)) != Z_STREAM_ERROR)
                    m_OutData.insert(m_OutData.end(), OutBuf, OutBuf + (m_BufferSize - Stream.avail_out));
            }while(Stream.avail_out == 0);
        }while(Flush != Z_FINISH);

        uInt DictSize = m_Dictionary.size();
        m_DictionaryPos = deflateGetDictionary(&Stream, m_Dictionary.data(), &DictSize);

        deflateEnd(&Stream);
        delete[] OutBuf;

        if(Ret != Z_STREAM_END)
            throw std::runtime_error("Stream couldn't finished");
    }

    //--------------------------------------------------------------------
    //                            Inflate
    //--------------------------------------------------------------------

    ZStream_Base &ZDecompressionStream::operator<<(const std::string &Data)
    {
        Write(reinterpret_cast<const uint8_t*>(Data.data()), Data.size());
        return *this;
    }

    ZStream_Base &ZDecompressionStream::operator<<(std::istream &Data)
    {
        size_t Size = 0;
        size_t CurPos = Data.tellg();

        Data.seekg(0, Data.end);
        Size = Data.tellg();
        Data.seekg(CurPos, Data.beg);

        std::vector<uint8_t> InData;
        InData.reserve(m_BufferSize);

        size_t Datasize = Size;
        while(Datasize != 0)
        {
            Data.read(reinterpret_cast<char*>(InData.data()), m_BufferSize);
            Datasize -= Data.gcount();

            Decompress(InData);
        }

        return *this;
    }

    ZStream_Base &ZDecompressionStream::operator<<(IFile *Data)
    {
        std::vector<uint8_t> InData;
        InData.reserve(m_BufferSize);

        size_t Datasize = Data->Size();
        while(Datasize != 0)
        {
            Datasize -= Data->Read(reinterpret_cast<char*>(InData.data()), m_BufferSize);
            Decompress(InData);
        }

        return *this;
    }

    std::string &ZDecompressionStream::operator>>(std::string &Data)
    {
        Data.insert(Data.end(), m_OutData.begin(), m_OutData.end());
        return Data;
    }

    std::ostream &ZDecompressionStream::operator>>(std::ostream &Data)
    {
        Data.write(reinterpret_cast<char*>(m_OutData.data()), m_OutData.size());
        return Data;
    }

    IFile *ZDecompressionStream::operator>>(IFile *Data)
    {
        Data->Write(reinterpret_cast<char*>(m_OutData.data()), m_OutData.size());
        return Data;
    }

    /*!
     * @brief Writes data to the stream.
     * 
     * @param Data: Data to write.
     * @param Size: Datasize
     */
    void ZDecompressionStream::Write(const uint8_t *Data, size_t Size)
    {
        std::vector<uint8_t> InData;
        InData.reserve(Size);
        std::copy(Data, Data + Size, InData.begin());

        Decompress(InData);
    }

    /*!
     * @brief Reads data from the stream.
     * 
     * @param Buf: Buffer to store the data.
     * @param Size: Buffersize.
     */
    void ZDecompressionStream::Read(uint8_t *Buf, size_t Size)
    {
        size_t CopySize = Size;

        if(CopySize > m_OutData.size())
            CopySize = m_OutData.size();

        memcpy(Buf, m_OutData.data(), CopySize);
    }

    void ZDecompressionStream::Decompress(std::vector<uint8_t> &Data)
    {
        z_stream Stream;
        int Ret = 0;
        size_t DataPos = 0;

        Stream.zalloc = Z_NULL;
        Stream.zfree = Z_NULL;
        Stream.opaque = Z_NULL;

        if(inflateInit(&Stream) != Z_OK)
            throw std::runtime_error("Failed to init inflate stream");

        inflateSetDictionary(&Stream, m_Dictionary.data(), m_DictionaryPos);

        size_t TotalSize = Data.size();
        uint8_t *OutBuf = new uint8_t[m_BufferSize];

        do
        {
            if(Data.size() > m_BufferSize)
                Stream.avail_in = m_BufferSize;
            else
                Stream.avail_in = Data.size();

            Stream.next_in = Data.data() + DataPos;

            TotalSize -= Stream.avail_in;
            DataPos += Stream.avail_in;

            do
            {
                Stream.avail_out = m_BufferSize;
                Stream.next_out = OutBuf;

                if((Ret = inflate(&Stream, Z_NO_FLUSH)) != Z_STREAM_ERROR)
                    m_OutData.insert(m_OutData.end(), OutBuf, OutBuf + (m_BufferSize - Stream.avail_out));
            }while(Stream.avail_out == 0);
        }while(TotalSize > 0);

        uInt DictSize = m_Dictionary.size();
        m_DictionaryPos = inflateGetDictionary(&Stream, m_Dictionary.data(), &DictSize);

        inflateEnd(&Stream);
        delete[] OutBuf;

        if(Ret != Z_STREAM_END)
            throw std::runtime_error("Stream couldn't finished");
    }
}