/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef RESOURCES_H_
#define RESOURCES_H_

#include "OpenSans.h"
#include <platform.h>

namespace DivineAPI
{
#ifndef DIVINE_ANDROID
	//Data from file sprite.fs
	const char spritefs[] = {
			"#version 330\n"
			"\n"
			"in vec2 UV;\n"
			"out vec4 Final;\n"
			"\n"
			"uniform sampler2D Texture;\n"
			"\n"
			"void main()\n"
			"{\n"
			"	Final = texture2D(Texture, UV);\n"
			"}\n"
	};

	//Data from file sprite.vs
	const char spritevs[] = {
			"#version 330\n"
			"\n"
			"out vec2 UV;\n"
			"\n"
			"uniform mat4 Projection;\n"
			"uniform mat4 Translation;\n"
			"uniform mat4 Scale;\n"
			"uniform mat4 Rotation;\n"
			"\n"
			"in vec3 Coord;\n"
			"in vec2 UVCoord;\n"
			"\n"
			"void main()\n"
			"{\n"
			"	gl_Position = Projection * Translation * Rotation * Scale * vec4(Coord, 1.0);\n"
			"	UV = UVCoord;\n"
			"}\n"
	};

	//Data from file text.fs
	const char textfs[] = {
			"#version 330\n"
			"\n"
			"in vec2 UV;\n"
			"in vec4 Color;\n"
			"\n"
			"out vec4 Final;\n"
			"\n"
			"uniform sampler2D Texture;\n"
			"\n"
			"void main()\n"
			"{\n"
			"	Final = vec4(Color.r, Color.g, Color.b, Color.a * texture2D(Texture, UV).r);\n"
			"}\n"
	};

	//Data from file text.vs
	const char textvs[] = {
			"#version 330\n"
			"\n"
			"out vec2 UV;\n"
			"out vec4 Color;\n"
			"\n"
			"uniform mat4 Projection;\n"
			"uniform mat4 Translation;\n"
			"uniform mat4 Scale;\n"
			"uniform mat4 Rotation;\n"
			"\n"
			"uniform vec4 TextColor;\n"
			"\n"
			"in vec3 Coord;\n"
			"in vec2 UVCoord;\n"
			"\n"
			"void main()\n"
			"{\n"
			"	gl_Position = Projection * Translation * Rotation * Scale * vec4(Coord, 1.0);\n"
			"	UV = UVCoord;\n"
			"	Color = TextColor;\n"
			"}\n"
	};
#else
	//Data from file sprite.vs
	const char spritevs[] = {
			"varying vec2 UV;\n"
			"\n"
			"uniform mat4 Projection;\n"
			"uniform mat4 Translation;\n"
			"uniform mat4 Scale;\n"
			"uniform mat4 Rotation;\n"
			"\n"
			"attribute vec3 Coord;\n"
			"attribute vec2 UVCoord;\n"
			"\n"
			"void main()\n"
			"{\n"
			"	gl_Position = Projection * Translation * Rotation * Scale * vec4(Coord, 1.0);\n"
			"	UV = UVCoord;\n"
			"}\n"
	};

	//Data from file sprite.fs
	const char spritefs[] = {
			"precision mediump float;"
			"\n"
			"varying vec2 UV;\n"
			"\n"
			"uniform sampler2D Texture;\n"
			"\n"
			"void main()\n"
			"{\n"
			"	gl_FragColor = texture2D(Texture, UV);\n"
			"}\n"
	};

	//Data from file text.vs
	const char textvs[] = {
			"varying vec2 UV;\n"
			"varying vec4 Color;\n"
			"\n"
			"uniform mat4 Projection;\n"
			"uniform mat4 Translation;\n"
			"uniform mat4 Scale;\n"
			"uniform mat4 Rotation;\n"
			"\n"
			"uniform vec4 TextColor;\n"
			"\n"
			"attribute vec3 Coord;\n"
			"attribute vec2 UVCoord;\n"
			"\n"
			"void main()\n"
			"{\n"
			"	gl_Position = Projection * Translation * Rotation * Scale * vec4(Coord, 1.0);\n"
			"	UV = UVCoord;\n"
			"	Color = TextColor;\n"
			"}\n"
	};

	//Data from file text.fs
	const char textfs[] = {
			"precision mediump float;"
			"\n"
			"varying vec2 UV;\n"
			"varying vec4 Color;\n"
			"\n"
			"uniform sampler2D Texture;\n"
			"\n"
			"void main()\n"
			"{\n"
			"	gl_FragColor = vec4(Color.r, Color.g, Color.b, Color.a * texture2D(Texture, UV).a);\n"
			"}\n"
	};
#endif
}  // namespace DivineAPI

#endif /* RESOURCES_H_ */
