/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef RESOURCEMANAGER_HPP_
#define RESOURCEMANAGER_HPP_

#include <map>
#include <string>
#include <resource/IResourcemanager.hpp>
#include <window/IWindow.hpp>
#include <driver/ITexture.hpp>
#include <text/FontAtlas.hpp>
#include <image/Image.hpp>
#include <math/Vector2D.hpp>
#include <stdint.h>
#include "OpenSans.h"

namespace DivineAPI
{
	/*!
	 * @brief Loads and manages some ressources.
	 */
	class CResourcemanager : public IResourcemanager
	{
		public:
			explicit CResourcemanager(IWindow *win)
			{
				m_Window = win;

				LoadMemoryFont(OpenSansRegular, sizeof(OpenSansRegular));
				m_DefaultFontName = m_Resources.begin()->first;
			}

			/*!
			 * @brief Loads and creates a texture.
			 * 
			 * @param Path: Path to the texture to load.
			 * 
			 * @return Returns a texture.
			 * 
			 * @throw std::runtime_error Throws on error.
			 */
			ITexture *LoadTexture(const std::string &Path);

			/*!
			 * @brief Creates a texture from an CImage.
			 * 
			 * @param img: The image to use.
			 * 
			 * @return Returns a texture.
			 * 
			 * @throw std::runtime_error Throws on error.
			 */
			ITexture *CreateTextureFromImage(const CImage &img);

			/*!
			 * @brief Loads a font from file.
			 * 
			 * @param Path: Path to the font file.
			 * 
			 * @return Returns the loaded font.
			 * 
			 * @throw std::runtime_error Throws on error.
			 */
			CFontAtlas *LoadFont(const std::string &Path);

			/*!
			 * @brief Loads a font from memory.
			 * 
			 * @param Data: Data of the font.
			 * @param Size: Size of the data.
			 * 
			 * @return Returns the loaded font.
			 * 
			 * @throw std::runtime_error Throws on error.
			 */
			CFontAtlas *LoadMemoryFont(const unsigned char *Data, size_t Size);

			/*!
			 * @brief Gets the default font.
			 */
			CFontAtlas *GetDefaultFont()
			{
				return dynamic_cast<CFontAtlas*>(m_Resources[m_DefaultFontName]);
			}

			/*!
			 * @brief Deletes a resource.
			 */
			void DeleteResource(CBaseResource *Res);

			IWindow *GetParent()
			{
				return m_Window;
			}

			virtual ~CResourcemanager();
		private:
			/*! Saves information of a loaded texture. */
			struct TextureResource
			{
				ITexture *Texture;
				unsigned int RefCount; //!< The reference counter to identify whether the texture should be released.
			};

			IWindow *m_Window; //!< Reference to the parent window.

			std::string m_DefaultFontName;
			std::map<std::string, CBaseResource*> m_Resources;
	};

}  // namespace DevineAPI

#endif /* RESOURCEMANAGER_HPP_ */
