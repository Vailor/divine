/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#include "Resourcemanager.hpp"
#include <window/IWindow.hpp>
#include <time.h>
#include <utils/Log.hpp>

namespace DivineAPI 
{
	/*!
	 * @brief Loads and creates a texture.
	 * 
	 * @param Path: Path to the texture to load.
	 * 
	 * @return Returns a texture.
	 * 
	 * @throw std::runtime_error Throws on error.
	 */
	ITexture *CResourcemanager::LoadTexture(const std::string &Path)
	{
		return CreateTextureFromImage(Path);
	}

	/*!
	 * @brief Creates a texture from an CImage.
	 * 
	 * @param img: The image to use.
	 * 
	 * @return Returns a texture.
	 * 
	 * @throw std::runtime_error Throws on error.
	 */
	ITexture *CResourcemanager::CreateTextureFromImage(const CImage &img)
	{
		if(img.GetPixels() == nullptr)
			throw std::runtime_error(std::string(__FUNCTION__) + ": Texture can't created from empty image.");

		m_Window->BindContext();

		std::string Path = img.GetPath();

		if(!Path.empty())
		{
			std::map<std::string, CBaseResource*>::iterator IT = m_Resources.find(Path);

			if(IT != m_Resources.end())
			{
				ITexture *Ret = dynamic_cast<ITexture*>(IT->second);
				if(Ret)
				{
					Ret->Grab();
					return Ret;
				}
			}
		}

		ITexture *Ret = m_Window->GetDriver()->CreateTexture(this, img);	
		Ret->Grab();	

		if(Path.empty())
			Path = std::to_string((size_t)Ret);

		Ret->SetKey(Path);
		m_Resources[Path] = Ret;

		return Ret;
	}

	/*!
	 * @brief Loads a font from file.
	 * 
	 * @param Path: Path to the font file.
	 * 
	 * @return Returns the loaded font.
	 * 
	 * @throw std::runtime_error Throws on error.
	 */
	CFontAtlas *CResourcemanager::LoadFont(const std::string &Path)
	{
		CFontAtlas *font = new CFontAtlas(this);
		try
		{
			font->LoadFont(Path);
		}
		catch(const std::runtime_error &e)
		{
			delete font;
			throw;
		}

		std::map<std::string, CBaseResource*>::iterator IT = m_Resources.find(font->GetFontname());
		if(IT != m_Resources.end())
		{
			delete font;
			font = dynamic_cast<CFontAtlas*>(IT->second);
		}
		else
		{
			font->SetKey(font->GetFontname());
			m_Resources[font->GetFontname()] = font;
		}

		return font;
	}

	/*!
	 * @brief Loads a font from memory.
	 * 
	 * @param Data: Data of the font.
	 * @param Size: Size of the data.
	 * 
	 * @return Returns the loaded font.
	 * 
	 * @throw std::runtime_error Throws on error.
	 */
	CFontAtlas *CResourcemanager::LoadMemoryFont(const unsigned char *Data, size_t Size)
	{
		CFontAtlas *font = new CFontAtlas(this);
		
		try
		{
			font->LoadFont(Data, Size);
		}
		catch(const std::runtime_error& e)
		{
			delete font;
			throw;
		}

		std::map<std::string, CBaseResource*>::iterator IT = m_Resources.find(font->GetFontname());
		if(IT != m_Resources.end())
		{
			delete font;
			font = dynamic_cast<CFontAtlas*>(IT->second);
		}
		else
		{
			font->SetKey(font->GetFontname());
			m_Resources[font->GetFontname()] = font;
		}

		return font;
	}

	/*!
	 * @brief Deletes a resource.
	 */
	void CResourcemanager::DeleteResource(CBaseResource *Res)
	{
		m_Resources.erase(Res->GetKey());
		Res->Free();
	}

	CResourcemanager::~CResourcemanager()
	{
		m_Window->BindContext();

		std::map<std::string, CBaseResource*>::iterator IT = m_Resources.begin();
		while(IT != m_Resources.end())
		{
			IT->second->Free();
			IT = m_Resources.erase(IT);
		}
	}
}  // namespace DivineAPI


