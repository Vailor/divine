/***************************************************************
 * Name:      	XMLElement.cpp
 * Purpose:   	
 * Author:    	Christian Tost
 * Created:   	11.11.2017
 * Modified by:
 * Copyright: 	(C) Christian Tost
 * License: 	
 **************************************************************/

#include <xml/XMLElement.hpp>

namespace DivineXML
{
	XMLElement::XMLElement(XMLElement *Root) : XMLElement()
	{
		Root->AddChild(this);
	}

	/*!
	 * @return Gets the first child.
	 */
	XMLElement *XMLElement::GetFirstElement()
	{
		m_ElementsPos = 0;
		return GetNextElement();
	}

	/*!
	 * @return Gets the next child. Before must @see GetFirstElement be called.
	 */
	XMLElement *XMLElement::GetNextElement()
	{
		if(m_Elements.empty() || m_ElementsPos == m_Elements.size() || m_ElementsPos == -1)
			return nullptr;

		return m_Elements[m_ElementsPos++];
	}

	/*!
	 * @return Gets the first attribute of this tag.
	 */
	XMLAttribute *XMLElement::GetFirstAttr()
	{
		m_AttrsPos = 0;
		return GetNextAttr();
	}

	/*!
	 * @return Gets the next attribute of this tag. Before must @see GetFirstAttr be called.
	 */
	XMLAttribute *XMLElement::GetNextAttr()
	{
		if(m_Attrs.empty() || m_AttrsPos == m_Attrs.size() || m_AttrsPos == -1)
			return nullptr;

		return m_Attrs[m_AttrsPos++];
	}

	/*!
	 * @return Searchs an attribute by its name and return the attribute, if it was found. otherwise null.
	 */
	XMLAttribute *XMLElement::GetAttrByName(std::string Name)
	{
		XMLAttribute *Attr = nullptr;

		for (size_t i = 0; i < m_Attrs.size(); ++i)
		{
			if(m_Attrs[i]->GetName() == Name)
			{
				Attr = m_Attrs[i];
				break;
			}
		}

		return Attr;
	}

	/*!
	 * @return Gets a element by its name. Returns null if the child wasn't found.
	 */
	XMLElement *XMLElement::GetElementByName(std::string Name)
	{
		XMLElement *Element = nullptr;

		for (size_t i = 0; i < m_Elements.size(); ++i)
		{
			if(m_Elements[i]->GetTagName() == Name)
			{
				Element = m_Elements[i];
				break;
			}
		}

		return Element;
	}

	/*!
	 * @brief Adds a new attribute to this element.
	 * @param Name: The name of the attribute.
	 * @param: Value: The value of the attribute.
	 */
	void XMLElement::AddAttribute(std::string Name, std::string Value)
	{
		XMLAttribute *Attr = new XMLAttribute();
		Attr->SetName(Name);
		Attr->SetValue(Value);

		m_Attrs.push_back(Attr);
	}

	/*!
	 * @brief Adds a new attribute to this tag.
	 * @param Attribute: Attribute to add.
	 */
	void XMLElement::AddAttribute(XMLAttribute *Attribute)
	{
		m_Attrs.push_back(Attribute);
	}

	/*!
	 * @brief Adds a new child to this element.
	 */
	void XMLElement::AddChild(XMLElement *Child)
	{
		Child->m_Root = this;
		m_Elements.push_back(Child);
	}

	/*!
	 * @brief Removes and frees the given child, if the child exists.
	 * @return Returns 0 on success otherwise -1.
	 */
	int XMLElement::RemoveChild(XMLElement *Child)
	{
		for (size_t i = 0; i < m_Elements.size(); ++i)
		{
			if(m_Elements[i] == Child)
			{
				Child->m_Root = nullptr;
				return 0;
			}
		}

		return -1;
	}

	/*!
	 * @brief Removes and frees a child by its name, if the child exists.
	 * @return Returns 0 on success otherwise -1.
	 */
	int XMLElement::RemoveChild(std::string Name)
	{
		for (size_t i = 0; i < m_Elements.size(); ++i)
		{
			if(m_Elements[i]->GetTagName() == Name)
			{
				m_Elements[i]->m_Root = nullptr;
				m_Elements.erase(m_Elements.begin() + i);
				return 0;
			}
		}

		return -1;
	}

	void XMLElement::ClearElements()
	{
		if(!m_Elements.empty())
		{
			for (size_t i = 0; i < m_Elements.size(); i++)
			{
				delete m_Elements[i];
			}

			m_Elements.clear();
		}

		if(!m_Attrs.empty())
		{
			for (size_t i = 0; i < m_Attrs.size(); i++)
			{
				delete m_Attrs[i];
			}

			m_Attrs.clear();
		}
	}

	XMLElement::~XMLElement()
	{
		ClearElements();
	}
}
