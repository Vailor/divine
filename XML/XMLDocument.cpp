/***************************************************************
 * Name:      	XMLDocument.cpp
 * Purpose:   	
 * Author:    	Christian Tost
 * Created:   	11.11.2017
 * Modified by:
 * Copyright: 	(C) Christian Tost
 * License: 	
 **************************************************************/

#include <xml/XMLDocument.hpp>
#include <filesystem/File.hpp>
#include <sstream>
#include <iostream>

namespace DivineXML
{
	enum class PROLOG
	{
		VERSION = 0,
		ENCODING = 1,
		STANDALONE = 2
	};

	/*!
	 * @brief Loads and parse a xml file.
	 * @param Path: Path to the file.
	 * @return True if the file was successfully loaded and parsed, otherwise the function returns false and GetLastErrorStr is set.
	 */
	bool XMLDocument::LoadXMLFile(std::string Path)
	{
		m_Error.clear();
		DivineAPI::File in(Path, DivineAPI::File::OPENMODE::READ);
		if(!in.IsOpen())
		{
			m_Error = "Error: Failed to open file: " + Path;
			return false;
		}

		std::string XML;
		XML.resize(in.Size());

		in.Read(&XML[0], in.Size());
		in.Close();

		if(XML.empty())
		{
			m_Error = "Empty file!";
			return false;
		}

		std::stringstream Stream;
		Stream << XML;

		std::vector<XMLDocument::Token> Tokens = TokenizeStream(Stream);
		std::vector<Token>::iterator Current = Tokens.begin();

		bool Temp;
		bool Ret = ParseTag(nullptr, Current, Tokens.end(), Temp);

		if(!Ret)
		{
			if(m_Root)
			{
				delete m_Root;
				m_Root = nullptr;
			}
		}

		return Ret;
	}

	std::vector<XMLDocument::Token> XMLDocument::TokenizeStream(std::istream &in)
	{
		std::vector<Token> Tokens;
		bool IsDelimiter = true;
		unsigned int Row = 1;
		unsigned int Col = 0;
		std::string Value;
		Token Delimiter;

		while(in.good())
		{
			char c = in.get();
			Col++;

			IsDelimiter = true;

			switch(c)
			{
				case '<':
				{
					Delimiter = {"<", TYPE::LITTLERTHANSIGN, Col, Row};
				}break;

				case '>':
				{
					Delimiter = {">", TYPE::BIGGERTHANSIGN, Col, Row};
				}break;

				case '\"':
				{
					Delimiter = {"\"", TYPE::QUOTATIONMARKS, Col, Row};
				}break;

				case '\t':
				{
					Delimiter = {"\t", TYPE::TABULATOR, Col, Row};
				}break;

				case ' ':
				{
					Delimiter = {" ", TYPE::SPACE, Col, Row};
				}break;

				case '!':
				{
					Delimiter = {"!", TYPE::EXCLAMATIONMARK, Col, Row};
				}break;

				case '-':
				{
					Delimiter = {"-", TYPE::MINUS, Col, Row};
				}break;

				case '?':
				{
					Delimiter = {"?", TYPE::QUESTIONMARK, Col, Row};
				}break;

				case '=':
				{
					Delimiter = {"=", TYPE::ASSIGNOPERATOR, Col, Row};
				}break;

				case '/':
				{
					Delimiter = {"/", TYPE::SLASH, Col, Row};
				}break;

				case '\n':
				{
					Delimiter = {"\n", TYPE::LINEFEED, Col, Row};
					Row++;
					Col = 0;
				}break;

				case '\r':
				{
					Delimiter = {"\r", TYPE::CARRIAGERETURN, Col, Row};
					Col = 0;
				}break;

				default:
				{
					Value += c;
					IsDelimiter = false;
				}break;
			}

			if(IsDelimiter)
			{
				if(!Value.empty())
				{
					Tokens.push_back({Value, TYPE::TEXT, Col, (unsigned int)(Row - Value.size())});
					Value.clear();
				}

				Tokens.push_back(Delimiter);
			}
		}

		return Tokens;
	}

	bool XMLDocument::ParseTag(XMLElement *Root, std::vector<Token>::iterator &Currentpos, std::vector<Token>::iterator End, bool &IsEndtag)
	{
		XMLElement *Element = new XMLElement();
		IsEndtag = false;

		//Indicates the begin of a tag declaration.
		bool ExpectedTagname = false;
		bool CheckTagName = false;
		bool EndTag = false;
		bool OnlyEndTag = false;
		bool SearchContent = false;
		bool Quit = false;

		while(Currentpos != End)
		{
			switch (Currentpos->Type)
			{
				case TYPE::LITTLERTHANSIGN:
				{
					if(SearchContent)
					{
						if(!ParseTag(Element, Currentpos, End, Quit))
						{
							delete Element;
							return false;
						}
					}
					else
						ExpectedTagname = true;
				}break;

				case TYPE::BIGGERTHANSIGN:
				{
					if(EndTag)
						Quit = true;
					else
						SearchContent = true;
				}break;

				case TYPE::SLASH:
				{
					if(ExpectedTagname)
					{
						CheckTagName = true;
						ExpectedTagname = false;
						OnlyEndTag = true;
					}

					EndTag = true;
				}break;

				case TYPE::TEXT:
				{
					//The first text is the tag name.
					if(ExpectedTagname)
					{
						ExpectedTagname = false;
						Element->SetTagName(Currentpos->Value);
					}
					else if(SearchContent)
					{
						std::string Content;

						if (ParseContent(Currentpos, End, Content))
							Element->SetContent(Content);
						else
						{
							m_Error = "Error: End of file reached.";
							delete Element;
							return false;
						}
					}
					else if (CheckTagName)
					{
						if(Root)
						{
							if(Currentpos->Value != Root->GetTagName())
							{
								m_Error = std::string("Endtag found! Line: ") + std::to_string(Currentpos->Row) + std::string(" Column: ") + std::to_string(Currentpos->Col);
								delete Element;
								return false;
							}
							else
								CheckTagName = false;
						}
						else
						{
							m_Error = std::string("Endtag found! Line: ") + std::to_string(Currentpos->Row) + std::string(" Column: ") + std::to_string(Currentpos->Col);
							delete Element;
							return false;
						}
					}
					else
					{
						if(EndTag)
						{
							m_Error = std::string("Endtag can't have attributes! Line: ") + std::to_string(Currentpos->Row) + std::string(" Column: ") + std::to_string(Currentpos->Col);
							delete Element;
							return false;
						}
						else
						{
							XMLAttribute *Attribute = ParseAttribute(Currentpos, End);
							if(!Attribute)
							{
								delete Element;
								return false;
							}

							Element->AddAttribute(Attribute);
						}
					}
				}break;

				case TYPE::TABULATOR:
				case TYPE::CARRIAGERETURN:
				case TYPE::LINEFEED:
				case TYPE::SPACE:
				{
					//Ignore.
				}break;

				default:
				{
					delete Element;
					m_Error = std::string("Content ('") + Currentpos->Value + std::string("') outside of tag! Line: ") + std::to_string(Currentpos->Row) + std::string(" Column: ") + std::to_string(Currentpos->Col);
					return false;
				}break;
			}

			if(Quit)
				break;

			Currentpos++;
		}

		if(OnlyEndTag)
		{
			delete Element;
			IsEndtag = true;
		}
		else
		{
			if(!Root)
				m_Root = Element;
			else
				Root->AddChild(Element);
		}

		return true;
	}

	XMLAttribute *XMLDocument::ParseAttribute(std::vector<Token>::iterator &Currentpos, std::vector<Token>::iterator End)
	{
		XMLAttribute *Attribute = new XMLAttribute();
		std::string Value;
		bool IsValue = false;
		bool AssignOperatorFound = false;
		bool Quit = false;

		while(Currentpos != End)
		{
			switch (Currentpos->Type)
			{
				case TYPE::TEXT:
				{
					if(!IsValue)
						Attribute->SetName(Currentpos->Value);
					else
						Value += Currentpos->Value;
				}break;

				case TYPE::QUESTIONMARK:
				case TYPE::EXCLAMATIONMARK:
				case TYPE::MINUS:
				case TYPE::SLASH:
				{
					if(IsValue)
						Value += Currentpos->Value;
					else
					{
						delete Attribute;
						Attribute = nullptr;
						Quit = true;
						m_Error = std::string("'") + Currentpos->Value + std::string("' not expected! Line: ") + std::to_string(Currentpos->Row) + std::string(" Column: ") + std::to_string(Currentpos->Col);
					}
				}break;

				case TYPE::TABULATOR:
				case TYPE::SPACE:
				{
					if(IsValue)
						Value += Currentpos->Value;
				}break;

				//Checks for quotation marks, which indicates the value for an attribute.
				case TYPE::QUOTATIONMARKS:
				{
					if(IsValue)
					{
						Attribute->SetValue(Value);
						Quit = true;
					}
					else if(AssignOperatorFound)
						IsValue = true;
					else
					{
						delete Attribute;
						Attribute = nullptr;
						Quit = true;
						m_Error = std::string("Quotation mark found but expected a '='! Line: ") + std::to_string(Currentpos->Row) + std::string(" Column: ") + std::to_string(Currentpos->Col);
					}
				}break;

				case TYPE::ASSIGNOPERATOR:
				{
					AssignOperatorFound = true;
				}break;

				default:
				{
					delete Attribute;
					Attribute = nullptr;
					Quit = true;

					m_Error = std::string("'") + Currentpos->Value + std::string("' not expected! Line: ") + std::to_string(Currentpos->Row) + std::string(" Column: ") + std::to_string(Currentpos->Col);
				}break;
			}

			if(Quit)
				break;

			Currentpos++;
		}

		return Attribute;
	}

	bool XMLDocument::ParseContent(std::vector<Token>::iterator &Currentpos, std::vector<Token>::iterator End, std::string &Content)
	{
		while(Currentpos != End)
		{
			if(Currentpos->Type == TYPE::LITTLERTHANSIGN)
			{
				Currentpos--;
				break;
			}

			if(Currentpos->Type != TYPE::LINEFEED && Currentpos->Type != TYPE::CARRIAGERETURN && Currentpos->Type != TYPE::TABULATOR)
				Content += Currentpos->Value;

			Currentpos++;
		}

		return (Currentpos != End);
	}

	/*bool XMLDocument::ParseTokens(std::vector<Token> &Tokens)
	{
		bool IsTagOpen = false, IsComment = false, IsPrologOrPI = false, IsProlog = false, IsEndTag = false, IsPI = false;
		std::string Value;

		std::vector<XMLElement*> ElementStack;
		std::vector<std::string> PrologTags;
		std::vector<std::string> ContentStack;

		PrologTags.push_back("version");
		PrologTags.push_back("encoding");
		PrologTags.push_back("standalone");

		for (size_t i = 0; i < Tokens.size(); ++i)
		{
			if(Tokens[i].Value == "\n")
				continue;

			switch(Tokens[i].Type)
			{
				case TYPE::BEGINTAG:
				{
					//If we find a opentag in a tag there is something wrong.
					if(IsTagOpen)
					{
						Clear();
						m_Error = "Error: There is a tag in a tag! Row: " + std::to_string(Tokens[i].Row);
						return false;
					}

					if(!Value.empty())
					{
						ContentStack.push_back(Value);
						Value.clear();
					}

					IsTagOpen = true;
				}break;

				case TYPE::ENDTAG:
				{
					//If we find a endtag without opentag there is something wrong.
					if(!IsTagOpen)
					{
						Clear();
						m_Error = "Error: There is a close tag without open tag! Row: " + std::to_string(Tokens[i].Row);
						return false;
					}

					if(IsEndTag)
					{
						if(!ElementStack.empty() && (ElementStack.back()->GetTagName() == Tokens[i - 1].Value || (Tokens.size() >= 2 && Tokens[i - 1].Value == "/")))
						{
							if(!ContentStack.empty())
							{
								Value = ContentStack.back();
								ContentStack.pop_back();

								ElementStack.back()->SetContent(Value);
								Value.clear();
							}

							ElementStack.pop_back();
						}
						else
						{
							Clear();
							m_Error = "Error: There is an endtag without opentag! Row: " + std::to_string(Tokens[i].Row);
							return false;
						}
					}

					if(IsProlog || IsPI || IsPrologOrPI)
					{
						Clear();
						m_Error = "Error: Wrong declaration of prolog or PI! Row: " + std::to_string(Tokens[i].Row);
						return false;
					}

					IsEndTag = false;
					IsTagOpen = false;
				}break;

				case TYPE::BEGINCOMMENT:
				{
					IsComment = true;
				}break;

				case TYPE::ENDCOMMENT:
				{
					if(!Value.empty())
					{
						m_Comments.push_back(Value);
						Value.clear();
					}

					IsComment = false;
				}break;

				case TYPE::TAGNAME:
				{
					if(IsPrologOrPI && Tokens[i].Value == "xml")
						IsProlog = true;
					else if(!IsEndTag)
					{
						if(IsPrologOrPI)
							IsPI = true;

						//Creates a new element and push it back on the "stack";
						XMLElement *Element = new XMLElement();
						Element->SetTagName(Tokens[i].Value);

						if(!IsPI)
						{
							//If the current element is not in another element push it on the documents element list, otherwise to the last elements list,
							if(ElementStack.empty())
								m_Root = Element;
							else
								ElementStack.back()->AddChild(Element);
						}

						ElementStack.push_back(Element);
					}

					IsPrologOrPI = false;
				}break;

				case TYPE::ATTRIBUTE:
				{
					if(IsPI)
					{
						Value += Tokens[i].Value;
						break;
					}


					if(ElementStack.empty() && !IsProlog)
					{
						Clear();
						m_Error = "Error: There content outside of any tag! Row: " + std::to_string(Tokens[i].Row);
						return false;
					}
					else if(Tokens[i].Type == TYPE::QUOTES)
					{
						Clear();
						m_Error = "Error: Value without attribute! Row: " + std::to_string(Tokens[i].Row);
						return false;
					}
					else if(Tokens[i].Type == TYPE::QUOTES)
					{
						Clear();
						m_Error = "Error: Assignation without attribute! Row: " + std::to_string(Tokens[i].Row);
						return false;
					}

					Token tt = Tokens[i];
					int QuotesCount = 0, EqualCount = 0;
					XMLAttribute Attr;

					for(; (QuotesCount < 2 && Tokens[i].Type != TYPE::ENDTAG); i++)
					{
						if(Tokens[i].Type == TYPE::ATTRIBUTE)
							Attr.SetName(Tokens[i].Value);
						else if(Tokens[i].Type == TYPE::QUOTES)
							QuotesCount++;
						else if(Tokens[i].Type == TYPE::OPERATOR && QuotesCount == 0)
							EqualCount++;
						else if(QuotesCount == 1 && EqualCount == 1 && !Attr.GetName().empty())
							Attr.SetValue(Attr.GetValue() + Tokens[i].Value);
						else if(Tokens[i].Type != TYPE::WHITESPACE)
						{
							Clear();
							Token tmp = Tokens[i];
							std::string Test = Tokens[i].Value;

							m_Error = "Error: Wrong attribute format! Row: " + std::to_string(Tokens[i].Row);
							return false;
						}

						if(EqualCount > 1)
						{
							Clear();
							m_Error = "Error: Too many operators! Row: " + std::to_string(Tokens[i].Row);
							return false;
						}
					}

					//Decrement by one, because the counter is point to the next object.
					i--;

					if(IsProlog)
					{
						if(m_Version.empty() && Attr.GetName() != PrologTags[(int)PROLOG::VERSION])
						{
							Clear();
							m_Error = "Error: Wrong prolog attribute order! Row: " + std::to_string(Tokens[i].Row);
							return false;
						}
						else if(m_Version.empty() && Attr.GetName() == PrologTags[(int)PROLOG::VERSION])
						{
							m_Version = Attr.GetValue();
						}
						else if(m_Encoding == ENCODING::UNINITIALIZED && Attr.GetName() != PrologTags[(int)PROLOG::ENCODING])
						{
							Clear();
							m_Error = "Error: Wrong prolog attribute order! Row: " + std::to_string(Tokens[i].Row);
							return false;
						}
						else if(m_Encoding == ENCODING::UNINITIALIZED && Attr.GetName() == PrologTags[(int)PROLOG::ENCODING])
						{
							if(LowerString(Attr.GetValue()) == "utf-8")
								m_Encoding = ENCODING::UTF8;
							else
							{
								Clear();
								m_Error = "Error: Unsupported encoding!";
								return false;
							}
						}
					}
					else
						ElementStack.back()->AddAttribute(Attr.GetName(), Attr.GetValue());
				}break;

				case TYPE::INDICATOR:
				{
					if(Tokens[i].Value == "?" && !IsProlog && !IsPI)
						IsPrologOrPI = true;
					else if(Tokens[i].Value == "?" && IsProlog)
						IsProlog = false;
					else if(Tokens[i].Value == "?" && IsPI)
					{
						IsPI = false;
						ElementStack.back()->SetContent(Value);
						m_PIs.push_back(ElementStack.back());

						ElementStack.pop_back();

						Value.clear();
					}
					else if(Tokens[i].Value == "/")
						IsEndTag = true;
				}break;

				case TYPE::WHITESPACE:
				case TYPE::TEXT:
				{
					if(IsComment)
					{
						if((Value.empty() && Tokens[i].Value != "\t") || !Value.empty())
						{
							if(Tokens[i].Value == "\t")
								Value += " ";
							else
								Value += Tokens[i].Value;
						}
					}
					else if(!IsTagOpen || IsPI)
					{
						if((Value.empty() && (Tokens[i].Value != "\t" && Tokens[i].Value != " ")) || !Value.empty())
						{
							if(Tokens[i].Value == "\t")
								Value += " ";
							else if(Tokens[i].Value != "\n" && Tokens[i].Value != "\r")
								Value += Tokens[i].Value;
						}
					}
				}break;
			}
		}

		if(m_Encoding == ENCODING::UNINITIALIZED)
			m_Encoding = ENCODING::UTF8;

		return true;
	}*/

	std::string XMLDocument::LowerString(std::string Str)
	{
		std::string Ret;

		for(size_t i = 0; i < Str.size(); i++)
		{
			Ret += tolower(Str[i]);
		}

		return Ret;
	}

	/*!
	 * @brief Writes this xml document to a file
	 * @param Path: Path to the file.
	 * @return True if the file was successfully written, otherwise false.
	 */
	bool XMLDocument::SaveXMLFile(std::string Path)
	{
		if(!m_Root)
		{
			m_Error = "Error: No root element!";
			return false;
		}

		std::ofstream out(Path, std::ios::out);
		if(!out.is_open())
		{
			m_Error = "Error: Failed to open file for write: " + Path;
			return false;
		}

		std::string Encoding;

		switch (m_Encoding)
		{
			case ENCODING::UTF8:
			{
				Encoding = "utf-8";
			}break;
			default:
			{
				Encoding = "utf-8";
			}break;
		}

		if(m_Version.empty())
		{
			m_Version = "1.0";
		}

		out << "<?xml version=\"" << m_Version << "\" encoding=\"" << Encoding << "\"?>";
		out.flush();

		WriteElementToFile(out, m_Root, 0);

		out.close();
		return true;
	}

	/*!
	 * @brief Sets the root element;
	 */
	void XMLDocument::SetRoot(XMLElement *Root)
	{
		if(m_Root)
		{
			delete m_Root;
			m_Root = nullptr;
		}

		m_Root = Root;
	}

	void XMLDocument::WriteElementToFile(std::ofstream &out, XMLElement *Root, size_t TabIndex)
	{
		std::string Tabs;
		bool EmptyTag = false;

		for(size_t i = 0; i < TabIndex; i++)
			Tabs += "\t";

		out << "\n" << Tabs << "<" << Root->GetTagName();
		XMLAttribute *TmpAttr = Root->GetFirstAttr();
		while(TmpAttr)
		{
			out << " " << TmpAttr->GetName() << "=\"" << TmpAttr->GetValue() << "\"";
			TmpAttr = Root->GetNextAttr();
		}

		if(Root->GetContent().empty() && Root->GetChildCount() == 0)
		{
			EmptyTag = true;
			out << "/>";
		}
		else
			out << ">";

		if(!Root->GetContent().empty())
			out << "\n" << Tabs << "\t" << Root->GetContent();

		out.flush();

		XMLElement *Tmp = Root->GetFirstElement();
		while(Tmp)
		{
			WriteElementToFile(out, Tmp, TabIndex + 1);
			Tmp = Root->GetNextElement();
		}

		if(!EmptyTag)
			out << "\n" << Tabs << "</" << Root->GetTagName() << ">";

		out.flush();
	}

	void XMLDocument::Clear()
	{
		if(m_Root)
		{
			delete m_Root;
			m_Root = NULL;
		}

		if(!m_PIs.empty())
		{
			for (size_t i = 0; i < m_PIs.size(); i++)
			{
				delete m_PIs[i];
			}

			m_PIs.clear();
		}

		m_Comments.clear();
	}
}
