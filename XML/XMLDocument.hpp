/***************************************************************
 * Name:      	XMLDocument.hpp
 * Purpose:   	
 * Author:    	Christian Tost
 * Created:   	11.11.2017
 * Modified by:
 * Copyright: 	(C) Christian Tost
 * License: 	
 **************************************************************/

#ifndef XMLDOCUMENT_HPP_
#define XMLDOCUMENT_HPP_

#include <string>
#include <xml/XMLElement.hpp>
#include <fstream>

namespace DivineXML
{
	enum class ENCODING
	{
		UNINITIALIZED = 0,
		UTF8 = 1
	};

	class XMLDocument
	{
		public:
			XMLDocument() : m_Root(nullptr), m_Encoding(ENCODING::UNINITIALIZED) {}

			/*!
			 * @brief Loads and parse a xml file.
			 * @param Path: Path to the file.
			 * @return True if the file was successfully loaded and parsed, otherwise the function returns false and GetLastErrorStr is set.
			 */
			bool LoadXMLFile(std::string Path);

			/*!
			 * @brief Writes this xml document to a file
			 * @param Path: Path to the file.
			 * @return True if the file was successfully written, otherwise false.
			 */
			bool SaveXMLFile(std::string Path);

			/*!
			 * @brief Sets the root element;
			 */
			void SetRoot(XMLElement *Root);

			/*!
			 * @return Returns the root element of the document.
			 */
			XMLElement *GetRoot()
			{
				return m_Root;
			}

			std::string GetLastErrorStr()
			{
				return m_Error;
			}

			/*!
			 * @return Returns the version of the xml document.
			 */
			std::string GetXMLVersion()
			{
				return m_Version;
			}

			/*!
			 * @return Returns the encoding of the xml document.
			 */
			ENCODING GetEncoding()
			{
				return m_Encoding;
			}

			/*!
			 * @brief Sets the version of this document.
			 */
			void SetXMLVersion(std::string Version)
			{
				m_Version = Version;
			}

			/*!
			 * @brief Sets the encoding of this file.
			 */
			void SetEncoding(ENCODING Encoding)
			{
				m_Encoding = Encoding;
			}

			virtual ~XMLDocument()
			{
				Clear();
			}
		protected:
			void Clear();

		private:
			enum class TYPE
			{
				EXCLAMATIONMARK,
				MINUS,
				ASSIGNOPERATOR,
				QUOTATIONMARKS,
				QUESTIONMARK,
				BIGGERTHANSIGN,
				LITTLERTHANSIGN,
				TEXT,
				CARRIAGERETURN,
				LINEFEED,
				SPACE,
				TABULATOR,
				SLASH
			};

			struct Token
			{
				std::string Value;
				TYPE Type;
				unsigned int Col;
				unsigned int Row;
			};

			void WriteElementToFile(std::ofstream &out, XMLElement *Root, size_t TabIndex);

			std::vector<Token> TokenizeStream(std::istream &in);
			bool ParseTag(XMLElement *Root, std::vector<Token>::iterator &Currentpos, std::vector<Token>::iterator End, bool &IsEndtag);
			XMLAttribute *ParseAttribute(std::vector<Token>::iterator &Currentpos, std::vector<Token>::iterator End);
			bool ParseContent(std::vector<Token>::iterator &Currentpos, std::vector<Token>::iterator End, std::string &Content);

			//bool ParseTokens(std::vector<Token> &Tokens);
			std::string LowerString(std::string Str);

			std::string m_Error;
			std::string m_Version;
			std::vector<std::string> m_Comments;
			std::vector<XMLElement*> m_PIs;

			XMLElement *m_Root;

			ENCODING m_Encoding;
	};
}

#endif /* XMLDOCUMENT_HPP_ */
