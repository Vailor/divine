/***************************************************************
 * Name:      	XMLElement.hpp
 * Purpose:   	
 * Author:    	Christian Tost
 * Created:   	11.11.2017
 * Modified by:
 * Copyright: 	(C) Christian Tost
 * License: 	
 **************************************************************/

#ifndef XMLELEMENT_HPP_
#define XMLELEMENT_HPP_

#include <xml/XMLAttribute.hpp>
#include <vector>
#include <string>

namespace DivineXML
{
	class XMLElement
	{
		public:
			XMLElement() : m_Root(nullptr), m_ElementsPos(-1), m_AttrsPos(-1) {}

			XMLElement(XMLElement *Root);

			/*!
			 * @return Gets the first child.
			 */
			XMLElement *GetFirstElement();

			/*!
			 * @return Gets the next child. Before must @see GetFirstElement be called.
			 */
			XMLElement *GetNextElement();

			/*!
			 * @return Gets the first attribute of this tag.
			 */
			XMLAttribute *GetFirstAttr();

			/*!
			 * @return Gets the next attribute of this tag. Before must @see GetFirstAttr be called.
			 */
			XMLAttribute *GetNextAttr();

			/*!
			 * @return Searchs an attribute by its name and return the attribute, if it was found. otherwise null.
			 */
			XMLAttribute *GetAttrByName(std::string Name);

			/*!
			 * @return Gets a element by its name. Returns null if the child wasn't found.
			 */
			XMLElement *GetElementByName(std::string Name);

			size_t GetChildCount()
			{
				return m_Elements.size();
			}

			/*!
			 * @brief Sets the tag name of this element;
			 */
			void SetTagName(std::string Name)
			{
				m_TagName = Name;
			}

			/*!
			 * @return Gets the tag name of this element;
			 */
			std::string GetTagName()
			{
				return m_TagName;
			}

			/*!
			 * @brief Adds a new attribute to this tag.
			 * @param Name: The name of the attribute.
			 * @param: Value: The value of the attribute.
			 */
			void AddAttribute(std::string Name, std::string Value);

			/*!
			 * @brief Adds a new attribute to this tag.
			 * @param Attribute: Attribute to add.
			 */
			void AddAttribute(XMLAttribute *Attribute);

			/*!
			 * @brief Adds a new child to this element.
			 */
			void AddChild(XMLElement *Child);

			/*!
			 * @brief Removes and frees the given child, if the child exists.
			 * @return Returns 0 on success otherwise -1.
			 */
			int RemoveChild(XMLElement *Child);

			/*!
			 * @brief Removes and frees a child by its name, if the child exists.
			 * @return Returns 0 on success otherwise -1.
			 */
			int RemoveChild(std::string Name);

			/*!
			 * @return Gets the root element of this tag.
			 */
			XMLElement *GetRoot()
			{
				return m_Root;
			}

			void SetContent(std::string Content)
			{
				m_Content = Content;
			}

			std::string GetContent()
			{
				return m_Content;
			}

			virtual ~XMLElement();

		private:
			void ClearElements();

			XMLElement *m_Root;

			std::string m_Content;
			std::string m_TagName;
			std::vector<XMLElement*> m_Elements;
			std::vector<XMLAttribute*> m_Attrs;
			size_t m_ElementsPos;
			size_t m_AttrsPos;
	};
}  // namespace DivineXML

#endif /* XMLELEMENT_HPP_ */
