/***************************************************************
 * Name:      	XMLAttributes.hpp
 * Purpose:   	
 * Author:    	Christian Tost
 * Created:   	11.11.2017
 * Modified by:
 * Copyright: 	(C) Christian Tost
 * License: 	
 **************************************************************/

#ifndef XMLATTRIBUTE_HPP_
#define XMLATTRIBUTE_HPP_

#include <string>

namespace DivineXML
{
	class XMLAttribute
	{
		public:
			XMLAttribute() {}

			/*!
			 * @brief Sets the attribute name.
			 */
			void SetName(std::string Name)
			{
				m_Name = Name;
			}

			/*!
			 * @brief Sets the attribute value.
			 */
			void SetValue(std::string Value)
			{
				m_Value = Value;
			}

			/*!
			 * @return Gets the attribute name.
			 */
			std::string GetName()
			{
				return m_Name;
			}

			/*!
			 * @return Gets the attribute value.
			 */
			std::string GetValue()
			{
				return m_Value;
			}

			virtual ~XMLAttribute(){}
		private:
			std::string m_Name;
			std::string m_Value;
	};
}

#endif /* XMLATTRIBUTES_HPP_ */
