APP_OPTIM := release
APP_CPPFLAGS += -fexceptions
APP_CPPFLAGS += -frtti
APP_STL := c++_static
APP_ABI := all

# Min runtime API level
APP_PLATFORM=android-19
