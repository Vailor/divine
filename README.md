# Divine Engine
The Divine engine is a lightweight and simple engine for 2D rendering, it uses OpenGL for rendering and runs on Windows, Linux and Android.

## Getting Started

### Prerequisites

* [mingw](https://mingw-w64.org/doku.php/download), gcc or [Visual Studio](https://visualstudio.microsoft.com/de/) with C++
* SDL2 can be downloaded [here](https://libsdl.org/download-2.0.php)
* cmake can be downloaded [here](https://cmake.org/download/)
* The source of the engine.

For Linux you must download these programs and libraries over your package manager. You need also the opengl-dev(Warning not the real package name) package.

###### Optionally

* Android SDK and NDK, if you want to compile the engine for Android
* Doxygen, if you want to generate the api documentation
* git for cloning the repo


### Building

After you have downloaded and installed the requirements you can build the engine.

###### Under Linux

Open a terminal

```
cd /path/to/Divine project
mkdir build
cd build/
cmake-gui ../
```

###### Under Windows

* Go into the Divine project directory
* Create a directory with the name build
* Start cmake-gui.exe
* In cmake set the source directory to the Project root directory and set the build directory to the directory you have been created.

###### For Android

* Compile the libSDL2 for Android
* Go into the Divine project directory
* Create a directory which is named "SDL"
* Go into this directory and create the directory libs.
* Copy the android library of SDL2 in this directory
* Create a directory SDL2 in the SDL directory and copy the include files of the SDL into this directory
* Run ndk-build in the Divine project directory.

## Third party libraries

 * zlib
 * tinyxml2
 * stb_image.h
 * stb_image_write.h
 * stb_truetype.h
 * SDL2
 * Google's OpenSans-Regular.ttf

## License

This project is licensed under the zlib License - see the [LICENSE.md](LICENSE.md) file for details
