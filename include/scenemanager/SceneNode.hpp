/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef SCENENODE_HPP_
#define SCENENODE_HPP_

#include <platform.h>
#include <vector>
#include <math/Matrix.hpp>
#include <math/Vector2D.hpp>

namespace DivineAPI
{
	class CSceneManager;

	/*!
	 * @brief Since this engine has a node rendering system, this class is the base of all nodes.
	 */
	class DIVINE_API CSceneNode
	{
		public:
			/*!
			 * @param smgr: The scenemanager which should be managed this node.
			 * @param Parent: The parent node of this node.
			 * @param ZOrder: The rendering order. Negative values will be rendered first and positive values are rendered at last.
			 */
			CSceneNode(CSceneManager *smgr, CSceneNode *Parent, int ZOrder = 0);

			/*!
			 * @brief Sets the parent of this CSceneNode.
			 * 
			 * @param Parent: The new parent Scene.
			 */
			void SetParent(CSceneNode *Parent);

			/*!
			 * @brief Adds a new child node to this node.
			 * 
			 * @param Node: Child to add.
			 * @param ZOrder: The rendering order. Negative values will be rendered first and positive values are rendered at last.
			 */
			void AddNode(CSceneNode *Node, int ZOrder = 0);

			/*!
			 * @brief Removes a child node.
			 * 
			 * @param Node: The child to remove.
			 */
			void RemoveNode(CSceneNode *Node);

			/*!
			 * @brief Specifies the order in which the element should be rendered.
			 * @param ZOrder: The rendering order. Negative values will be rendered first and positive values are rendered at last.
			 */
			void SetZOrder(int ZOrder);

			/*!
			 * @brief For the logic of inherited nodes.
			 */
			virtual void Update();

			/*!
			 * @brief Draws this node with all its children.
			 */
			virtual void Render();

			/*!
			 * @brief Cleanups all child nodes.
			 */
			virtual void Cleanup();

			/*!
			 * @brief Rotates a node.
			 * 
			 * @param Angle: The angle to rotate.
			 * @param Axis: The axis to rotate around.
			 */
			void Rotate(float Angle, Vec3Df Axis);

			/*!
			 * @brief Scales the node.
			 */
			void Scale(Vec2Df ScaleVal);

			/*!
			 * @brief Sets the position of this node.
			 */
			void SetPosition(Vec2Df Pos)
			{
				this->Pos = Pos;
				UpdateWorldCoordinate(m_ParentWorldCoordinate);
			}

			/*!
			 * @return Gets the position.
			 */
			Vec2Df GetPosition() const
			{
				return Pos;
			}

			/*!
			 * @return Gets the size of this node.
			 */
			Vec2Di GetSize() const
			{
				return m_Size;
			}

			Vec2Df GetWorldCoordinate() const
			{
				return m_WorldCoordinate;
			}

			/*!
			 * @brief Sets the visibility of this node.
			 * 
			 * @param State: The visibility state.
			 * 
			 * @attention If this node is hidden all child nodes are hidden too.
			 */
			void SetVisibility(bool State)
			{
				m_Visible = State;
			}

			/*!
			 * @return Gets the visibility state.
			 */
			bool IsVisible() const
			{
				return m_Visible;
			}

			/*!
			 * @return Gets the ZOrder.
			 */
			int GetZOrder() const
			{
				return m_ZOrder;
			}

			/*!
			 * @return Gets the parent scene.
			 */
			CSceneNode *GetParent() const
			{
				return m_Parent;
			}

			/*!
			 * @return Gets the scenemanager of this node.
			 */
			CSceneManager *GetSceneManager() const
			{
				return m_SceneManager;
			}

			void UpdateWorldMatrix(Matrix World);

			virtual ~CSceneNode();

		protected:
			/*!
			 * @return Gets the local matrix of this node.
			 */
			virtual Matrix GetLocalMatrix();

			/*!
			 * @return Gets the world matrix of this node.
			 */
			Matrix GetWorldMatrix() const
			{
				return m_World;
			}

			Matrix m_Rotation;
			Matrix m_Scale;
			Vec2Df Pos;

			void SetSize(Vec2Di Size)
			{
				m_Size = Size;
			}

			Vec2Df GetParentWorldCoordinate() const
			{
				return m_ParentWorldCoordinate;
			}

		private:
			void UpdateWorldCoordinate(Vec2Df ParentWorldCoordinate);

			void AddNodeInternal(std::vector<CSceneNode*> &Side, CSceneNode *Node, int ZOrder);
			static void RemoveNodeInternal(std::vector<CSceneNode*> &Side, CSceneNode *Node);

			CSceneNode *m_Parent;
			CSceneManager *m_SceneManager;
			Matrix m_World;
			Vec2Di m_Size;
			Vec2Df m_WorldCoordinate;
			Vec2Df m_ParentWorldCoordinate;

			std::vector<CSceneNode*> m_Left, m_Right;
			int m_ZOrder;
			bool m_Visible;
	};
}  // namespace DivineAPI

#endif /* SCENENODE_HPP_ */
