/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef SCENEMANAGER_HPP_
#define SCENEMANAGER_HPP_

#include <platform.h>
#include <scenemanager/SceneNode.hpp>

namespace DivineAPI
{
    class IWindow;

    class DIVINE_API CSceneManager : public CSceneNode
    {
        public:
        	explicit CSceneManager(IWindow *Parent) : CSceneNode(nullptr, nullptr), m_Parent(Parent) { }

            /*!
             * @brief Creates and adds a child node.
             */
            template<class T>
            T* AddNode(CSceneNode *Parent, int ZOrder = 0)
            {
                if(!Parent)
                    Parent = this;

                return new T(this, Parent, ZOrder);
            }

			/*!
			 * @return Gets the window of this scenemanager.
			 */
			IWindow *GetWindow()
            {
                return m_Parent;
            }

            void Test();

            virtual ~CSceneManager() {}

        private:
            IWindow *m_Parent;
    };
}

#endif