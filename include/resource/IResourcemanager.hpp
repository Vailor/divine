/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef IRESOURECMANAGER_HPP
#define IRESOURECMANAGER_HPP

#include <platform.h>
#include <image/Image.hpp>

namespace DivineAPI
{
	class CBaseResource;
	class IWindow;
	class ITexture;
	class CFontAtlas;

	/*!
	 * @brief Loads and manages some ressources.
	 */
    class DIVINE_API IResourcemanager
    {
        public:
			/*!
			 * @brief Loads and creates a texture.
			 * 
			 * @param Path: Path to the texture to load.
			 * 
			 * @return Returns a texture.
			 * 
			 * @throw std::runtime_error Throws on error.
			 */
			virtual ITexture *LoadTexture(const std::string &Path) = 0;

			/*!
			 * @brief Creates a texture from an CImage.
			 * 
			 * @param img: The image to use.
			 * 
			 * @return Returns a texture.
			 * 
			 * @throw std::runtime_error Throws on error.
			 */
			virtual ITexture *CreateTextureFromImage(const CImage &img) = 0;

			/*!
			 * @brief Loads a font from file.
			 * 
			 * @param Path: Path to the font file.
			 * 
			 * @return Returns the loaded font.
			 * 
			 * @throw std::runtime_error Throws on error.
			 */
			virtual CFontAtlas *LoadFont(const std::string &Path) = 0;

			/*!
			 * @brief Loads a font from memory.
			 * 
			 * @param Data: Data of the font.
			 * @param Size: Size of the data.
			 * 
			 * @return Returns the loaded font.
			 * 
			 * @throw std::runtime_error Throws on error.
			 */
			virtual CFontAtlas *LoadMemoryFont(const unsigned char *Data, size_t Size) = 0;

			/*!
			 * @brief Gets the default font.
			 */
			virtual CFontAtlas *GetDefaultFont() = 0;

			/*!
			 * @brief Deletes a resource.
			 */
			virtual void DeleteResource(CBaseResource *Res) = 0;

			virtual IWindow *GetParent() = 0;

            virtual ~IResourcemanager() {}
    };

	class CBaseResource
	{
		public:
			CBaseResource(IResourcemanager *rmgr) : m_Rmgr(rmgr), m_RefCount(0) {}

			/*!
			 * @brief Increments the ref counter.
			 */
			void Grab()
			{
				m_RefCount++;
			}

			/*!
			 * @brief Decrements the ref counter
			 */
			virtual void Ungrab()
			{
				m_RefCount--;

				if(m_Rmgr && m_RefCount == 0)
					m_Rmgr->DeleteResource(this);
			}

			/*!
			 * @brief Frees a resource.
			 */
			virtual void Free()
			{
				delete this;
			}

			void SetKey(const std::string &Key)
			{
				m_Key = Key;
			}

			std::string GetKey()
			{
				return m_Key;
			}

			virtual ~CBaseResource() {}

		protected:
			IResourcemanager *m_Rmgr;

		private:
			size_t m_RefCount;
			std::string m_Key;
	};
}

#endif