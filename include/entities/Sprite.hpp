/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef SPRITE_HPP_
#define SPRITE_HPP_

#include <platform.h>
#include <entities/RenderNode.hpp>
#include <driver/ITexture.hpp>
#include <image/Image.hpp>
#include <scenemanager/SceneManager.hpp>

namespace DivineAPI
{
	/*!
	 * @brief With the CSprite class you can load and render spritesheets and animations.
	 */
	class DIVINE_API CSprite : public CRenderNode
	{
		public:
			/*!
			 * @brief Same as RenderNode::RenderNode.
			 */
			CSprite(CSceneManager *smgr, CSceneNode *Parent, int ZOrder = 0);

			/*!
			 * @brief Loads an non-animated sprite.
			 * 
			 * @param Path: Path to the sprite file.
			 * @throw std::runtime_error Throws if the sprite couldn't load.
			 * 
			 * @attention All image formats supported by the CImage class can be loaded.
			 */
			void LoadSprite(std::string Path);

			/*!
			 * @brief Loads an animated sprite.
			 * 
			 * @param Path: Path to the sprite file.
			 * @param Tilesize: Size of each tile in the spritesheet.
			 * @param Pos: Animationposition to render.
			 * 
			 * @throw std::runtime_error Throws if the sprite couldn't load.
			 * 
			 * @attention All image formats supported by the CImage class can be loaded.
			 */
			void LoadSprite(std::string Path, Vec2Di Tilesize, unsigned int Pos);

			/*!
			 * @brief Loads an non-animated sprite.
			 * 
			 * @param img: Loaded sprite file.
			 * @throw std::runtime_error Throws if the sprite couldn't load.
			 * 
			 * @attention All image formats supported by the CImage class can be loaded.
			 */
			void LoadSprite(const CImage &img);

			/*!
			 * @brief Loads an animated sprite.
			 * 
			 * @param img: Loaded sprite file.
			 * @param Tilesize: Size of each tile in the spritesheet.
			 * @param Pos: Animationposition to render.
			 * 
			 * @throw std::runtime_error Throws if the sprite couldn't load.
			 * 
			 * @attention All image formats supported by the CImage class can be loaded.
			 */
			void LoadSprite(const CImage &img, Vec2Di Tilesize, unsigned int Pos);

			/*!
			 * @brief Sets the current animationposition.
			 * 
			 * @param Pos: Animationposition to render.
			 */
			void SetAnimationPos(int Pos);

			virtual void Render();
			virtual void Cleanup();

			/*!
			 * @brief Clears the buffer and deletes the texture.
			 */
			void DeleteSprite();

			/*!
			 * @return Gets the size of the texture.
			 */
			Vec2Di GetImageSize() const
			{
				return m_ImgSize;
			}

			virtual ~CSprite() {}

		protected:
			Vec2Di m_ImgSize;	//!< Texture size.
			ITexture *m_Texture;

			bool m_IsAnimated;

		private:
			void CreateSprite(unsigned int Pos);
	};
} /* namespace DivineAPI */

#endif /* SPRITE_HPP_ */
