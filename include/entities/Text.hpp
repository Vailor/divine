/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef TEXT_HPP_
#define TEXT_HPP_

#include <platform.h>
#include <entities/RenderNode.hpp>
#include <driver/ITexture.hpp>
#include <text/FontAtlas.hpp>
#include <scenemanager/SceneManager.hpp>
#include <window/IWindow.hpp>

namespace DivineAPI
{
	class DIVINE_API CText : public CRenderNode
	{
		public:
			CText(CSceneManager *smgr, CSceneNode *Parent, int ZOrder = 0);

			virtual void Render();

			/*!
			 * @brief Sets the text for rendering.
			 * 
			 * @param Text: UTF-8 String.
			 */
			void SetText(std::string Text);

			void SetFont(CFontAtlas *Font)
			{
				m_Atlas->Ungrab();
				m_Atlas = Font;
			}

			void SetFontSize(int Size)
			{
				m_FontSize = Size;
				m_Atlas->SetFontSize(Size);
				SetText(m_Text);
			}

			void SetFontColor(Color color)
			{
				m_TextColor = color;
			}

			std::string GetText() const
			{
				return m_Text;
			}

			CFontAtlas *GetFont() const
			{
				return m_Atlas;
			}

			int GetTextsize() const
			{
				return m_FontSize;
			}

			Color GetFontcolor() const
			{
				return m_TextColor;
			}

			virtual ~CText() 
			{
				m_Atlas->Ungrab();
			}
		private:
			std::string m_Text;
			int m_FontSize;
			CFontAtlas *m_Atlas;
			Color m_TextColor;
	};
}  // namespace DivineAPI

#endif /* TEXT_HPP_ */
