/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef RENDERNODE_HPP_
#define RENDERNODE_HPP_

#include <string>

#include <platform.h>
#include <math/Vector2D.hpp>
#include <math/Vector3D.hpp>
#include <utils/Color.hpp>
#include <driver/IDriver.hpp>
#include <scenemanager/SceneNode.hpp>

namespace DivineAPI
{
	/*!
	 * @brief Basic node for entities to be displayed on the screen.
	 */
	class DIVINE_API CRenderNode : public CSceneNode
	{
		public:
			/*!
			 * @brief Same as CSceneNode::CSceneNode.
			 */
			CRenderNode(CSceneManager *smgr, CSceneNode *Parent, int ZOrder = 0);

			/*!
			 * @brief Applies the shader to be used for this entity.
			 * 
			 * @param Shader: The shader id to use.
			 * 
			 * @throw std::invalid_argument Throws if the shader id doesn't exists.
             * @throw std::runtime_error Throws if the shader couldn't compile or link.
			 */
			virtual void ApplyShader(uint32_t Shader);

			virtual void Cleanup();

			virtual void Render();

			virtual ~CRenderNode() {}
		protected:
			IObjectBuffer *m_Buffer;	//!< The vertexbuffer of this node.
			IShaderManager *m_Shader;
			uint32_t m_ShaderID;		//!< Current applied shader.
	};
}  // namespace DivineAPI

#endif /* RENDERNODE_HPP_ */
