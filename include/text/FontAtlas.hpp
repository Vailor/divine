/*
 * zlib License
 *
 * (C) 2019 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef FONTATLAS_HPP_
#define FONTATLAS_HPP_

#include <string>
#include <filesystem/IFile.hpp>
#include <driver/ITexture.hpp>
#include <text/stb_truetype.h>
#include <memory>
#include <vector>
#include <resource/IResourcemanager.hpp>

namespace DivineAPI
{
    /*!
     * @brief Dynamic font atlas, which resize the texture if no more space is available.
     */
    class CFontAtlas : public CBaseResource
    {
        public:
            struct Glyph
            {
                int Codepoint;
                Vec2Dui Pos;        //!< Position on the texture.
                Vec2Dui Size;       //!< Texture size.

                int Advance;
                Vec2Di Bearing;
                int Baseline;
            };

            CFontAtlas(IResourcemanager *ResMgr) : CBaseResource(ResMgr), m_Data(nullptr), m_FontSize(14), m_ConstMem(false) { }

            CFontAtlas(IResourcemanager *ResMgr, const std::string &Path, int FontSize = 14) : CFontAtlas(ResMgr) 
            {
                LoadFont(Path, FontSize);
            }

            CFontAtlas(IResourcemanager *ResMgr, const uint8_t *Data, size_t Size, int FontSize = 14) : CFontAtlas(ResMgr) 
            {
                LoadFont(Data, Size, FontSize);
            }

            /*!
             * @brief Loads a font from a file.
             * 
             * @param Path: Font to load.
             * @param FontSize: Size of the font in points.
             * 
             * @throw std::runtime_error Throws on error.
             */
            void LoadFont(const std::string &Path, int FontSize = 14);

            /*!
             * @brief Loads a font from memory.
             * 
             * @param Data: Font data.
             * @param Size: Size of the data.
             * @param FontSize: Size of the font in points.
             * 
             * @throw std::runtime_error Throws on error.
             */
            void LoadFont(const uint8_t *Data, size_t Size, int FontSize = 14);

            std::vector<Glyph> RenderText(const std::string &Text);

            void SetFontSize(int FontSize);

            int GetFontSize()
            {
                return m_FontSize;
            }

            ITexture *GetTexture()
            {
                return m_Resolutions[m_FontSize].Atlas;
            }

            int GetHeigth()
            {
                return m_Resolutions[m_FontSize].Height;
            }

            int GetLinegap()
            {
                return m_Resolutions[m_FontSize].Linegap;
            }

            std::string GetCopyright()
            {
                return m_Copyright;
            }

            std::string GetAuthor()
            {
                return m_Author;
            }

            std::string GetFontname()
            {
                return m_Fontname;
            }

            virtual ~CFontAtlas();

        private:
            enum
            {
                COPYRIGHT,
                FONTFAMILIENAME,
                AUTHOR = 8
            };

            struct Fontresolution
            {
                int Height, Linegap;
                ITexture *Atlas;
                uint32_t XPos, YPos, RowH;
                std::map<int, Glyph> Glyphs;

                Fontresolution() : Height(0), Linegap(0), Atlas(nullptr), XPos(0), YPos(0), RowH(0) {}

                ~Fontresolution()
                {
                    if(Atlas)
                        Atlas->Free();
                }
            };

            std::string LoadInfo(int ID);

            float PointsToPixel(int PtSize);
            void InitFont();

            void LoadGlyph(int Codepoint);
            void CalculateHeightAndLinegap();

            bool m_ConstMem;

            std::string m_Copyright, m_Author, m_Fontname;

            stbtt_fontinfo m_Info;
            uint8_t *m_Data;
            int m_FontSize;

            std::map<int, Fontresolution> m_Resolutions;
    };
} // DivineAPI

#endif