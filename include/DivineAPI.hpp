/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef DIVINEAPI_HPP_
#define DIVINEAPI_HPP_

#include <entities/RenderNode.hpp>
#include <entities/Sprite.hpp>
#include <entities/Text.hpp>
#include <engine/Engine.hpp>
#include <engine/IGameLoop.hpp>
#include <image/IImageHandler.hpp>
#include <image/Image.hpp>
#include <input/Event.hpp>
#include <input/IInputHandler.hpp>
#include <input/KeyboardEvent.hpp>
#include <input/MouseEvent.hpp>
#include <input/InputManager.hpp>
#include <resource/IResourcemanager.hpp>
#include <driver/ITexture.hpp>
#include <driver/IShaderManager.hpp>
#include <driver/IDriver.hpp>
#include <text/FontAtlas.hpp>
#include <window/IWindow.hpp>
#include <platform.h>
#include <utils/Log.hpp>
#include <utils/System.hpp>
#include <utils/zstream.hpp>
#include <filesystem/IFile.hpp>
#include <xml/XML.hpp>
#include <platform.h>

#endif /* DIVINEAPI_HPP_ */
