/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef TIMER_HPP_
#define TIMER_HPP_

#include <platform.h>

namespace DivineAPI
{
	class Divine;

	/*!
	 * @brief Base class for timers. These timers updates every frame.
	 */
	class DIVINE_API CBaseTimer
	{
		friend Divine;

		public:
			/*!
			 * @param Started: Starts the timer.
			 */
			explicit CBaseTimer(bool Started = false) : m_Start(0), m_Elapsed(0), m_Running(false)
			{
				if(Started)
					Start();
			}

			/*!
			 * @brief Starts the timer.
			 */
			virtual void Start();

			/*!
			 * @brief Resets the start and elapsed time, without stopping the timer.
			 */
			virtual void Reset();

			/*!
			 * @brief Stops the timer.
			 */
			virtual void Stop();

			/*!
			 * @return Gets the elapsed time, since the last frame.
			 */
			float GetElapsed() const
			{
				return m_Elapsed;
			}

			bool IsRunning() const
			{
				return m_Running;
			}

			virtual ~CBaseTimer()
			{
				Stop();
			}

		protected:
			/*!
			 * @brief Updates the timer.
			 * 
			 * @attention Is called by the engine main loop.
			 */
			virtual void Update();

		private:
			float m_Start;
			float m_Elapsed;
			bool m_Running;
	};
}  // namespace DivineAPI

#endif /* TIMER_HPP_ */
