/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef ENGINE_HPP_
#define ENGINE_HPP_

#include <platform.h>
#include <timer/BaseTimer.hpp>
#include <engine/IGameLoop.hpp>
#include <input/IInputHandler.hpp>
#include <vector>

namespace DivineAPI
{
	/*!
	 * @brief This static class is the heart of the engine. This class manages the logic.
	 */
	class DIVINE_API Divine
	{
		public:
			/*!
			 * @brief Initialize the engine.
			 * 
			 * @throw std::runtime_error Throws if the engine couldn't initialized.
			 */
			static void Init();

			/*!
			 * @brief Handles the logic.
			 */
			static void Run();

			/*!
			 * @brief Deinitialize the engine and cleanup.
			 */
			static void Quit();

			/*!	
				http://semver.org/
			*/
			static std::string GetVersion();

			static void RegisterInputHandler(IInputHandler *Handler);
			static void RegisterGameLoop(IGameLoop *Loop);
			static void RegisterTimer(CBaseTimer *tmr);
			static void RemoveTimer(CBaseTimer *tmr);
		private:
			static void UpdateTimers();

			static IInputHandler *m_Handler;
			static IGameLoop *m_Loop;
			static std::vector<CBaseTimer*> m_Timers;
	};
}  // namespace DivineAPI

#endif /* ENGINE_HPP_ */
