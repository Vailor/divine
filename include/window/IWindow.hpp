/*
 * zlib License
 * 
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef IWINDOW_H
#define IWINDOW_H

#include <platform.h>
#include <string>
#include <math/Vector2D.hpp>
#include <input/IInputHandler.hpp>
#include <utils/Color.hpp>
#include <driver/IDriver.hpp>
#include <math/Matrix.hpp>
#include <resource/IResourcemanager.hpp>
#include <scenemanager/SceneManager.hpp>
#include <driver/IShaderManager.hpp>

namespace DivineAPI
{
    class CInputManager;
    class Divine;

    class DIVINE_API IWindow
    {
        friend CInputManager;
        friend Divine;

        public:
            enum class WindowFlags
            {
                DEFAULT = 0,        //!< Default window.
                HIDDEN = 1,         //!< Hidden window
                POPUP = 2,          //!< Borderless window
                MAXIMIZED = 4,      //!< Maximized window
                MINIMIZED = 8,      //!< Minimized window
                RESIZEABLE = 16     //!< Resizable window.
            };

            /*!
             * @brief Creates a window.
             * 
             * @param Title: Window title.
			 * @param Pos: Position of the window.
			 * @param Size: Size of the window.
             * @param Flags: The window flags. See also WindowFlags.
             * 
             * @throw std::runtime_error Throws if an error occures.
             */
			static IWindow *Create(std::string Title, Vec2Di Pos, Vec2Dui Size, WindowFlags Flags = WindowFlags::DEFAULT);

            /*!
             * @brief Sets the visibility state of the window.
             * 
             * @param show: If true the window is visible otherwise the window is hidden.
             */
			virtual void Show(bool show = true) = 0;

			/*!
             * @brief Close the window.
             */
			virtual void Close() = 0;

            /*!
             * @brief Destorys the window.
             */
			virtual void Destroy() = 0;

			/*!
             * @brief Adds an input handler for this window.
             * 
             * @throw std::invalid_argument Throws if the handler is null.
             */
			virtual void AddInputHandler(IInputHandler *Handler) = 0;

            /*!
             * @brief Removes an input handler.
             * 
             * @throw std::invalid_argument Throws if the handler is null.
             */
			virtual void RemoveInputHandler(IInputHandler *Handler) = 0;

            /*!
             * @throw std::runtime_error Throws if fullscreen mode failed
             */
			virtual void SetFullscreen(bool Fullscreen) = 0;

			/*!
             * @brief Sets the minimum size of the window.
             */
			virtual void SetMinSize(Vec2Di MinSize) = 0;

			/*!
             * @brief Sets the maximum size of the window.
             */
			virtual void SetMaxSize(Vec2Di MaxSize) = 0;

            /*!
             * @brief Sets the size of the window.
             */
            virtual void SetWindowSize(Vec2Dui Size) = 0;

            /*!
             * @brief Sets the title of the window.
             */
            virtual void SetWindowTitle(std::string Title) = 0;

            /*!
             * @return Gets the title of the window.
             */
            virtual std::string GetWindowTitle() = 0;

            /*!
             * @return Gets the minimum size of the window.
             */
			virtual Vec2Di GetMinSize() = 0;

            /*!
             * @return Gets the maximum size of the window.
             */
			virtual Vec2Di GetMaxSize() = 0;
            
            /*!
             * @brief Binds the current render context.
             */
            virtual void BindContext() = 0;
            
            /*!
             * @return Gets the current window size.
             */
            virtual Vec2Di GetWindowSize() = 0;

			/*!
			 * @brief Enables vsync. 
			 */
			virtual void EnableVSync(bool Enable) = 0;

			/*!
             * @brief Sets the background color of the window.
             */
			virtual void SetBackgroundColor(Color Color) = 0;

			/*!
             * @return Gets the current used driver for rendering.
             */
            virtual IDriver *GetDriver() = 0;

            /*!
             * @return Gets the shader manager.
             */
            virtual IShaderManager *GetShaderManager() = 0;
			
			/*!
             * @return Gets the background color.
             */
			virtual Color GetBackgroundColor() = 0;

			/*!
             * @brief Checks if the window is closed.
             */ 
			virtual bool IsClosed() = 0;

			/*!
             * @brief Checks if the window is in fullscreen mode.
             */ 
			virtual bool IsFullscreen() = 0;

            /*!
             * @return Gets the scene manager.
             */
			virtual CSceneManager *GetSceneManager() = 0;

            /*!
             * @return Gets the ressource manager.
             */
			virtual IResourcemanager *GetResourceManager() = 0;

            /*!
             * @return Gets the window id of this window.
             * 
             * @throw std::runtime_error Throws if the window is not initialized.
             */ 
            virtual uint32_t GetWindowID() = 0;

            virtual ~IWindow() {}
        protected:
            /*!
             * @brief Called by the engine main loop.
             */
            virtual void Run() = 0;

            virtual void ProcessResize(Vec2Dui Size) = 0;
			virtual void ProcessKeyboardEvent(CKeyboardEvent ev) = 0;
			virtual void ProcessMouseEvent(CMouseEvent ev) = 0;
    };

    inline IWindow::WindowFlags operator|(IWindow::WindowFlags a, IWindow::WindowFlags b)
    {
        return static_cast<IWindow::WindowFlags>(static_cast<int>(a) | static_cast<int>(b));
    }

    inline IWindow::WindowFlags operator&(IWindow::WindowFlags a, IWindow::WindowFlags b)
    {
        return static_cast<IWindow::WindowFlags>(static_cast<int>(a) & static_cast<int>(b));
    }

    inline IWindow::WindowFlags &operator|=(IWindow::WindowFlags &a, IWindow::WindowFlags b)
    {
        return a = a | b;
    }

    inline IWindow::WindowFlags &operator&=(IWindow::WindowFlags &a, IWindow::WindowFlags b)
    {
        return a = a & b;
    }

    inline IWindow::WindowFlags operator~(IWindow::WindowFlags a)
    {
        return static_cast<IWindow::WindowFlags>(~static_cast<int>(a));
    }
}

#endif // IWINDOW_H
