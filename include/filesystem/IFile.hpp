/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef IFILE_HPP_
#define IFILE_HPP_

#include <platform.h>
#include <string>
#include <stdint.h>

namespace DivineAPI
{    
	/*!
	 * @brief File interface to interacting with the filesystem.
	 */
	class DIVINE_API IFile
	{
		public:
			/*!
			 * @brief There are several modes for opening files.
			 */
			enum class Openmode
			{
				READ,	//!< The file can only be read. 
				WRITE,	//!< To the file can only be written.
				RW,		//!< The file can be readed and written.
				APPEND 	//!< Appends data at the end of the file.
			};

			enum class Seekpos
			{
				BEG,	//!< The cursor will be move from the beginning position of the file.
				END,	//!< The cursor will be move from the ending position of the file.
				CUR		//!< The cursor will be move from the current position of the file.
			};

			/*!
			 * @brief Creates an instance for file handling.
			 * 
			 * @param Path: Path to the file.
			 * @param mode: See Openmode.
			 * 
			 * @throw std::runtime_error Throws if the combination of Path and mode lead to an error.
			 */
			static IFile *Create(std::string Path, Openmode mode);

			/*!
			 * @return Gets the file extension of a given file. An empty string is returned if no extension is found.
			 */
			static std::string GetFileExtension(const std::string &Path);

			/*!
			 * @return Gets the path of a file. An empty string is returned if no path delimiter is found.
			 * 
			 * @attention The path includes the the last path delimiter. 
			 */
			static std::string GetFilePath(const std::string &Path);

			/*!
			 * @return Gets the file name.
			 */
			static std::string GetFileName(const std::string &Path);

			/*!
			 * @brief Opens a file.
			 * 
			 * @param Path: Path to the file.
			 * @param mode: See Openmode.
			 * 
			 * @throw std::runtime_error Throws if the combination of Path and mode lead to an error.
			 */
			virtual void Open(const std::string &Path, Openmode mode) = 0;

			/*!
			 * @brief Reads a line of a file.
			 * 
			 * @return Returns a line or an empty string, if the file is empty or the end of the file is reached.
			 * 
			 * @throw std::runtime_error Throws if the file isn't opened in read mode.
			 */
			virtual std::string ReadLn() = 0;

			/*!
			 * @brief Reads data from the file.
			 * 
			 * @param Buf: The buffer to store the data in.
			 * @param Size: The size to read.
			 * 
			 * @return Returns the size of readed data.
			 * 
			 * @throw std::runtime_error Throws if the file isn't opened in read mode.
			 */
			virtual size_t Read(char *Buf, size_t Size) = 0;

			/*!
			 * @brief Writes data to the file.
			 * 
			 * @param Buf: The buffer to be written.
			 * @param Size: The size to be written.
			 * 
			 * @return Returns the size of the written data.
			 * 
			 * @throw std::runtime_error Throws if the file isn't opened in write mode or if the data couldn't be written.
			 */
			virtual size_t Write(const char *Buf, size_t Size) = 0;

			/*!
			 * @brief Moves the cursor to a specified position.
			 * 
			 * @param Pos: The size in bytes to move the cursor.
			 * @param From: See Seekpos.
			 * 
			 * @return Returns the current position in the datastream.
			 * 
			 * @throw std::runtime_error Throws if seek fails.
			 */
			virtual int64_t Seek(int64_t Pos, Seekpos From) = 0;

			/*!
			 * @return Gets the current position in the datastream.
			 * 
			 * @throws std::runtime_error Throws if tell fails.
			 */
			virtual int64_t Tell() = 0;

			/*!
			 * @brief Gets the next character of the file.
			 * @return Returns -1 if the end of file is reached, otherwise the next byte.
			 * 
			 * @throw std::runtime_error Throws if the file isn't opened in read mode.
			 */
			virtual int Get() = 0;

			/*!
			 * @return Returns true, if the end of file is reached.
			 */
			virtual bool IsEOF() = 0;

			/*!
			 * @brief Closes the file.
			 */
			virtual void Close() = 0;

			/*!
			 * @return Gets the total file size.
			 */
			virtual size_t Size() = 0;

			/*!
			 * @return Returns true if the file is opened.
			 */
			virtual bool IsOpen() = 0;

			virtual ~IFile() {}
	};
}

#endif /* IFILE_HPP_ */
