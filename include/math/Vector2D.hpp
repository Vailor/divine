/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef __VECTOR2D__
#define __VECTOR2D__

#include <iostream>

namespace DivineAPI
{
	/*! Basic vector class for 2d operations. */
	template <class T>
	class Vector2D
	{
		public:
			union
			{
				struct
				{
					T x, y; //< x and y values.
				};

				T v[2];
			};

			Vector2D()
			{
				x = y = 0;
			}

			Vector2D(T x, T y) :
					x(x), y(y)
			{
			}

			template<class Type>
			Vector2D(const Vector2D<Type>& vec2d) : x(static_cast<T>(vec2d.x)), y(static_cast<T>(vec2d.y))
			{
			}

			inline Vector2D<T> &operator=(const Vector2D<T> &vec2d)
			{
				this->x = vec2d.x;
				this->y = vec2d.y;

				return *this;
			}

			inline Vector2D<T> &operator+=(const Vector2D<T> &vec2d)
			{
				this->x += vec2d.x;
				this->y += vec2d.y;

				return *this;
			}

			inline Vector2D<T> &operator+=(T scalar)
			{
				this->x += scalar;
				this->y += scalar;

				return *this;
			}

			inline Vector2D<T> &operator-=(const Vector2D<T> &vec2d)
			{
				this->x -= vec2d.x;
				this->y -= vec2d.y;

				return *this;
			}

			inline Vector2D<T> &operator-=(T scalar)
			{
				this->x -= scalar;
				this->y -= scalar;

				return *this;
			}

			inline Vector2D<T> &operator*=(const Vector2D<T> &vec2d)
			{
				this->x *= vec2d.x;
				this->y *= vec2d.y;

				return *this;
			}

			inline Vector2D<T> &operator*=(T scalar)
			{
				this->x *= scalar;
				this->y *= scalar;

				return *this;
			}

			inline Vector2D<T> &operator/=(const Vector2D<T> &vec2d)
			{
				this->x /= vec2d.x;
				this->y /= vec2d.y;

				return *this;
			}

			inline Vector2D<T> &operator/=(T scalar)
			{
				this->x /= scalar;
				this->y /= scalar;

				return *this;
			}

			inline Vector2D<T> operator/(const Vector2D &vec2d) const
			{
				return Vector2D<T>(this->x / vec2d.x, this->y / vec2d.y);
			}

			inline Vector2D<T> operator/(T scalar) const
			{
				return Vector2D<T>(this->x / scalar, this->y / scalar);
			}

			inline Vector2D<T> operator+(const Vector2D<T> &vec2d) const
			{
				return Vector2D<T>(this->x + vec2d.x, this->y + vec2d.y);
			}

			inline Vector2D<T> operator+(T scalar) const
			{
				return Vector2D<T>(this->x + scalar, this->y + scalar);
			}

			inline Vector2D<T> operator-(const Vector2D<T> &vec2d) const
			{
				return Vector2D<T>(this->x - vec2d.x, this->y - vec2d.y);
			}

			inline Vector2D<T> operator-(T scalar) const
			{
				return Vector2D<T>(this->x - scalar, this->y - scalar);
			}

			inline Vector2D<T> operator*(const Vector2D<T> &vec2d) const
			{
				return Vector2D<T>(this->x * vec2d.x, this->y * vec2d.y);
			}

			inline Vector2D<T> operator*(T scalar) const
			{
				return Vector2D<T>(this->x * scalar, this->y * scalar);
			}

			inline bool operator>(const Vector2D<T> &vec) const
			{
				return ((this->x > vec.x) && (this->y > vec.y));
			}

			inline bool operator>=(const Vector2D<T> &vec) const
			{
				return ((this->x >= vec.x) && (this->y >= vec.y));
			}

			inline bool operator<(const Vector2D<T> &vec) const
			{
				return ((this->x < vec.x) && (this->y < vec.y));
			}

			inline bool operator<=(const Vector2D<T> &vec) const
			{
				return ((this->x <= vec.x) && (this->y <= vec.y));
			}

			inline bool operator!= (const Vector2D<T> &vec) const
			{
				return ((this->x != vec.x) && (this->y != vec.y));
			}

			inline bool operator== (const Vector2D<T> &vec) const
			{
				return ((this->x == vec.x) && (this->y == vec.y));
			}

			friend std::ostream &operator<<(std::ostream &out, const Vector2D<T> &vec)
			{
				out << "X: " << vec.x << " Y: " << vec.y;
				return out;
			}
	};

	template<class T>
	inline T Cross(const Vector2D<T> &a, const Vector2D<T> &b)
	{
		return (a.x * b.y - a.y * b.y);
	}

	typedef Vector2D<float> Vec2Df;
	typedef Vector2D<int> Vec2Di;
	typedef Vector2D<unsigned int> Vec2Dui;
}  // namespace DivineAPI

#endif // __VECTOR2D__
