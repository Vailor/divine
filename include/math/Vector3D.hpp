/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef __VECTOR3D__
#define __VECTOR3D__

#include <math.h>
#include <iostream>
#include <math/Vector2D.hpp>
namespace DivineAPI
{
	/*! Basic vector class for 3d operations. */
	template <class T>
	class Vector3D
	{
		public:
			union
			{
				struct
				{
					T x, y, z; //< x, y, z values.
				};

				T v[3];
			};

			Vector3D()
			{
				x = y = z = 0;
			}

			Vector3D(T x, T y, T z) :
					x(x), y(y), z(z)
			{
			}

			template<class Type>
			Vector3D(const Vector3D<Type>& vec3d) : x(static_cast<T>(vec3d.x)), y(static_cast<T>(vec3d.y)), z(static_cast<T>(vec3d.z))
			{
			}

			template<class Type>
			explicit Vector3D(const Vector2D<Type>& vec2d) :
					x(static_cast<T>(vec2d.x)), y(static_cast<T>(vec2d.y)), z(0)
			{
			}

			inline Vector3D<T> &operator=(const Vector3D<T> &vec3d)
			{
				this->x = vec3d.x;
				this->y = vec3d.y;
				this->z = vec3d.z;

				return *this;
			}

			inline Vector3D<T> &operator+=(const Vector3D<T> &vec3d)
			{
				this->x += vec3d.x;
				this->y += vec3d.y;
				this->z += vec3d.z;

				return *this;
			}

			inline Vector3D<T> &operator+=(T scalar)
			{
				this->x += scalar;
				this->y += scalar;
				this->z += scalar;

				return *this;
			}

			inline Vector3D<T> &operator-=(const Vector3D<T> &vec3d)
			{
				this->x -= vec3d.x;
				this->y -= vec3d.y;
				this->z -= vec3d.z;

				return *this;
			}

			inline Vector3D<T> &operator-=(T scalar)
			{
				this->x -= scalar;
				this->y -= scalar;
				this->z -= scalar;

				return *this;
			}

			inline Vector3D<T> &operator*=(const Vector3D<T> &vec3d)
			{
				this->x *= vec3d.x;
				this->y *= vec3d.y;
				this->z *= vec3d.z;

				return *this;
			}

			inline Vector3D<T> &operator*=(T scalar)
			{
				this->x *= scalar;
				this->y *= scalar;
				this->z *= scalar;

				return *this;
			}

			inline Vector3D<T> &operator/=(const Vector3D<T> &vec3d)
			{
				this->x /= vec3d.x;
				this->y /= vec3d.y;
				this->z /= vec3d.z;

				return *this;
			}

			inline Vector3D<T> &operator/=(T scalar)
			{
				this->x /= scalar;
				this->y /= scalar;
				this->z /= scalar;

				return *this;
			}

			inline Vector3D<T> operator+(Vector3D<T> vec3d)
			{
				return Vector3D<T>(this->x + vec3d.x, this->y + vec3d.y, this->z + vec3d.z);
			}

			inline Vector3D<T> operator+(T scalar)
			{
				return Vector3D<T>(this->x + scalar, this->y + scalar, this->z + scalar);
			}

			inline Vector3D<T> operator-(Vector3D<T> vec3d)
			{
				return Vector3D<T>(this->x - vec3d.x, this->y - vec3d.y, this->z - vec3d.z);
			}

			inline Vector3D<T> operator-(T scalar)
			{
				return Vector3D<T>(this->x - scalar, this->y - scalar, this->z - scalar);
			}

			inline Vector3D<T> operator*(Vector3D vec3d)
			{
				return Vector3D<T>(this->x * vec3d.x, this->y * vec3d.y, this->z * vec3d.z);
			}

			inline Vector3D<T> operator*(float scalar)
			{
				return Vector3D<T>(this->x * scalar, this->y * scalar, this->z * scalar);
			}

			inline Vector3D<T> operator/(Vector3D vec3d)
			{
				return Vector3D<T>(this->x / vec3d.x, this->y / vec3d.y, this->z / vec3d.z);
			}

			inline Vector3D<T> operator/(float scalar)
			{
				return Vector3D<T>(this->x / scalar, this->y / scalar, this->z / scalar);
			}

			friend std::ostream &operator<<(std::ostream &out, const Vector3D<T> &vec)
			{
				out << "X: " << vec.x << " Y: " << vec.y << " Z: " << vec.z;
				return out;
			}
	};

	template <class T>
	inline Vector3D<T> Normalize(const Vector3D<T> &vec)
	{
		float v = sqrt((vec.x * vec.x) + (vec.y * vec.y) + (vec.z * vec.z));
		return Vector3D<T>(vec.x / v, vec.y / v, vec.z / v);
	}

	template<class T>
	inline Vector3D<T> Cross(const Vector3D<T> &a, const Vector3D<T> &b)
	{
		return Vector3D<T>(a.y * b.z - a.z * b.y, 
						   a.z * b.x - a.x * b.z, 
						   a.x * b.y - a.y * b.x);
	}
	typedef Vector3D<float> Vec3Df;
	typedef Vector3D<int> Vec3Di;
	typedef Vector3D<unsigned int> Vec3Dui;
}  // namespace DivineAPI

#endif // __VECTOR3D__
