/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef __MATRIX__
#define __MATRIX__

#include <math.h>
#include <math/Vector2D.hpp>
#include <math/Vector3D.hpp>

#define PI 3.14159265

namespace DivineAPI
{
	class Matrix;

	inline Matrix Identity();

	/*! A 4x4 matrix class. */
	class Matrix
	{
		public:
			union
			{
				struct
				{
					float m1, m2, m3, m4,
						  m5, m6, m7, m8,
						  m9, m10, m11, m12,
						  m13, m14,	m15, m16;
				};

				float m[16];
			};

			Matrix()
			{
				*this = Identity();
			}

			Matrix(float m1, float m2, float m3, float m4, float m5, float m6, float m7,
					float m8, float m9, float m10, float m11, float m12, float m13,
					float m14, float m15, float m16) :
					m1(m1), m2(m2), m3(m3), m4(m4), m5(m5), m6(m6), m7(m7), m8(m8), m9(
							m9), m10(m10), m11(m11), m12(m12), m13(m13), m14(m14), m15(
							m15), m16(m16)
			{
			}

			Matrix(const Matrix &m)
			{
				Copy(m.m);
			}

			explicit Matrix(float m[16])
			{
				Copy(m);
			}

			void Transpose()
			{
				float NewM[16];
				NewM[0] = m1;
				NewM[1] = m5;
				NewM[2] = m9;
				NewM[3] = m13;

				NewM[4] = m2;
				NewM[5] = m6;
				NewM[6] = m10;
				NewM[7] = m14;

				NewM[8] = m3;
				NewM[9] = m7;
				NewM[10] = m11;
				NewM[11] = m15;

				NewM[12] = m4;
				NewM[13] = m8;
				NewM[14] = m12;
				NewM[15] = m16;

				Copy(NewM);
			}

			inline Matrix& operator= (const Matrix &m)
			{
				Copy(m.m);

				return *this;
			}

			inline Matrix operator*(const Matrix &m)
			{
				return Matrix(m.m1 * this->m1 + m.m5 * this->m2 + m.m9 * this->m3 + m.m13 * this->m4,
							  m.m2 * this->m1 + m.m6 * this->m2 + m.m10 * this->m3 + m.m14 * this->m4,
							  m.m3 * this->m1 + m.m7 * this->m2 + m.m11 * this->m3 + m.m15 * this->m4,
							  m.m4 * this->m1 + m.m8 * this->m2 + m.m12 * this->m3 + m.m16 * this->m4,

							  m.m1 * this->m5 + m.m5 * this->m6 + m.m9 * this->m7 + m.m13 * this->m8,
							  m.m2 * this->m5 + m.m6 * this->m6 + m.m10 * this->m7 + m.m14 * this->m8,
							  m.m3 * this->m5 + m.m7 * this->m6 + m.m11 * this->m7 + m.m15 * this->m8,
							  m.m4 * this->m5 + m.m8 * this->m6 + m.m12 * this->m7 + m.m16 * this->m8,

							  m.m1 * this->m9 + m.m5 * this->m10 + m.m9 * this->m11 + m.m13 * this->m12,
							  m.m2 * this->m9 + m.m6 * this->m10 + m.m10 * this->m11 + m.m14 * this->m12,
							  m.m3 * this->m9 + m.m7 * this->m10 + m.m11 * this->m11 + m.m15 * this->m12,
							  m.m4 * this->m9 + m.m8 * this->m10 + m.m12 * this->m11 + m.m16 * this->m12,

							  m.m1 * this->m13 + m.m5 * this->m14 + m.m9 * this->m15 + m.m13 * this->m16,
							  m.m2 * this->m13 + m.m6 * this->m14 + m.m10 * this->m15 + m.m14 * this->m16,
							  m.m3 * this->m13 + m.m7 * this->m14 + m.m11 * this->m15 + m.m15 * this->m16,
							  m.m4 * this->m13 + m.m8 * this->m14 + m.m12 * this->m15 + m.m16 * this->m16);
			}

			inline Vec3Df operator*(Vec3Df vec3d)
			{
				return Vec3Df(this->m1 * vec3d.x + this->m2 * vec3d.y + this->m3 * vec3d.z + this->m4,
						this->m5 * vec3d.x + this->m6 * vec3d.y + this->m7 * vec3d.z + this->m8,
						this->m9 * vec3d.x + this->m10 * vec3d.y + this->m11 * vec3d.z + this->m12);
			}

			inline Vec2Df operator*(Vec2Df vec2d)
			{
				return Vec2Df(this->m1 * vec2d.x + this->m2 * vec2d.y + this->m3 + this->m4,
						this->m5 * vec2d.x + this->m6 * vec2d.y + this->m7 + this->m8);
			}
		private:
			void Copy(const float matrix[16])
			{
				for (int i = 0; i < 16; ++i)
				{
					this->m[i] = matrix[i];
				}
			}
	};

	inline Matrix Identity()
	{
		return Matrix(1, 0, 0, 0,
					  0, 1, 0, 0,
					  0, 0, 1, 0,
					  0, 0, 0, 1);
	}

	inline Matrix Ortho(float left, float right, float bottom, float top, float nearVal, float farVal)
	{
		float tx = -((right + left) / (right - left));
		float ty = -((top + bottom) / (top - bottom));
		float tz = -((farVal + nearVal) / (farVal - nearVal));

		float rl = 2 / (right - left);
		float tb = 2 / (top - bottom);
		float fn = 2 / (farVal - nearVal);

		return Matrix(rl, 0, 0, tx,
					  0, tb, 0, ty,
					  0, 0, fn, tz,
					  0, 0, 0, 1);
	}

	template <class T>
	inline Matrix Translate(T vec3d)
	{
		return Matrix(1, 0, 0, vec3d.x,
					  0, 1, 0, vec3d.y,
					  0, 0, 1, vec3d.z,
					  0, 0, 0, 1);
	}

	template <class T>
	inline Matrix LookAt(T eye, T center, T up)
	{
		T f = Normalize(center - eye);
		T UP = Normalize(up);

		T s = Cross(f, UP);
		T u = Cross(Normalize(s), f);

		return Translate(Vec3Df(-eye.x, -eye.y, -eye.z)) * Matrix(s.x, s.y, s.z, 0,
					  u.x, u.y, u.z, 0,
					  -f.x, -f.y, -f.z, 0,
					  0, 0, 0, 1);
	}

	template <class T>
	inline Matrix Rotate(float angle, const T &vec3d)
	{
		float c = cos(angle);
		float s = sin(angle);
		float xy = vec3d.x * vec3d.y;
		float xz = vec3d.x * vec3d.z;
		float yx = vec3d.y * vec3d.x;
		float yz = vec3d.y * vec3d.z;
		float xs = vec3d.x * s;
		float ys = vec3d.y * s;
		float zs = vec3d.z * s;

		return Matrix(pow(vec3d.x, 2) * (1 - c) + c, xy * (1 - c) - zs, xz * (1 - c) + ys, 0,
					  yx * (1 - c) + zs, pow(vec3d.y, 2) * (1 - c) + c, yz * (1 - c) - xs, 0,
					  xz * (1 - c) - ys, yz * (1 - c) + xs, pow(vec3d.z, 2) * (1 - c) + c, 0,
					  0, 0, 0, 1);
	}

	inline Matrix ImgRotate(float angle)
	{
		float c = cos(angle);
		float s = sin(angle);

		return Matrix(c, -s, 0, 0,
					  s, c, 0, 0,
					  0, 0, 1, 0,
					  0, 0, 0, 1);
	}

	inline Matrix Scale(float x, float y, float z)
	{
		return Matrix(x, 0, 0, 0,
					  0, y, 0, 0,
					  0, 0, z, 0,
					  0, 0, 0, 1);
	}

	template <class T>
	inline Matrix Scale(T vec3d)
	{
		return Matrix(vec3d.x, 0, 0, 0,
					  0, vec3d.y, 0, 0,
					  0, 0, vec3d.z, 0,
					  0, 0, 0, 1);
	}
}  // namespace DivineAPI

#endif // __MATRIX__
