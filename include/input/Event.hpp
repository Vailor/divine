/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef EVENT_HPP_
#define EVENT_HPP_

#include <platform.h>
#include <stdint.h>
#include <SDL2/SDL.h>

namespace DivineAPI
{
	class DIVINE_API CEvent
	{
		public:
			CEvent() : m_Skip(false), m_Timestamp(0), m_Type(-1) {}

			/*!
			 * @brief Skips a event.
			 */
			void Skip()
			{
				m_Skip = true;
			}

			/*!
			 * @brief Checks if the event is skipped.
			 */
			bool IsSkipped() const
			{
				return m_Skip;
			}

			/*!
			 * @brief Sets the timestamp.
			 */
			void SetTimestamp(uint32_t Stamp)
			{
				m_Timestamp = Stamp;
			}

			/*!
			 * @return Gets the timestamp.
			 */
			uint32_t GetTimestamp() const
			{
				return m_Timestamp;
			}

			/*!
			 * @brief Sets the eventtype. The type id is the same as of libsdl.
			 */
			void SetType(int Type)
			{
				m_Type = Type;
			}

			/*!
			 * @return Gets the eventtype.
			 */
			int GetType() const
			{
				return m_Type;
			}

			virtual ~CEvent() {}
		private:
			bool m_Skip;
			uint32_t m_Timestamp;
			int m_Type;
	};
} /* namespace DivineAPI */

#endif /* EVENT_HPP_ */
