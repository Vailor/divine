/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef INPUTMANAGER_HPP_
#define INPUTMANAGER_HPP_

#include <utils/Singleton.hpp>
#include <math/Vector2D.hpp>
#include <input/IInputHandler.hpp>
#include <SDL2/SDL_keyboard.h>
#include <SDL2/SDL_mouse.h>

namespace DivineAPI
{
    class Divine;

    #define InputManager CInputManager::Get()

    /*!
     * @brief The input manager manages all input events.
     * 
     * @attention Never create an instance of this class use instead the singleton macro InputManager.
     */
    class CInputManager : Singleton<CInputManager>
    {
        friend Divine;

        public:
            CInputManager();

            /*!
             * @brief Checks if the given key is pressed.
             * @return Returns true if the given key is pressed.
             */
            bool IsKeyPressed(int Key);

            /*!
             * @brief Checks if the given button is pressed.
             * @return Returns true if the given button is pressed.
             */
            bool IsButtonPressed(int Button);

            /*!
             * @return Gets the current mouse position.
             */
            Vec2Di GetMousePos()
            {
                return m_MousePos;
            }

            /*!
             * @brief Registers a new input callback.
             * 
             * @attention The callback will be released by the input manager.
             */
            void RegisterInputHandler(IInputHandler *Handler);

            virtual ~CInputManager();

        private:
            /*!
             * @brief Processes all messages.
             */
            void ProcessMessages();

            const Uint8 *m_Keymap;
            Uint8 m_ButtonMap[5];
            Vec2Di m_MousePos;

            IInputHandler *m_Handler;
    };
}

#endif