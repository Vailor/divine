/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef KEYBOARDEVENT_HPP_
#define KEYBOARDEVENT_HPP_

#include <platform.h>
#include <input/Event.hpp>
#include <string>
#include <SDL2/SDL_keyboard.h>

namespace DivineAPI
{
	class DIVINE_API CKeyboardEvent : public CEvent
	{
		public:
			CKeyboardEvent() : CEvent()
			{
				m_State = -1;
				m_Scancode = SDL_SCANCODE_UNKNOWN;
				m_Keycode = SDLK_UNKNOWN;
				m_Mod = KMOD_NONE;
			}

			/*!
			 * @brief Sets the state of the current key.
			 */
			void SetState(Uint8 State)
			{
				m_State = State;
			}

			/*!
			 * @return Gets the state of the current key.
			 */
			Uint8 GetState() const
			{
				return m_State;
			}

			/*!
			 * @brief Sets the scancode.
			 */
			void SetScancode(SDL_Scancode Scancode)
			{
				m_Scancode = Scancode;
			}

			/*!
			 * @return Gets the scancode.
			 */
			SDL_Scancode GetScancode() const
			{
				return m_Scancode;
			}

			/*!
			 * @brief Sets the virtual key.
			 */
			void SetKeycode(SDL_Keycode Keycode)
			{
				m_Keycode = Keycode;
			}

			/*!
			 * @return Gets the virtual key.
			 */
			SDL_Keycode GetKeycode() const
			{
				return m_Keycode;
			}

			/*!
			 * @brief Sets the modifier mask.
			 */
			void SetMod(Uint16 Mod)
			{
				m_Mod = Mod;
			}

			/*!
			 * @return Gets the modifier mask.
			 */
			Uint16 GetMod() const
			{
				return m_Mod;
			}

			/*!
			 * @brief Sets the input text.
			 */
			void SetText(const std::string &Text)
			{
				m_Text = Text;
			}

			/*!
			 * @return Gets the input text.
			 */
			std::string GetText() const
			{
				return m_Text;
			}

			virtual ~CKeyboardEvent() {}
		private:
			Uint8 m_State;			//State of the button, pressed or released.
			SDL_Scancode m_Scancode;
			SDL_Keycode m_Keycode;
			Uint16 m_Mod;		//Modifier key.
			std::string m_Text;
	};

}  // namespace DivineAPI

#endif /* KEYBOARDEVENT_HPP_ */
