/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef MOUSEEVENT_HPP_
#define MOUSEEVENT_HPP_

#include <platform.h>
#include <input/Event.hpp>
#include <math/Vector2D.hpp>

namespace DivineAPI
{
	class DIVINE_API CMouseEvent : public CEvent
	{
		public:
			CMouseEvent() : CEvent()
			{
				m_Which = 0;
				m_Button = -1;
				m_State = -1;
				m_Clicks = -1;
			}

			/*!
			 * @brief Sets the mouse instance id. For more informations, see https://wiki.libsdl.org/SDL_MouseWheelEvent.
			 */
			void SetWhich(Uint32 Which)
			{
				m_Which = Which;
			}

			/*!
			 * @return Gets the mouse instance id. For more informations, see https://wiki.libsdl.org/SDL_MouseWheelEvent.
			 */
			Uint32 GetWhich() const
			{
				return m_Which;
			}

			/*!
			 * @brief Sets the mouse position.
			 */
			void SetMousePos(Vec2Di Pos)
			{
				m_Position = Pos;
			}

			/*!
			 * @return Gets the mouse position.
			 */
			Vec2Di GetMousePos()
			{
				return m_Position;
			}

			/*!
			 * @brief Sets the button id which has triggered an event. The button has the same id as libsdl.
			 */
			void SetButton(Uint8 Button)
			{
				m_Button = Button;
			}

			/*!
			 * @return Gets the button id which has triggered an event. The button has the same id as libsdl.
			 */
			Uint8 GetButton() const
			{
				return m_Button;
			}

			/*!
			 * @brief Sets the button state. The state has the same id as libsdl.
			 */
			void SetButtonState(Uint8 State)
			{
				m_State = State;
			}

			/*!
			 * @return Gets the button state. The state has the same id as libsdl.
			 */
			Uint8 GetButtonState() const
			{
				return m_State;
			}

			/*!
			 * @brief Sets the click count, either 1 or 2.
			 */
			void SetClickCount(Uint8 Clicks)
			{
				m_Clicks = Clicks;
			}

			/*!
			 * @return Gets the click count, either 1 or 2.
			 */
			Uint8 GetClickCount() const
			{
				return m_Clicks;
			}

			/*!
			 * @brief Sets the position relative to the old position.
			 */
			void SetRelativePos(Vec2Di Pos)
			{
				m_RelPosition = Pos;
			}

			/*!
			 * @return Gets the position relative to the old position.
			 */
			Vec2Di GetRelativePos()
			{
				return m_RelPosition;
			}

			/*!
			 * @brief Sets the scroll value. For more informations, see https://wiki.libsdl.org/SDL_MouseWheelEvent.
			 */
			void SetScrollValue(Vec2Di Value)
			{
				m_ScrollValue = Value;
			}

			/*!
			 * @brief Gets the scroll value. For more informations, see https://wiki.libsdl.org/SDL_MouseWheelEvent.
			 */
			Vec2Di GetScrollValue()
			{
				return m_ScrollValue;
			}

			virtual ~CMouseEvent() {}
		private:
			Vec2Di m_Position;
			Vec2Di m_RelPosition;
			Vec2Di m_ScrollValue;
			Uint32 m_Which; //The mouse instance id. For more informations, see https://wiki.libsdl.org/SDL_MouseWheelEvent.
			Uint8 m_Button;
			Uint8 m_State;
			Uint8 m_Clicks;
	};
}  // namespace DivineAPI

#endif /* MOUSEEVENT_HPP_ */
