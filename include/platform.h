/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef PLATFORM_H_
#define PLATFORM_H_

#if defined(_WIN32) || defined(_WIN64)
    #ifdef _MSC_VER 
        #define NOMINMAX
    #endif
    
    #include <windows.h>
    #undef ERROR

    #ifdef BUILD_DLL
        #define DIVINE_API __declspec(dllexport)
    #else
        #define DIVINE_API __declspec(dllimport)
    #endif

    #define DIVINE_WINDOWS

    #define PATH_DELIMITER '\\'
#elif defined(__linux)
    #if __GNUC__ >= 4
        #define DIVINE_API __attribute__((visibility ("default")))
    #else
        #define DIVINE_API
    #endif

    #define DIVINE_UNIX
    
    #define PATH_DELIMITER '/'

    #if defined(ANDROID) || defined(__ANDROID__)
        #define DIVINE_ANDROID
    #endif 
#endif

#endif /* PLATFORM_H_ */
