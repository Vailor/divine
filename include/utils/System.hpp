/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef SYSTEM_HPP_
#define SYSTEM_HPP_

#include <platform.h>
#include <string>
#include <math/Vector2D.hpp>
#include <vector>

namespace DivineAPI
{
	/*!
	 * @brief Static class for accessing system informations.
	 */
	class DIVINE_API System
	{
		public:
			/*! Contains informations about the systems memory. */
			struct Memory
			{
				uint64_t TotalMemory;	//!< Total ram size in bytes.
				uint64_t FreeMemory;	//!< Free ram size in bytes.
			};

			struct DiskInfo
			{
				std::string Path;				//!< Path to the disk.
				unsigned long long TotalSpace;
				unsigned long long FreeSpace;
				unsigned long long Available;	//!< Available space for the current user.
			};

			/*! Contains informations about a monitor. */
			struct MonitorInfo
			{
				int Index;			//!< Monitor index is used for internal operations.
				std::string Name;	//!< Monitor name.
				Vec2Di Resolution;	//!< Width and height in pixel of the monitor.
				Vec2Di LeftTop;		//!< Left top position of the monitor in pixels.
				Vec2Df DPI;			//!< The dpi of the monitor.
				unsigned int Depth;	//!< Colordepth of the monitor.
				int RefreshRate;	//!< Monitor refresh rate.
			};

			/*!
			 * @return Gets informations about the free and total ram of the system.
			 * 
			 * @throw std::runtime_error Throws on error.
			 */
			static Memory GetSystemMemoryInfo();

			/*!
			 * @brief Gets informations about all mounted disks.
			 * 
			 * @return Returns a list of all mounted disks.
			 * 
			 * @throw std::runtime_error Throws on error.
			 */
			static std::vector<DiskInfo> GetDisksInfos();

			/*!
			 * @return Gets a list of all monitors which are connected to this system.
			 * 
			 * @throw std::runtime_error Throws on error.
			 */
			static std::vector<MonitorInfo> GetMonitors();

		private:
		#ifdef DIVINE_UNIX
			static size_t FindNumeric(std::string Str);
		#endif

			System() {}
			~System() {}
	};

}  // namespace DivineAPI

#endif /* SYSTEM_HPP_ */
