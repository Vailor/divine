/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef LOG_HPP_
#define LOG_HPP_

#include <platform.h>
#include <utils/Singleton.hpp>
#include <string>
#include <algorithm>
#include <filesystem/IFile.hpp>
#include <time.h>
#include <stdarg.h>
#include <iomanip>

#ifdef DIVINE_ANDROID
	#include <android/log.h>
#else
	#include <iostream>
#endif

namespace DivineAPI
{
	#define LOG CLog::Get()
	#define LOGE(msg, ...) CLog::Get()->Print(CLog::LOGTYPE::ERROR, msg, ##__VA_ARGS__)
	#define LOGW(msg, ...) CLog::Get()->Print(CLog::LOGTYPE::WARNING, msg, ##__VA_ARGS__)
	#define LOGI(msg, ...) CLog::Get()->Print(CLog::LOGTYPE::INFO, msg, ##__VA_ARGS__)

	/*!
	 * @brief The log class allows you to log runtime informations. You can either log in the console window or as textfile.
	 */
	class DIVINE_API CLog : public Singleton<CLog>
	{
		public:
			CLog() : m_Logfile(nullptr) {}

			enum class LOGTYPE
			{
				INFO,
				WARNING,
				ERROR
			};

			/*!
			 * @brief Sets the path where to generate the logfile.
			 */
			void SetLogFilePath(const std::string &Path)
			{
				if(m_Logfile)
				{
					m_Logfile->Close();
					m_Logfile->Open(Path, IFile::Openmode::RW);
				}
				else
					m_Logfile = IFile::Create(Path, IFile::Openmode::RW);
			}

			/*!
			 * @brief Writes data to the log.
			 * 
			 * @param type: The type of log.
			 * @param Format: Formated log message.
			 */
			void Print(LOGTYPE type, const std::string Format, ...)
			{
				std::string Msg;

				va_list FormatList;
				va_start(FormatList, Format);

				va_list Copy;
				va_copy(Copy, FormatList);
				size_t Size = vsnprintf(nullptr, 0, Format.c_str(), Copy);
				va_end(Copy);
				
				Msg.resize(Size + 1);
				vsnprintf(&Msg[0], Msg.size(), Format.c_str(), FormatList);

				va_end(FormatList);

				time_t t = time(nullptr);
				tm *Time = localtime(&t);		
				char TimeStr[80];

				strftime(TimeStr, 80, "%H:%M:%S %d.%m.%Y", Time);

				switch(type)
				{
					case LOGTYPE::INFO:
					{
						Msg = "[" + std::string(TimeStr) + "] Info: " + Msg;
					}break;

					case LOGTYPE::WARNING:
					{
						Msg = "[" + std::string(TimeStr) + "] Warning: " + Msg;
					}break;

					case LOGTYPE::ERROR:
					{
						Msg = "[" + std::string(TimeStr) + "] Error: " + Msg;
					}break;
				}

				Msg.erase(Msg.size() - 1);
				Msg += '\n';

				if(m_Logfile)
					m_Logfile->Write(Msg.c_str(), Msg.size());

#ifndef DIVINE_ANDROID
				if(type != LOGTYPE::ERROR)
					std::cout << Msg;
				else
					std::cerr << Msg;
#else
				int Priority = 0;
				switch(type)
				{
					case LOGTYPE::INFO:
					{
						Priority = ANDROID_LOG_INFO;
					}break;

					case LOGTYPE::WARNING:
					{
						Priority = ANDROID_LOG_WARN;
					}break;

					case LOGTYPE::ERROR:
					{
						Priority = ANDROID_LOG_ERROR;
					}break;
				}

				__android_log_print(Priority, "Divine Engine", "%s", Msg.c_str());
#endif
			}

			virtual ~CLog() 
			{
				delete m_Logfile;
			}

		private:
			IFile *m_Logfile;
	};
}

#endif /* LOG_HPP_ */
