#ifndef ZSTREAM_HPP
#define ZSTREAM_HPP

#include <string>
#include <istream>
#include <ctype.h>
#include <platform.h>
#include <vector>
#include <streambuf>
#include <filesystem/IFile.hpp>

namespace DivineAPI
{
    /*!
     * @brief Base class for zlib based compression and decompression streams.
     */
    class DIVINE_API ZStream_Base
    {
        public:
            ZStream_Base();
            ZStream_Base(size_t BufferSize);

            virtual ZStream_Base &operator<<(const std::string &Data) = 0;
            virtual ZStream_Base &operator<<(std::istream &Data) = 0;
            virtual ZStream_Base &operator<<(IFile *Data) = 0;

            virtual std::string &operator>>(std::string &Data) = 0;
            virtual std::ostream &operator>>(std::ostream &Data) = 0;
            virtual IFile *operator>>(IFile *Data) = 0;

            /*!
             * @brief Writes data to the stream.
             * 
             * @param Data: Data to write.
             * @param Size: Datasize
             */
            virtual void Write(const uint8_t *Data, size_t Size) = 0;

            /*!
             * @brief Reads data from the stream.
             * 
             * @param Buf: Buffer to store the data.
             * @param Size: Buffersize.
             */
            virtual void Read(uint8_t *Buf, size_t Size) = 0;

            /*!
             * @brief Flushes the stream.
             */
            virtual void Flush();

            size_t Size()
            {
                return m_OutData.size();
            }

            virtual ~ZStream_Base() {}

        protected:
            std::vector<uint8_t> m_OutData, m_Dictionary;
            size_t m_DictionaryPos;
            size_t m_BufferSize;
    };

    class DIVINE_API ZCompressionStream : public ZStream_Base
    {
        public:
            ZCompressionStream() : ZStream_Base(), m_Level(8) {}
            ZCompressionStream(size_t BufferSize) : ZStream_Base(BufferSize), m_Level(8){}

            /*!
             * @brief Sets the compression level.
             * 
             * @param Level: 0 non compression 9 high compression.
             */
            void SetCompressionLevel(int Level)
            {
                m_Level = Level;
            }

            ZStream_Base &operator<<(const std::string &Data);
            ZStream_Base &operator<<(std::istream &Data);
            ZStream_Base &operator<<(IFile *Data);

            std::string &operator>>(std::string &Data);
            std::ostream &operator>>(std::ostream &Data);
            IFile *operator>>(IFile *Data);

            /*!
             * @brief Writes data to the stream.
             * 
             * @param Data: Data to write.
             * @param Size: Datasize
             */
            void Write(const uint8_t *Data, size_t Size);

            /*!
             * @brief Reads data from the stream.
             * 
             * @param Buf: Buffer to store the data.
             * @param Size: Buffersize.
             */
            void Read(uint8_t *Buf, size_t Size);

        private:
            void Compress(std::vector<uint8_t> &Data);
            int m_Level;
    };

    class DIVINE_API ZDecompressionStream : public ZStream_Base
    {
        public:
            ZDecompressionStream() : ZStream_Base() {}
            ZDecompressionStream(size_t BufferSize) : ZStream_Base(BufferSize) {}

            ZStream_Base &operator<<(const std::string &Data);
            ZStream_Base &operator<<(std::istream &Data);
            ZStream_Base &operator<<(IFile *Data);

            std::string &operator>>(std::string &Data);
            std::ostream &operator>>(std::ostream &Data);
            IFile *operator>>(IFile *Data);

            /*!
             * @brief Writes data to the stream.
             * 
             * @param Data: Data to write.
             * @param Size: Datasize
             */
            void Write(const uint8_t *Data, size_t Size);

            /*!
             * @brief Reads data from the stream.
             * 
             * @param Buf: Buffer to store the data.
             * @param Size: Buffersize.
             */
            void Read(uint8_t *Buf, size_t Size);

        private:
            void Decompress(std::vector<uint8_t> &Data);
    };
}

#endif