/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef SINGLETON_HPP_
#define SINGLETON_HPP_

namespace DivineAPI
{
	template <class T>
	class Singleton
	{
		public:
			static T *Get()
			{
				if(!m_Instance)
					m_Instance = new T();

				return m_Instance;
			}

			static void Del()
			{
				if(m_Instance)
				{
					delete m_Instance;
					m_Instance = nullptr;
				}
			}

		protected:
			static T *m_Instance;
	};

	template <class T>
	T *Singleton<T>::m_Instance = nullptr;
}

#endif /* SINGLETON_HPP_ */
