/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef COLOR_HPP_
#define COLOR_HPP_

#include <platform.h>
namespace DivineAPI
{
	struct DIVINE_API Color
	{
		Color() : R(0), G(0), B(0), A(255) {}
		Color(const unsigned char &R_, const unsigned char &G_, const unsigned char &B_, const unsigned char &A_ = 255)
		{
			A = A_;
			R = R_;
			G = G_;
			B = B_;
		}

		explicit Color(const unsigned int &color)
		{
			unsigned char *RGBA = (unsigned char*)&color;

			A = RGBA[0];
			R = RGBA[1];
			G = RGBA[2];
			B = RGBA[3];
		}

		Color(const Color &color)
		{
			A = color.A;
			R = color.R;
			G = color.G;
			B = color.B;
		}

		Color &operator= (const Color &color)
		{
			A = color.A;
			R = color.R;
			G = color.G;
			B = color.B;

			return *this;
		}

		Color &operator+= (const Color &color)
		{
			A += color.A;
			R += color.R;
			G += color.G;
			B += color.B;

			return *this;
		}

		Color &operator-= (const Color &color)
		{
			A -= color.A;
			R -= color.R;
			G -= color.G;
			B -= color.B;

			return *this;
		}

		Color &operator/= (const Color &color)
		{
			A /= color.A;
			R /= color.R;
			G /= color.G;
			B /= color.B;

			return *this;
		}

		Color &operator*= (const Color &color)
		{
			A *= color.A;
			R *= color.R;
			G *= color.G;
			B *= color.B;

			return *this;
		}

		Color &operator= (const unsigned int &color)
		{
			unsigned char *RGBA = (unsigned char*)&color;

			A = RGBA[0];
			R = RGBA[3];
			G = RGBA[2];
			B = RGBA[1];

			return *this;
		}

		Color &operator+= (const unsigned int &color)
		{
			unsigned char *RGBA = (unsigned char*)&color;

			A += RGBA[0];
			R += RGBA[3];
			G += RGBA[2];
			B += RGBA[1];

			return *this;
		}

		Color &operator-= (const unsigned int &color)
		{
			unsigned char *RGBA = (unsigned char*)&color;

			A -= RGBA[0];
			R -= RGBA[3];
			G -= RGBA[2];
			B -= RGBA[1];

			return *this;
		}

		Color &operator*= (const unsigned int &color)
		{
			unsigned char *RGBA = (unsigned char*)&color;

			A *= RGBA[0];
			R *= RGBA[3];
			G *= RGBA[2];
			B *= RGBA[1];

			return *this;
		}

		Color &operator/= (const unsigned int &color)
		{
			unsigned char *RGBA = (unsigned char*)&color;

			A /= RGBA[0];
			R /= RGBA[3];
			G /= RGBA[2];
			B /= RGBA[1];

			return *this;
		}

		union
		{
			struct
			{
				unsigned char R, G, B, A;
			};

			char c[4];
		};
	};
}

#endif /* COLOR_HPP_ */
