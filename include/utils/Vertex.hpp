#ifndef VERTEX_HPP
#define VERTEX_HPP

#include <math/Vector2D.hpp>
#include <math/Vector3D.hpp>
#include <utils/Color.hpp>

namespace DivineAPI
{
    enum VertexAttributes
    {
        VERTEX_POSITION = 1,
        VERTEX_UV = 2,
        VERTEX_COLOR = 4,

        VERTEX_POS_UV = (VERTEX_POSITION | VERTEX_UV),
        VERTEX_POS_COLOR = (VERTEX_POSITION | VERTEX_COLOR),
        VERTEX_POS_UV_COLOR = (VERTEX_POS_UV | VERTEX_COLOR)
    };

    struct Vertex
    {
        Vec3Df Pos;
        Vec2Df UV;
        Color Col;
    };
}

#endif