/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef ITEXTURE_HPP
#define ITEXTURE_HPP

#include <math/Vector2D.hpp>
#include <stdint.h>
#include <platform.h>
#include <image/Image.hpp>
#include <resource/IResourcemanager.hpp>

namespace DivineAPI
{
    /*!
     * @brief Interface for managing textures on the gpu side.
     */
    class DIVINE_API ITexture : public CBaseResource
    {
        public:
            ITexture(IResourcemanager *mgr) : CBaseResource(mgr) {}

            /*!
             * @brief Creates an empty texture.
             * 
             * @param Size: Size of the texture.
             * @param Format: Texture format. See also CImage::Pixelformat.
             */
            virtual void CreateEmptyTexture(Vec2Di Size, CImage::Pixelformat Format) = 0;

            /*!
             * @brief Copies a subtexture to a specific position.
             * 
             * @param img: Image to copy.
             * @param Pos: Position to insert.
             */
            virtual void CopySubTexture(const CImage &img, Vec2Di Pos) = 0;

            /*!
             * @brief Resizes the texture without scale the current data.
             */
            virtual void ResizeTexture(Vec2Di Size) = 0;

            /*!
             * @return Gets the image which is stored in this texture.
             */
            virtual CImage GetImage() = 0; 

            /*!
             * @brief Binds this texture.
             */
            virtual void BindTexture() = 0;

            /*!
             * @brief Unbinds this texture.
             */
			virtual void UnbindTexture() = 0;

            /*!
             * @return Gets the size of the texture.
             */
			virtual Vec2Di TextureSize() const = 0;

            /*!
             * @return Gets the size in bytes which is used by this texture.
             */
            virtual uint64_t Size() const = 0;

            /*!
             * @return Gets a unique id.
             */
            virtual uint32_t GetID() const = 0;

            virtual ~ITexture() {}
    };
}

#endif