/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef IDRIVER_HPP_
#define IDRIVER_HPP_

#include <vector>
#include <platform.h>
#include <math/Vector3D.hpp>
#include <math/Vector2D.hpp>
#include <utils/Color.hpp>
#include <driver/IShaderManager.hpp>
#include <image/Image.hpp>
#include <resource/IResourcemanager.hpp>
#include <driver/ITexture.hpp>
#include <utils/Vertex.hpp>

namespace DivineAPI
{
	/*! 
	 * @brief This interface managed object buffers on gpu side.
	 */
	class DIVINE_API IObjectBuffer
	{
		public:
			/*!
			 * @brief In this enumration are various render flags for rendering.
			 */
			enum class RenderModes
			{
				TRIANGLE_STRIP,
				TRIANGLE_FAN,
				TRIANGLES
			};

			/*!
			 * @brief Sets the render mode of this buffer. See also RenderModes.
			 * 
			 * @param Mode: The render mode.
 			 */
			virtual void SetRenderMode(RenderModes Mode) = 0;

			/*!
			 * @brief Uploads or updates vertices of this buffer.
			 * 
			 * @param Vertices: Vertices to upload.
			 * @param Attribute: The vertex attribute to update or upload. See also VertexAttributes.
			 * 
			 * @remarks If an attribute is to be update, care must be taken that the update buffer has the same size as the already existing buffer.
			 * @remark An exception to this is the update of all attributes that exists in this buffer.
			 * 
			 * @throw std::runtime_error Throws only if the first remark occurs.
			 */
			virtual void UploadVertices(const std::vector<Vertex> &Vertices, VertexAttributes Attribute) = 0;

			/*!
			 * @brief Uploads or updates the indices.
			 * 
			 * @param Indices: The new indices for this buffer.
			 * @param ObjIndices: The count of vertices per object.
			 */
			virtual void UploadIndices(const std::vector<unsigned short> &Indices, const std::vector<int> &ObjIndices = std::vector<int>()) = 0;

			/*!
			 * @brief Renders the object buffer.
			 */
			virtual void Render() = 0;

			/*!
			 * @brief Deletes this buffer on the gpu side.
			 */
			virtual void ClearBuffer() = 0;

			/*!
			 * @return Gets the size in bytes of this buffer.
			 */
			virtual uint64_t Size() const = 0;

			/*!
			 * @return Gets the total count of triangles in this buffer.
			 */
			virtual uint64_t TrisCount() const = 0;

			/*!
			 * @return Gets the vertex count of this buffer.
			 */
			virtual uint64_t VertexCount() const = 0;

			/*!
			 * @brief Deletes this object. You must use this to free the object instead of the delete operator.
			 */
			virtual void Release() = 0;

			virtual ~IObjectBuffer() {}
	};

	/*!
	 * @brief This structure contains information about the memory usage of the gpu.
	 * 
	 * @remarks These values may not 100% correct, because the graphics driver manages where to store the data.
	 * @remark To obtain informations about the memory usage there are some requirements. On NVIDIA GPUs the OpenGL extension GL_NVX_gpu_memory_info must be supported.
	 * @remark On AMD or ATI GPUs the extension GL_ATI_meminfo and WGL_AMD_gpu_association (or GLX_AMD_gpu_association on linux) must be supported.
	 * @remark For Intel or graphic cards which doesn't support these extensions OpenCL must be available.
	 * @remark For Android OpenCL must be available, otherwise the TotalMemory is always the size of the whole ram.
	 * 
	 * @attention All values are in bytes.
	 */
	struct VRAMInfo
	{
		uint64_t TotalMemory; //!< Total size of the memory which is available.
		uint64_t UsedMemory; //!< The current memory usage.

		/*! 
		 * @brief This structure contains the theoretically calculated vram usage of the engine. Its not the exactly usage of the engine. 
		 */
		struct EngineVRAM
		{
			uint64_t UsedMemory;	//!< Total used memory of the engine. 
			uint64_t BufferMemory; //!< Used memory of the vertex buffers.
			uint64_t TextureMemory;//!< Used memory of the textures.
		};

		EngineVRAM EngineUsage;
	};

	/*!
	 * @brief This structure contains general informations about the gpu.
	 */
	struct GPUInfo
	{
		std::string Vendor;		//!< GPU Vendor
		std::string GPUName;	//!< GPU name
		std::string GraphicsAPIVersion;	//!< Used version of the underlying graphics api.
		std::string ShaderVersion;	//!< Used shader version.
		VRAMInfo MemInfo;	//!< GPU memory usage. See also ::VRAMInfo.
	};

	/*!
	 * @brief Default interface to communicate with the gpu.
	 */
	class DIVINE_API IDriver
	{
		public:
			/*!
			 * @brief Sets the viewport.
			 * 
			 * @param Pos: Position of the viewport.
			 * @param Size: Size of the viewport.
			 */
			virtual void SetViewport(Vec2Di Pos, Vec2Dui Size) = 0;

			/*!
			 * @brief Clears the screen.
			 * 
			 * @param color: The clearscreen color.
			 */
			virtual void Clear(Color color) = 0;

			/*!
			 * @brief Creates a new object buffer.
			 */
			virtual IObjectBuffer *CreateBuffer() = 0;

			/*!
			 * @return Gets informations about the gpu.
			 */
			virtual GPUInfo GetGPUInfo() = 0;

			/*!
			 * @return Gets informations about the gpu memory usage.
			 */
			virtual VRAMInfo GetVRAMInfo() = 0;

			/*!
			 * @brief Creates a driver for the current platform.
			 * 
			 * @return Returns a driver.
 			 * @throw std::runtime_error Throws if no driver for this platform exists.
			 */
			static IDriver *CreateDriver();

			/*!
			 * @brief Creates a new texture from an image.
			 * 
			 * @param mgr: The ressource manger which manages the new texture.
			 * @param img: The image to create the texture from.
			 * 
			 * @attention Never call this function outside of the ressourcemanager.
			 */
			virtual ITexture* CreateTexture(IResourcemanager *mgr, const CImage &img) = 0; 

			/*!
			 * @brief Creates an empty texture.
			 */
			virtual ITexture *CreateTexture() = 0; 

			virtual ~IDriver() {}
	};
}

#endif /* IDRIVER_HPP_ */
