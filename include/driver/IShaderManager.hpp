/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef ISHADERMANAGER_HPP_
#define ISHADERMANAGER_HPP_

#include <platform.h>
#include <stdint.h>
#include <string>
#include <utils/Color.hpp>
#include <math/Matrix.hpp>
#include <math/Vector2D.hpp>
#include <math/Vector3D.hpp>

namespace DivineAPI
{
    class IShaderHandler;

    /*!
     * @brief The shader manager provides an interface to communicate with all available shaders. It also manages the registration and compilation of shaders.
     */
    class DIVINE_API IShaderManager
    {
        public:
            static const int BUILTIN_SPRITE_SHADER = 0;
            static const int BUILTIN_TEXT_SHADER = 1;

            /*!
             * @brief If an shader will be load the shader manager passes to the associated shader handler these values to say which shader is needed. See also ::IShaderHandler.
             */
            enum class ShaderType
            {
                VERTEX,
                FRAGMENT
            };

            /*!
             * @brief Registers a new shader with its handler. See also ::IShaderHandler to see how to implement a shader.
             * 
             * @param Shaderhandler: The shaderhandler to register.
             * @return Returns a unique id for this shader.
             * 
             * @note The shader handler will be released from the shader manager.
             */
            virtual uint32_t RegisterShader(IShaderHandler *Shaderhandler) = 0;

            /*!
             * @brief Applies the shader which should be used to render an object.
             * 
             * @param Shader: The shader id.
             * 
             * @throw std::invalid_argument Throws if the shader id doesn't exists.
             * @throw std::runtime_error Throws if the shader couldn't compile or link.
             */
            virtual void ApplyShader(uint32_t Shader) = 0;

            /*!
             * @brief Sets the data for the given uniform id.
             */
            virtual void SetUniform(uint32_t Uniform, const Vec3Df &vec) = 0;
            virtual void SetUniform(uint32_t Uniform, const Vec2Df &vec) = 0;
            virtual void SetUniform(uint32_t Uniform, const Matrix &m) = 0;
            virtual void SetUniform(uint32_t Uniform, int value) = 0;
            virtual void SetUniform(uint32_t Uniform, const Color &color) = 0;
    };

    /*!
     * @brief This interface is used for the communication and implementation of a custom shader.
     * 
     * @remarks Since this engine uses the programmable pipeline instead of the fixed one, you can write your own shaders.
     * @remark But since the engine doesn't know the names of your variables, you can implement a shader handler which returns these names.
     * @remark So how to implement a shader handler.
     * 
     * @code{.cpp}
     * class MyShaderHandler : public IShaderHandler
     * {
     *  public:
     *      MyShaderHandler() { ... }
     * 
     *      void OnLoadShaders(std::string &Path, IShaderManager::ShaderType Type, bool &IsMemory)
     *      {     
     *          switch(Type)
     *          {
     *              case IShaderManager::ShaderType::VERTEX:
     *              {
     *                  Path = "MyVertexShader.vs";
     *              }break;
     *
     *              case IShaderManager::ShaderType::FRAGMENT:
     *              {
     *                  Path = "MyFragmentShader.fs";
     *              }break;
     *          }
     *      }
     * 
     *      void OnUniformNameRequest(uint32_t ReqUniform, std::string &Name)
     *      {
     *          switch(ReqUniform)
     *          {
     *              case UNIFORM_TRANSLATION:
     *              {
     *                  Name = "Translation";
     *              }break;
     * 
     *              ...
     *          }
     *      }
     * 
     *      void OnAttribLocationNameRequest(uint32_t ReqAttrib, std::string &Name)
     *      {
     *          switch(ReqAttrib)
     *          {
     *              case ATTRIBUTE_VERTEX_COORDS:
     *              {
     *                  Name = "Position";
     *              }break;
     * 
     *              ...
     *          }
     *      }
     * 
     *      virtual ~MyShaderHandler() { ... }
     * }
     * @endcode
     */
    class IShaderHandler
    {
        public:
            /*!
             * @brief Called if the shader is loaded the first time.
             * 
             * @param Path: The path or the shader data.
             * @param Type: Requested shader.
             * 
             * @param IsMemory: If is set to true, the path variable is interpreted as the shader data.
             */
            virtual void OnLoadShaders(std::string &Path, IShaderManager::ShaderType Type, bool &IsMemory) = 0;

            /*!
             * @brief Called if a uniform name is required.
             * 
             * @param ReqUniform: The id which identify the requested uniform.
             * @param Name: The name of the uniform.
             */
            virtual void OnUniformNameRequest(uint32_t ReqUniform, std::string &Name) = 0;

            /*!
             * @brief Called if an attribute name is required.
             * 
             * @param ReqAttrib: The id which identify the requested attribute.
             * @param Name: The name of the attribute.
             */
            virtual void OnAttribLocationNameRequest(uint32_t ReqAttrib, std::string &Name) = 0;

            virtual ~IShaderHandler() {}
    };

    /*!
     * @brief Uniforms identifier.
     * 
     * @remark You can also define uniform ids starting from UNIFORM_USER.
     */
    enum UniformsIDs
    {   
        UNIFORM_ROTATION,
        UNIFORM_TRANSLATION,
        UNIFORM_SCALE,
        UNIFORM_PROJECTION,
        UNIFORM_COLOR,
        UNIFORM_USER,

        MAX_UNIFORMS = 255
    };

    /*!
     * @brief Attributes identifier.
     * 
     * @remark You can also define attribute ids starting from ATTRIBUTE_USER.
     */
    enum AttributeIDs
    {
        ATTRIBUTE_VERTEX_COORDS,
        ATTRIBUTE_UV_COORDS,
        ATTRIBUTE_COLOR,
        ATTRIBUTE_USER,

        MAX_ATTRIBUTES = 255
    };
}

#endif