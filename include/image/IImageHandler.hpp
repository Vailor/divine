/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef IIMAGELOADER_HPP_
#define IIMAGELOADER_HPP_

#include <platform.h>
#include <math/Vector2D.hpp>
#include <string>
#include <image/Image.hpp>

namespace DivineAPI
{
	/*!
	 * @interface IImageHandler
	 * @brief Interface for image loading and saving. See also ::Create.
	 */
	class DIVINE_API IImageHandler
	{
		public:
			/*!
			 * @brief Loads an image from a file.
			 * 
			 * @param Path: The path to the file to load.
			 * @param[out] Size: The resolution of the loaded image.
			 * @param[out] Format: The pixel format of the loaded image. See also CImage::Pixelformat.
			 * 
			 * @return Returns an array of the pixel data.
			 * 
			 * @annotation An exception should be thrown, if an error occurs.
			 */
			virtual void *LoadImage(const std::string &Path, Vec2Di &Size, CImage::Pixelformat &Format) = 0;

			/*!
			 * @brief Loads an image from a memory stream.
			 * 
			 * @param Data: The memory stream which contains the image.
			 * @param Length: The length of the memory stream.
			 * @param[out] Size: The resolution of the loaded image.
			 * @param[out] Format: The pixel format of the loaded image. See also CImage::Pixelformat.
			 * 
			 * @return Returns an array of the pixel data.
			 * 
			 * @annotation An exception should be thrown, if an error occurs.
			 */
			virtual void *LoadImage(const void *Data, size_t Length, Vec2Di &Size, CImage::Pixelformat &Format) = 0;

			/*!
			 * @brief Saves an image to a file.
			 * 
			 * @param Path: The path to the file to save.
			 * @param img: The image object which should be saved.
			 * 
			 * @annotation An exception should be thrown, if an error occurs.
			 */
			virtual void SaveImage(const std::string &Path, const CImage &img) = 0;

			virtual ~IImageHandler() {};
	};
}

#endif /* IIMAGELOADER_HPP_ */
