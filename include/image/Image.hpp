/*
 * zlib License
 *
 * (C) 2018 Christian Tost
 *
 * This software is provided 'as-is', without any express or implied
 * warranty.  In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 *    claim that you wrote the original software. If you use this software
 *    in a product, an acknowledgment in the product documentation would be
 *    appreciated but is not required.
 * 2. Altered source versions must be plainly marked as such, and must not be
 *    misrepresented as being the original software.
 * 3. This notice may not be removed or altered from any source distribution.
 */

#ifndef IMAGE_HPP_
#define IMAGE_HPP_

#include <platform.h>
#include <string>
#include <math/Vector2D.hpp>
#include <utils/Color.hpp>
#include <map>

#include <iostream>

namespace DivineAPI
{
	class IImageHandler;

	/*! 
	 * @brief Needed to create an image handler which is associated with an image format.
	 * 
	 * @remarks A new image format is registered via CImage::RegisterImageFormat("ext", &MyImageHandler::Create);
	 * @remark Example:
	 * @code{.cpp}
	 * class MyImageHandler : public IImageHandler
	 * {
	 * 	public:
	 * 		MyImageHandler() { ... }
	 * 
	 * 		void *LoadImage(std::string Path, Vec2Di &Size) { ... }
	 *  	void *LoadImage(const void *Data, size_t Length, Vec2Di &Size, CImage::Pixelformat &Format) { ... }
	 * 		void SaveImage(const std::string &Path, const CImage &img) { ... }
	 *
	 * 		IImageHandler *Create()
	 * 		{
	 * 			return new MyImageHandler();
	 * 		}
	 *		
	 * 		virtual ~MyImageHandler() { ... }
	 * };
	 * 
	 * int main(int argc, char **argv)
	 * {
	 * 	...
	 * 	CImage::RegisterImageFormat("myext", &MyImageHandler::Create);
	 * 	...
	 * }
	 * 
	 * @endcode
	 */
	typedef IImageHandler *(*Create)();

	/*!
	 * @brief With this class images can be read and saved.
	 * 
	 * @remarks Supported image formats are .bmp, .jpg, .png, .tga. For other image formats you need to write your own image loader. 
	 * See also ::Create.
	 * 
	 * @remark Different pixel formats are supported. See also CImage::Pixelformat.
	 */
	class DIVINE_API CImage
	{
		public:
			enum class Pixelformat
			{
				UNKNOWN,		//!< Pixel format is not supported or unknown(Initial value).
				ALPHA_8,		//!< 1 Byte for just an alpha channel.
				RGB_8_8_8,		//!< 3 Bytes where the first byte is the red value.
				BGR_8_8_8,		//!< 3 Bytes where the first byte is the blue value.
				RGBA_8_8_8_8,	//!< 4 Bytes where the first byte is the red value.
				BGRA_8_8_8_8	//!< 4 Bytes where the first byte is the blue value.
			};

			CImage();

			/*!
			 * @brief Loads a file.
			 * 
			 * @param Path: Path to the file to load.
			 * 
			 * @throws std::invalid_argument Occurs if the path is empty.
			 * @throw std::runtime_error Occurs if the format is unknown or the file couldn't loaded.
			 */
			CImage(const std::string &Path) : CImage()
			{
				LoadImage(Path);
			}

			/*!
			 * @brief Copies an image.
			 * 
			 * @param Img: The image to copy.
			 */
			CImage(const CImage &Img);

			/*!
			 * @brief Copies an image.
			 * 
			 * @param Img: The image to copy.
			 */
			CImage &operator=(const CImage &Img)
			{
				if(this != &Img)
					CopyImage(Img);

				return *this;
			}

			/*!
			 * @brief Creates a new empty image.
			 * 
			 * @param Size: The resolution of the new image.
			 * @param Format: The pixelformat of the image.
			 * 
			 * @throw std::invalid_argument Occurs if the pixel format is unknown.
			 */
			void CreateEmptyImage(Vec2Di Size, Pixelformat Format);

			/*!
			 * @brief Loads a file.
			 * 
			 * @param Path: Path to the file to load.
			 * 
			 * @throws std::invalid_argument Occurs if the path is empty.
			 * @throw std::runtime_error Occurs if the format is unknown or the file couldn't loaded.
			 */
			void LoadImage(const std::string &Path);

			/*!
			 * @brief Loads an image from memory.
			 * 
			 * @param Data: CImage data.
			 * @param Length: Length of the data.;
			 * @param Ext: The image type. E.g. png, jpg, bmp, tga.
			 * 
			 * @throw std::runtime_error Occurs if the format is unknown or the file couldn't loaded.
			 */
			void LoadFromMemory(const void *Data, size_t Length, const std::string &Ext);

			/*!
			 * @brief Saves a file.
			 * 
			 * @param Path: Path to the file to save.
			 * 
			 * @throws std::invalid_argument Occurs if the path is empty.
			 * @throw std::runtime_error Occurs if the format is unknown or the file couldn't saved.
			 */
			void SaveImage(const std::string &Path);

			/*!
			 * @brief Converts the pixelformat of the image to another one.
			 * 
			 * @param Format: The new pixel format.
			 * 
			 * @throws std::invalid_argument Occurs if the format is unknown.
			 * @throw std::runtime_error Occurs if no image is assigned.
			 * 
			 * @attention Function does nothing, if Format is equal the format of this image.
			 */
			void ConvertToFormat(CImage::Pixelformat Format);

			/*!
			 * @brief Sets the color of one pixel at the given position.
			 * 
			 * @param Pos: Position of the pixel.
			 * @param Color: New color of the pixel.
			 * 
			 * @throw std::runtime_error Occurs if no image is assigned.
			 */
			void SetPixel(Vec2Di Pos, Color color);

			/*!
			 * @brief Sets the color of one pixel at the given position.
			 * 
			 * @param Pos: Position of the pixel.
			 * @param Pixel: New color of the pixel.
			 * 
			 * @throw std::runtime_error Occurs if no image is assigned.
			 */
			void SetPixel(Vec2Di Pos, unsigned int Pixel);

			/*!
			 * @brief Draws an image on the given position.
			 * 
			 * @throw std::runtime_error Occurs if no image is assigned.
			 */
			void DrawImage(Vec2Di Pos, const CImage &img);

			/*!
			 * @brief Returns the count of bytes of the given pixel format.
			 * 
			 * @throw std::invalid_argument Occurs if the format is unknown.
			 */
			static int GetBytesPerPixel(Pixelformat Format);

			/*!
			 * @brief Frees all the image data.
			 */
			void FreeImage();

			/*!
			 * @brief Registers a new image format.
			 * 
			 * @param Ext: The extension of the format.
			 * @param Creator: A creator handler which allows to construct the loader class. See also ::Create.
			 * 
			 * @throw std::invalid_argument Occurs if Ext is empty or Creator is null.
			 */
			static void RegisterImageFormat(std::string Ext, Create Creator);

			/*!
			 * @brief Returns the current pixel data of the image.
			 * 
			 * @return Returns the pixel data in the format of pixel format.
			 * 
			 * @attention This is a direct access pointer, if you edit this pointer it will also edit the image.
			 */
			void *GetPixels() const
			{
				return m_Pixels;
			}

			Vec2Di GetSize() const
			{
				return m_Size;
			}

			Pixelformat GetPixelformat() const
			{
				return m_Format;
			}

			/*!
			 * @brief Returns the path of a loaded image.
			 * 
			 * @return Returns the path of the image.
			 */
			std::string GetPath() const
			{
				return m_Path;
			}

			virtual ~CImage()
			{
				FreeImage();
			}

		private:
			void CopyImage(const CImage &Img);

			static bool IsEqual(Pixelformat Format1, Pixelformat Format2);
			void ConvertPixelFormat(CImage::Pixelformat Format);
			void SwapRB(CImage::Pixelformat Format);

			void *m_Pixels;	//!< CImage data.
			Pixelformat m_Format;	//!< Pixel format.

			Vec2Di m_Size;		//!< The image resolution.
			std::string m_Path;	//!< Path of the loaded image.
			static std::map<std::string, Create> m_ImageFormats;	//!< List of supported image formats.
	};
}  // namespace DivineAPI

#endif /* IMAGE_HPP_ */
