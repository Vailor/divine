#version 330

in vec2 fragColor;
out vec4 final;

void main()
{
	final = fragColor;
}
