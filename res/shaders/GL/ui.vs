#version 330

out vec2 fragColor;

uniform mat4 Proj;
uniform mat4 Translation;
uniform mat4 Scale;
uniform mat4 Rotation;

in vec3 coord;
in vec4 color;

void main()
{
	gl_Position = Proj * Translation * Rotation * Scale * vec4(coord, 1.0);
	fragColor = color;
}
