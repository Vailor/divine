#version 330

out vec2 UV;

uniform mat4 Projection;
uniform mat4 Translation;
uniform mat4 Scale;
uniform mat4 Rotation;

in vec3 Coord;
in vec2 UVCoord;

void main()
{
	gl_Position = Projection * Translation * Rotation * Scale * vec4(Coord, 1.0);
	UV = UVCoord;
}
