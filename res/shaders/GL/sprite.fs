#version 330

in vec2 UV;
out vec4 Final;

uniform sampler2D Texture;

void main()
{
	Final = texture2D(Texture, UV);
}
