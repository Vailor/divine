cmake_minimum_required(VERSION 3.0)
project(EngineTest)

include_directories("${PROJECT_SOURCE_DIR}/../include")

link_directories("${PROJECT_SOURCE_DIR}/../build")

add_executable(EngineTest "${PROJECT_SOURCE_DIR}/main.cpp")
target_link_libraries(EngineTest Divine)