#include <DivineAPI.hpp>

using namespace DivineAPI;

//Gameloop class which is every rendering cyrcle called.
class CLoop : public IGameLoop
{
    public:
        CLoop(IWindow *win) : m_Window(win) 
        {
            //Creates two new text elements. Text elements uses as default font the build in font.
            m_FPS = m_Window->GetSceneManager()->AddNode<CText>(nullptr);
            m_VRAMUsage = m_Window->GetSceneManager()->AddNode<CText>(nullptr);

            m_FPS->SetText("0");
            m_FPS->SetPosition(Vec2Df(m_Window->GetWindowSize().x - m_FPS->GetSize().x, 0));

            m_VRAMUsage->SetFontSize(20);
            m_VRAMUsage->SetText("0/0 MB");
            m_VRAMUsage->SetPosition(Vec2Df(m_Window->GetWindowSize().x - m_VRAMUsage->GetSize().x, m_Window->GetWindowSize().y - m_VRAMUsage->GetSize().y));
        }

        void MainLoop(unsigned int FPS)
        {
            m_FPS->SetText(std::to_string(FPS));
            m_FPS->SetPosition(Vec2Df(m_Window->GetWindowSize().x - m_FPS->GetSize().x, 0));

            //Gets the current video ram usage.
            VRAMInfo Info = m_Window->GetDriver()->GetVRAMInfo();

            m_VRAMUsage->SetText(std::to_string(Info.UsedMemory / 1024 / 1024) + "/" + std::to_string(Info.TotalMemory / 1024 / 1024) + " MB");
            m_VRAMUsage->SetPosition(Vec2Df(m_Window->GetWindowSize().x - m_VRAMUsage->GetSize().x, m_Window->GetWindowSize().y - m_VRAMUsage->GetSize().y));
        }

    private:
        IWindow *m_Window;
        CText *m_FPS, *m_VRAMUsage;
};

//Prints system informations.
void PrintSystemInfos()
{
    //Gets all monitors which are connected to the pc.
    std::vector<System::MonitorInfo> Monitors = System::GetMonitors();

    LOGI("---------------Monitors---------------\n");
    
    for(size_t i = 0; i < Monitors.size(); i++)
    {
        LOGI(Monitors[i].Name);
        LOGI("Resolution: X: %i Y: %i", Monitors[i].Resolution.x, Monitors[i].Resolution.y);
        LOGI("LeftTop: X: %i Y: %i", Monitors[i].LeftTop.x, Monitors[i].LeftTop.y);
        LOGI("DPI: X: %.2f Y: %.2f", Monitors[i].DPI.x, Monitors[i].DPI.y);
        LOGI("Depth: %ui", Monitors[i].Depth);
        LOGI("%i Hz", Monitors[i].RefreshRate);
    }
     
    LOGI("\n---------------Disks---------------\n");

    //Get all partitions of the system.
    std::vector<System::DiskInfo> Disks = System::GetDisksInfos();

    for(size_t i = 0; i < Disks.size(); i++)
    {
        if(i != 0)
            LOGI("\n");

        LOGI(Disks[i].Path);
        LOGI("Total: %i GB", Disks[i].TotalSpace / 1024 / 1024 / 1024);
        LOGI("Free: %i GB", Disks[i].FreeSpace / 1024 / 1024 / 1024);
        LOGI("Available: %i GB", Disks[i].Available / 1024 / 1024 / 1024);
    }

    LOGI("\n---------------Memory---------------\n");

    //Gets the memory of the system.
    System::Memory mem = System::GetSystemMemoryInfo();

    LOGI("Total: %i GB", mem.TotalMemory / 1024 / 1024 / 1024);
    LOGI("Free: %i GB", mem.FreeMemory / 1024 / 1024 / 1024);
}

int main(int argc, char const *argv[])
{
    //Initializes the engine, must called before any function of the engine is be used.
    Divine::Init();

//Creates a new window.
#ifndef DIVINE_ANDROID
    IWindow *win = IWindow::Create("Engine Test", Vec2Di(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED), Vec2Dui(800, 600));
#else
    SDL_DisplayMode DM;
    SDL_GetCurrentDisplayMode(0, &DM);
    int Width = DM.w;
    int Height = DM.h;

    IWindow *win = IWindow::Create("Engine Test", Vec2Di(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED), Vec2Dui(Width, Height));
#endif

    PrintSystemInfos();

    win->SetBackgroundColor(Color(200, 200, 200));

    CSceneManager *smgr = win->GetSceneManager();
    IDriver *drv = win->GetDriver();

    //Get general gpu informations.
    GPUInfo Info = drv->GetGPUInfo();

    CText *txt = smgr->AddNode<CText>(nullptr);
    txt->SetText(Info.GPUName);
    txt->SetFontColor(Color(255, 0, 255, 128));
    txt->SetFontSize(20);

    Vec2Df Size = txt->GetSize();

    txt = smgr->AddNode<CText>(nullptr);
    txt->SetText(Info.GraphicsAPIVersion + "\n" + Info.ShaderVersion + "\n" + Info.Vendor);
    txt->SetPosition(Vec2Df(0, Size.y));

    txt = smgr->AddNode<CText>(nullptr);
    txt->SetText("tga");
    txt->SetPosition(Vec2Df(100, 120));

    //Creates a sprite instance.
    CSprite *spr = smgr->AddNode<CSprite>(txt);

    //Loads a tga file.
    spr->LoadSprite("Cobble.tga");
    spr->SetPosition(Vec2Df(0, txt->GetSize().y));

    txt = smgr->AddNode<CText>(nullptr);
    txt->SetText("png");
    txt->SetPosition(Vec2Df(200, 120));

    spr = smgr->AddNode<CSprite>(txt);
    spr->LoadSprite("Slime.png");
    spr->Scale(Vec2Df(3, 3));
    spr->SetPosition(Vec2Df(0, txt->GetSize().y));

    txt = smgr->AddNode<CText>(nullptr);
    txt->SetText("jpg");
    txt->SetPosition(Vec2Df(500, 120));

    spr = smgr->AddNode<CSprite>(txt);
    spr->LoadSprite("Smile.jpg");
    spr->SetPosition(Vec2Df(0, txt->GetSize().y));

    txt = smgr->AddNode<CText>(nullptr);
    txt->SetText("bmp");
    txt->SetPosition(Vec2Df(650, 120));

    spr = smgr->AddNode<CSprite>(txt);
    spr->LoadSprite("Smile2.bmp");
    spr->SetPosition(Vec2Df(0, txt->GetSize().y));

    CLoop Loop(win);
    
    //Registers a gameloop instance.
    Divine::RegisterGameLoop(&Loop);

    //Starts the engine main loop.
    Divine::Run();

    //Cleanups the engine.
    Divine::Quit();
    return 0;
}
